#ifndef SAYTEN_COMONOIDAL_HPP
#define SAYTEN_COMONOIDAL_HPP

#include <concepts>

#include "placeholder.hpp"
#include "unit.hpp"
#include "product.hpp"
#include "contravariant.hpp"

namespace sayten::types
{
    template<template<typename> typename comonoidal_t>
    concept has_unit_untransform =
    requires (comonoidal_t<unit> comonoidal_unit)
    {
        { comonoidal_t<placeholders::placeholder<0>>::unit_untransform(comonoidal_unit) } -> std::same_as<unit>;
    };

    template<template<typename> typename comonoidal_t>
    concept has_split =
    requires (comonoidal_t<placeholders::placeholder_product<0, 1>> comonoidal)
    {
        { comonoidal.split() } -> std::same_as<product<comonoidal_t<placeholders::placeholder<0>>, comonoidal_t<placeholders::placeholder<1>>>>;
    } && requires (comonoidal_t<placeholders::placeholder<0>> comonoidal)
    {
        { comonoidal.split() } -> std::same_as<product<comonoidal_t<unit>, comonoidal_t<placeholders::placeholder<0>>>>;
    };

    template<template<typename> typename comonoidal_t>
    concept comonoidal = functor<comonoidal_t> && has_unit_untransform<comonoidal_t> && has_split<comonoidal_t>;

    template<template<typename> typename comonoidal_t, typename value_t>
    requires has_unit_untransform<comonoidal_t>
    [[nodiscard]]
    constexpr unit call_unit_untransform(const comonoidal_t<unit> &value)
    {
        return comonoidal_t<value_t>::unit_untransform(value);
    }

    template<template<typename> typename comonoidal_t, typename value_t>
    requires has_split<comonoidal_t>
    [[nodiscard]]
    constexpr auto call_split(const comonoidal_t<value_t> &value)
    {
        return value.split();
    }
}

#endif