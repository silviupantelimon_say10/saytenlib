#ifndef SAYTEN_CONCEPTS_HPP
#define SAYTEN_CONCEPTS_HPP

#include <concepts>
#include "function_concepts.hpp"
#include "template_concepts.hpp"
#include "container_concepts.hpp"

namespace sayten
{
    template<size_t N, typename ...args>
    struct get_type;

    template<typename arg, typename ...args>
    struct get_type<0, arg, args...>
    {
        using type = arg;
    };

    template<size_t N, typename arg, typename ...args>
    requires (N != 0)
    struct get_type<N, arg, args...>
    {
        using type = get_type<N - 1, args...>::type;
    };

    template<size_t N, typename template_type>
    struct get_template_parameter;

    template<size_t N, template<typename...> typename template_type, typename ...args>
    struct get_template_parameter<N, template_type<args...>>
    {
        using type = get_type<N, args...>::type;
    };

    template<size_t N, typename template_type>
    using get_template_parameter_t = typename get_template_parameter<N, template_type>::type;

    template<typename template_t, typename ...args>
    struct replace_template_parameters;

    template<template<typename...> typename template_t, typename ...args, typename ...old_args>
    struct replace_template_parameters<template_t<old_args...>, args...>
    {
        using type = template_t<args...>;
    };
    
    template<typename template_t, typename ...args>
    using replace_template_parameters_t = replace_template_parameters<template_t, args...>::type;

    template<typename ...args>
    struct template_holder {};

    template<size_t N = 0, typename ...args>
    struct get_template_parameter_count;

    template<size_t N>
    struct get_template_parameter_count<N>
    {
        static constexpr size_t count = N;
    };

    template<size_t N, typename arg, typename ...args>
    struct get_template_parameter_count<N, arg, args...>
    {
        static constexpr size_t count = get_template_parameter_count<N + 1, args...>::count;
    };

    template<typename value_t>
    struct get_template_parameter_count_of_class
    {
        static constexpr size_t count = 0;
    };

    template<typename ...args, template<typename...> typename template_class_t>
    struct get_template_parameter_count_of_class<template_class_t<args...>>
    {
        static constexpr size_t count = get_template_parameter_count<0, args...>::count;
    };

    template<typename first_t, typename second_t>
    struct has_same_template_parameter_list
    {
        static constexpr bool value = std::is_same<first_t, second_t>::value;
    };

    template<typename ...first_args, template<typename...> typename first_t, typename ...second_args, template<typename, typename...> typename second_t>
    struct has_same_template_parameter_list<first_t<first_args...>, second_t<second_args...>>
    {
        static constexpr bool value = std::is_same<template_holder<first_args...>, template_holder<second_args...>>::value;
    };

    template<typename first_t, typename second_t>
    concept same_template_parameter_list_as = has_same_template_parameter_list<first_t, second_t>::value;

    template<template<typename...> typename first_t, template<typename...> typename second_t>
    struct is_same_template
    {
        static constexpr bool value = false;
    };

    template<template<typename...> typename type_t>
    struct is_same_template<type_t, type_t>
    {
        static constexpr bool value = true;
    };

    template<template<typename...> typename first_t, template<typename...> typename second_t>
    concept same_template = is_same_template<first_t, second_t>::value;

    template<typename first_t, typename second_t>
    struct has_same_template_class
    {
        static constexpr bool value = std::is_same<first_t, second_t>::value;
    };

    template<typename ...first_args, template<typename...> typename first_t, typename ...second_args, template<typename, typename...> typename second_t>
    struct has_same_template_class<first_t<first_args...>, second_t<second_args...>>
    {
        static constexpr bool value = (get_template_parameter_count<0, first_args...>::count == get_template_parameter_count<0, second_args...>::count) &&
                                        is_same_template<first_t, second_t>::value;
    };

    template<typename first_t, typename second_t>
    concept same_template_class_as = has_same_template_class<first_t, second_t>::value;

    template<typename class_t, template<typename...> typename template_t>
    struct is_of_template
    {
        constexpr static bool value = false;
    };

    template<template<typename...> typename template_t, typename ...args>
    struct is_of_template<template_t<args...>, template_t>
    {
        constexpr static bool value = true;
    };

    template<typename class_t, template<typename...> typename template_t>
    concept of_template = is_of_template<class_t, template_t>::value;

    template<typename first_t, typename second_t>
    struct has_same_parameters_except_first
    {
        constexpr static bool value = false;
    };

    template<typename first_arg, template<typename...> typename first_t, typename second_arg, template<typename, typename...> typename second_t, typename ...args>
    struct has_same_parameters_except_first<first_t<first_arg, args...>, second_t<second_arg, args...>>
    {
        constexpr static bool value = is_same_template<first_t, second_t>::value;
    };

    template<typename first_t, typename second_t>
    concept same_parameters_except_first = has_same_parameters_except_first<first_t, second_t>::value;

    template<typename class_t, template<typename...> typename template_t, typename ...args>
    struct is_of_template_with_parameters_except_first
    {
        constexpr static bool value = false;
    };

    template<typename value_t, template<typename...> typename template_t, typename ...args>
    struct is_of_template_with_parameters_except_first<template_t<value_t, args...>, template_t, args...>
    {
        constexpr static bool value = true;
    };

    template<typename class_t, template<typename...> typename template_t, typename ...args>
    concept of_template_with_parameters_except_first = is_of_template_with_parameters_except_first<class_t, template_t, args...>::value;

    template<size_t N, typename taken, typename rest>
    requires of_template<taken, template_holder> && of_template<rest, template_holder>
    struct template_parameter_splitter;

    template<typename taken, typename rest>
    struct template_parameter_splitter<0, taken, rest>
    {
        using taken_holder = taken;
        using rest_holder = rest;
    };

    template<size_t N, typename arg, typename ...taken_args, typename ...rest_args>
    requires (N != 0)
    struct template_parameter_splitter<N, template_holder<taken_args...>, template_holder<arg, rest_args...>>
    {
        using taken_holder = template_parameter_splitter<N - 1, template_holder<taken_args..., arg>, template_holder<rest_args...>>::taken_holder;
        using rest_holder = template_parameter_splitter<N - 1, template_holder<taken_args..., arg>, template_holder<rest_args...>>::rest_holder;
    };

    template<typename class_t, template<typename...> typename template_t>
    struct reverse_apply_same_parameters;

    template<template<typename...> typename template_source_t, template<typename...> typename template_t, typename ...args>
    struct reverse_apply_same_parameters<template_source_t<args...>, template_t>
    {
        using type = template_t<args...>;
    };

    template<typename class_t, template<typename...> typename template_t>
    using reverse_apply_same_parameters_t = reverse_apply_same_parameters<class_t, template_t>::type;

    template<typename first, typename second>
    requires of_template<first, template_holder> && of_template<second, template_holder>
    struct template_parameter_merger;

    template<typename ...first_args, typename ...second_args>
    struct template_parameter_merger<template_holder<first_args...>, template_holder<second_args...>>
    {
        using type = template_holder<first_args..., second_args...>;
    };

    template<typename class_t, typename parameter, size_t N>
    struct replace_template_parameter;

    template<template<typename...> typename template_class_t, typename parameter, size_t N, typename ...args>
    struct replace_template_parameter<template_class_t<args...>, parameter, N>
    {
    private:
        using taken_holder = template_parameter_splitter<N, template_holder<>, template_holder<args...>>::taken_holder;
        using rest_holder = template_parameter_splitter<N, template_holder<>, template_holder<args...>>::rest_holder;
        using rest_holder_but_one = template_parameter_splitter<1, template_holder<>, rest_holder>::rest_holder;
        using merged_holder_part = template_parameter_merger<taken_holder, template_holder<parameter>>::type;
        using merged_holder = template_parameter_merger<merged_holder_part, rest_holder_but_one>::type;
    public:
        using type = reverse_apply_same_parameters<merged_holder, template_class_t>::type;
    };

    template<typename class_t, typename parameter, size_t N>
    using replace_template_parameter_t = typename replace_template_parameter<class_t, parameter, N>::type;

    template<typename class_t, typename parameter, typename replacer>
    struct replace_parameter;

    template<typename class_t, typename parameter, typename replacer>
    struct replace_parameter
    {
        using type = class_t;
    };

    template<typename class_t, typename replacer>
    struct replace_parameter<class_t, class_t, replacer>
    {
        using type = replacer;
    };

    template<template<typename...> typename class_template_t, typename parameter, typename replacer, typename arg, typename ...args>
    requires (!std::same_as<class_template_t<arg, args...>, parameter>)
    struct replace_parameter<class_template_t<arg, args...>, parameter, replacer>
    {
        using type = reverse_apply_same_parameters_t<typename template_parameter_merger<template_holder<typename replace_parameter<arg, parameter, replacer>::type>, typename replace_parameter<template_holder<args...>, parameter, replacer>::type>::type, class_template_t>;
    };

    template<typename class_t, typename parameter, typename replacer>
    using replace_parameter_t = replace_parameter<class_t, parameter, replacer>::type;

    template<typename class_t, typename input_t, typename output_t>
    concept parametrized_invocable = std::same_as<typename get_template_parameter<0, class_t>::type, typename get_template_parameter<0, output_t>::type> &&
    requires (class_t obj, input_t input)
    {
        { std::invoke(&class_t::operator(), obj, input) } -> std::same_as<output_t>;
    };
    
    template<typename output_class_t, typename class_t, typename map_function_t>
    struct is_mapped_from
    {
        static constexpr bool value = false;
    };

    template<template<typename, typename...> typename template_class_t, typename map_function_t, typename input_t, typename output_t, typename ...args>
    requires invocable_and_resulting<map_function_t, output_t, input_t>
    struct is_mapped_from<template_class_t<output_t, args...>, template_class_t<input_t, args...>, map_function_t>
    {
        static constexpr bool value = true;
    };

    template<typename output_class_t, typename class_t, typename map_function_t>
    concept mapped_from = is_mapped_from<output_class_t, class_t, map_function_t>::value;

    template<typename value_t>
    concept hashed = 
    requires (const std::hash<value_t> &hasher, const value_t &value)
    {
        { hasher(value) } -> std::same_as<size_t>;
    };
}

#endif