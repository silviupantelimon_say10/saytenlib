#ifndef SAYTEN_MAYBE_HPP
#define SAYTEN_MAYBE_HPP

#include <optional>
#include <concepts>
#include "concepts.hpp"
#include "functor.hpp"
#include "applicative.hpp"
#include "alternative.hpp"
#include "monad.hpp"

namespace sayten::types
{
    template<typename value_t>
    class maybe
    {
    protected:
        static constexpr bool value_is_applicable = is_callable<value_t>::value && (unwrap_function<value_t>::args_size == 1) && (!std::is_void_v<typename unwrap_function<value_t>::result_type>);

        std::optional<value_t> opt;

    public:
        template<typename other_value_t>
        friend class maybe;

        maybe() : opt() {}
        maybe(const value_t &value) : opt(value) {}
        explicit maybe(value_t &&value) : opt(value) {}
        maybe(const maybe &other) : opt(other.opt) {}
        maybe &operator=(const maybe &other)
        {
            opt = other.opt;

            return *this;
        }

        constexpr bool has_value() const noexcept
        {
            return opt.has_value();
        }

        constexpr explicit operator bool() const noexcept
        {
            return opt.has_value();
        }

        constexpr value_t value() const
        {
            return opt.value();
        }

        template<typename map_function_t>
        requires types::fmap_requirement<value_t, map_function_t>
        constexpr maybe<typename unwrap_function<map_function_t>::result_type> fmap(const map_function_t &map_function) const
        {
            if (*this) {
                return maybe<typename unwrap_function<map_function_t>::result_type>(map_function(this->value()));
            }

            return maybe<typename unwrap_function<map_function_t>::result_type>();
        }

        constexpr bool operator==(const maybe &other) const noexcept
        requires std::equality_comparable<value_t>
        {
            return (!*this && !other) || (*this && other && this->value() == other.value());
        }

        constexpr std::partial_ordering operator<=>(const maybe &other) const noexcept
        requires std::three_way_comparable<value_t>
        {
            if (!*this && !other) {
                return std::partial_ordering::equivalent;
            } else if (!*this) {
                return std::partial_ordering::less;
            } else if (!other) {
                return std::partial_ordering::greater;
            }

            return this->value() <=> other.value();
        }

        constexpr bool operator==(const value_t &other) const noexcept
        requires std::equality_comparable<value_t>
        {
            return *this && this->value() == other;
        }

        constexpr std::partial_ordering operator<=>(const value_t &other) const noexcept
        requires std::three_way_comparable<value_t>
        {
            return !*this ? std::partial_ordering::unordered : this->value() <=> other;
        }

        [[nodiscard]]
        static constexpr maybe<unit> unit_transform(const unit &value) 
        { 
            return maybe<unit>(value); 
        }

        template<typename other_value_t>
        [[nodiscard]]
        constexpr maybe<product<value_t, other_value_t>> merge(const maybe<other_value_t> &other) const
        {
            if (!*this || !other)
                return maybe<product<value_t, other_value_t>>();

            return maybe<product<value_t, other_value_t>>(product<value_t, other_value_t>(value(), other.value())); 
        }

        static constexpr maybe<value_t> pure(value_t value)
        {
            return maybe<value_t>(value);
        }

        template<typename function_t>
        requires apply_requirement<value_t, function_t>
        constexpr maybe<typename unwrap_function<function_t>::result_type> reverse_apply(const maybe<function_t> &maybe_value) const
        {
            if (*this && maybe_value) {
                return maybe<typename unwrap_function<function_t>::result_type>(maybe_value.value()(this->value()));
            }

            return maybe<typename unwrap_function<function_t>::result_type>();
        }

        template<typename other_value_t>
        requires apply_requirement<other_value_t, value_t>
        constexpr auto apply(const maybe<other_value_t> &other) const
        {
            if (*this && other) {
                return maybe<typename unwrap_function<value_t>::result_type>(value()(other.value()));
            }

            return maybe<typename unwrap_function<value_t>::result_type>();
        }

        template<typename function_t>
        requires lift2_requirement<value_t, function_t>
        constexpr maybe<typename unwrap_function<function_t>::result_type> lift2(const function_t &function, const maybe<typename unwrap_function<function_t>::arg_no_cvref_type<1>> &maybe_value) const
        {
            if (*this && maybe_value) {
                return maybe<typename unwrap_function<function_t>::result_type>(function(this->value(), maybe_value.value()));
            }

            return maybe<typename unwrap_function<function_t>::result_type>();
        }

        static const maybe empty;

        constexpr maybe alternative(const maybe &other) const
        {
            if (*this)
                return *this;

            return other;
        }

        template<typename new_value_t = value_t>
        requires
        std::same_as<new_value_t, value_t> &&
        of_template<new_value_t, maybe>
        constexpr maybe<get_template_parameter_t<0, new_value_t>> join()
        {
            if (*this && this->value()) {
                return maybe<get_template_parameter_t<0, new_value_t>>(this->value().value());
            } 

            return maybe<get_template_parameter_t<0, new_value_t>>();
        }

        template<typename function_t>
        requires bind_requirement<value_t, function_t, maybe>
        constexpr typename unwrap_function<function_t>::result_type bind(function_t function)
        {
            if (*this) {
                return function(this->value());
            }

            return typename unwrap_function<function_t>::result_type();
        }

        template<typename other_value>
        constexpr maybe<other_value> then(maybe<other_value> other_maybe)
        {
            return *this ? other_maybe : maybe<other_value>();
        }

        constexpr value_t or_else(const value_t &other) const
        {
            if (*this)
                return this->value();

            return other;
        }

        template<typename function_t>
        requires callable<function_t> && 
        (unwrap_function<function_t>::args_size == 0) &&
        std::same_as<value_t, typename unwrap_function<function_t>::result_type>
        constexpr value_t or_else_get(const function_t &generate) const
        {
            if (*this)
                return this->value();

            return generate();
        }
    };

    template<typename value_t>
    const maybe<value_t> maybe<value_t>::empty = maybe<value_t>();
}

namespace std
{
    template<typename value_t>
    struct hash<sayten::types::maybe<value_t>>
    {
        size_t operator()(const sayten::types::maybe<value_t> &obj) const noexcept
        {
            hash<value_t> hasher;

            return obj ? hasher(obj.value()) : 0;
        }
    };
}

#endif