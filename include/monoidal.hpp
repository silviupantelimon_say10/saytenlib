#ifndef SAYTEN_MONOIDAL_HPP
#define SAYTEN_MONOIDAL_HPP

#include <concepts>

#include "placeholder.hpp"
#include "unit.hpp"
#include "product.hpp"
#include "functor.hpp"

namespace sayten::types
{
    template<template<typename> typename monoidal_t>
    concept has_unit_transform =
    requires
    {
        { monoidal_t<placeholders::placeholder<0>>::unit_transform(unit()) } -> std::same_as<monoidal_t<unit>>;
    };

    template<template<typename> typename monoidal_t>
    concept has_merge =
    requires (monoidal_t<placeholders::placeholder<0>> monoidal_a, monoidal_t<placeholders::placeholder<1>> monoidal_b)
    {
        { monoidal_a.merge(monoidal_b) } -> std::same_as<monoidal_t<placeholders::placeholder_product<0, 1>>>;
    };

    template<template<typename> typename monoidal_t>
    concept monoidal = functor<monoidal_t> && has_unit_transform<monoidal_t> && has_merge<monoidal_t>;

    template<template<typename> typename monoidal_t, typename value_t>
    requires has_unit_transform<monoidal_t>
    [[nodiscard]]
    constexpr monoidal_t<unit> call_unit_transform(const unit &value)
    {
        return monoidal_t<value_t>::unit_transform(value);
    }

    template<template<typename> typename monoidal_t, typename value_a_t, typename value_b_t>
    requires has_merge<monoidal_t>
    [[nodiscard]]
    constexpr monoidal_t<product<value_a_t, value_b_t>> call_merge(const monoidal_t<value_a_t> &a, const monoidal_t<value_b_t> &b)
    {
        return a.merge(b);
    }
}

namespace sayten::placeholders
{
    template<typename value_t>
    struct placeholder_monoidal
    {
        template<typename function_t>
        requires sayten::types::fmap_requirement<value_t, function_t>
        [[nodiscard]]
        constexpr placeholder_monoidal<typename unwrap_function<function_t>::result_type> fmap(const function_t &) const
        {
            return placeholder_monoidal<typename unwrap_function<function_t>::result_type>();
        }

        [[nodiscard]]
        constexpr static placeholder_monoidal<sayten::types::unit> unit_transform(const sayten::types::unit &value) 
        { 
            return placeholder_monoidal<sayten::types::unit>(value); 
        }

        template<typename other_value_t>
        [[nodiscard]]
        constexpr placeholder_monoidal<sayten::types::product<value_t, other_value_t>> merge(const placeholder_monoidal<other_value_t> &other) const
        { 
            return placeholder_monoidal<sayten::types::product<value_t, other_value_t>>(); 
        }
    };
}

#endif