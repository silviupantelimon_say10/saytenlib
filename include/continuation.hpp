#ifndef SAYTEN_CONTINUATION_HPP
#define SAYTEN_CONTINUATION_HPP

#include <functional>
#include "function_concepts.hpp"
#include "functor.hpp"
#include "applicative.hpp"
#include "monad.hpp"
#include "identity.hpp"

namespace sayten::types
{
    template<typename result_t, template<typename> typename container_t, typename input_t>
    class continuation_transformer;

    namespace
    {
        template<typename test_t>
        concept has_doubled_type =
        requires
        {
            typename test_t::doubled_type;
        };

        template<typename class_a_t, typename class_b_t>
        struct is_compatible_continuation
        {
            static constexpr bool value = false;
        };

        template<typename result_t, template<typename> typename container_t, typename input_a_t, typename input_b_t>
        struct is_compatible_continuation<continuation_transformer<result_t, container_t, input_a_t>, continuation_transformer<result_t, container_t, input_b_t>>
        {
            static constexpr bool value = true;
        };

        template<typename class_a_t, typename class_b_t>
        concept compatible_continuation = is_compatible_continuation<class_a_t, class_b_t>::value;
    }

    template<typename result_t, template<typename> typename container_t, typename input_t>
    class continuation_transformer
    {
    private:
        using continuation_call_type = std::function<container_t<result_t>(std::function<container_t<result_t>(const input_t&)>)>;

        template<typename other_result_t, typename other_input_t>
        using generic_continuation_call_type = std::function<container_t<other_result_t>(std::function<container_t<other_result_t>(const other_input_t&)>)>;

        continuation_call_type continuation_call;

        template<typename new_input_t>
        using specific_continuation_type = continuation_transformer<result_t, container_t, new_input_t>;

        using result_type = result_t;
        using container_type = container_t<result_t>;
        using input_type = input_t;

        using doubled_type = continuation_transformer<result_t, container_t, continuation_transformer<result_t, container_t, input_t>>;

    public:
        template<typename other_result_t, template<typename> typename other_container_t, typename other_input_t>
        friend class continuation_transformer;

        continuation_transformer(const continuation_call_type &continuation_call) : 
        continuation_call(continuation_call) {}

        continuation_transformer(const continuation_transformer &other) : 
        continuation_call(other.continuation_call) {}

        continuation_transformer &operator=(const continuation_transformer &other) const
        {
            continuation_call = other.continuation_call;

            return *this;
        }

        [[nodiscard]]
        static continuation_transformer pure(const input_t &input)
        {
            std::function<container_t<result_t>(const std::function<container_t<result_t>(const input_t&)>&)> call = [input] (const std::function<container_t<result_t>(const input_t&)> &cont) -> container_t<result_t> { return cont(input); };

            return continuation_transformer(call);
        }
        
        constexpr container_t<result_t> run_cont(const std::function<container_t<result_t>(const input_t&)> &call) const
        {
            return continuation_call(call);
        }

        [[nodiscard]]
        constexpr static specific_continuation_type<unit> unit_transform(const unit &) 
        { 
            std::function<container_t<result_t>(const std::function<container_t<result_t>(const unit&)>&)> call = [] (const std::function<container_t<result_t>(const unit&)> &cont) -> container_t<result_t> { return cont(unit{}); };

            return specific_continuation_type<unit>(call);
        }

        template<typename other_input_t>
        [[nodiscard]]
        constexpr specific_continuation_type<product<input_t, other_input_t>> merge(const specific_continuation_type<other_input_t> &other) const
        {
            auto merging = 
            [&origin = *this, &other] (const std::function<container_t<result_t>(const product<input_t, other_input_t>&)> &call) 
            {
                auto combining = 
                [&origin, &call] (const other_input_t &other_value)
                {
                    return origin.run_cont([&call, &other_value] (const input_t &value) { return call(product<input_t, other_input_t>(value, other_value)); }); 
                };

                return other.run_cont(combining);
            };

            return specific_continuation_type<product<input_t, other_input_t>>(merging);
        }

        template<typename function_t>
        requires fmap_requirement<input_t, function_t>
        [[nodiscard]]
        constexpr specific_continuation_type<typename unwrap_function<function_t>::result_type> fmap(const function_t &map_function) const
        {
            auto mapping = 
            [&origin = *this, &map_function] (const std::function<container_t<result_t>(const typename unwrap_function<function_t>::result_type&)> &call) 
            {
                return origin.run_cont(compose<decltype(call), function_t, input_t>(call, map_function));
            };

            return specific_continuation_type<typename unwrap_function<function_t>::result_type>(mapping);
        }
        
        template<typename function_t>
        requires apply_requirement<input_t, function_t>
        [[nodiscard]]
        constexpr continuation_transformer<result_t, container_t, typename unwrap_function<function_t>::result_type> reverse_apply(const continuation_transformer<result_t, container_t, function_t> &continuation_function) const
        {
            auto reverse_applying = 
            [&origin = *this, &continuation_function] (const std::function<container_t<result_t>(const typename unwrap_function<function_t>::result_type&)> &call) 
            {
                auto mapping = 
                [&origin, &call] (const function_t &map_function)
                {
                    return origin.run_cont(compose<decltype(call), function_t, input_t>(call, map_function));
                };

                return continuation_function.run_cont(mapping);
            };

            return specific_continuation_type<typename unwrap_function<function_t>::result_type>(reverse_applying);
        }

        template<typename other_input_t>
        requires apply_requirement<other_input_t, input_t>
        [[nodiscard]]
        constexpr auto apply(const continuation_transformer<result_t, container_t, other_input_t> &other) const
        {
            auto reverse_applying = 
            [&origin = *this, &other] (const std::function<container_t<result_t>(const typename unwrap_function<input_t>::result_type&)> &call) 
            {
                auto mapping = 
                [&origin, &call] (const other_input_t &other_input)
                {
                    return origin.run_cont([&call, &other_input] (const input_t &input) { return call(input(other_input)); });
                };

                return other.run_cont(mapping);
            };

            return specific_continuation_type<typename unwrap_function<input_t>::result_type>(reverse_applying);
        }

        [[nodiscard]]
        constexpr input_t join() const
        requires has_doubled_type<input_t> && 
        std::same_as<continuation_transformer, typename input_t::doubled_type>
        {
            auto joining =
            [&origin = *this] (const std::function<container_t<result_t>(const typename input_t::input_type&)> &call)
            {
                auto cont = [&call] (const input_t &inner)
                {
                    return inner.run_cont(call);
                };

                return origin.run_cont(cont);
            };

            return input_t(joining);
        }
        
        template<typename function_t>
        requires fmap_requirement<input_t, function_t> && 
        compatible_continuation<continuation_transformer, typename unwrap_function<function_t>::result_type>
        [[nodiscard]]
        constexpr typename unwrap_function<function_t>::result_type bind(const function_t &function) const
        {
            using other_result_type = typename unwrap_function<function_t>::result_type;

            auto binding =
            [&origin = *this, &function] (const std::function<container_t<result_t>(const typename other_result_type::input_type&)> &call)
            {
                auto cont = [&call, &function] (const input_t &inner)
                {
                    return function(inner).run_cont(call);
                };

                return origin.run_cont(cont);
            };

            return typename unwrap_function<function_t>::result_type(binding);
        }
        
        template<typename other_value>
        [[nodiscard]]
        constexpr continuation_transformer<result_t, container_t, other_value> then(const continuation_transformer<result_t, container_t, other_value> &other_continuation) const
        {
            auto ignoring =
            [&origin = *this, &other_continuation] (const std::function<container_t<result_t>(const other_value&)> &call)
            {
                auto cont = [&call, &other_continuation] (input_t)
                {
                    return other_continuation.run_cont(call);
                };

                return origin.run_cont(cont);
            };

            return continuation_transformer<result_t, container_t, other_value>(ignoring);
        }
    };

    template<typename result_t, typename input_t>
    using continuation = continuation_transformer<result_t, identity, input_t>;
}

#endif