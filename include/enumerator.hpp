#include <coroutine>

namespace sayten
{
    template<typename type_t>
    class enumerable;

    template<typename type_t>
    class enumerator
    {
    private:
        type_t current_value;

    public:
        std::suspend_always initial_suspend()
        {                                
            return std::suspend_always();
        }

        std::suspend_always final_suspend()
        {
            return std::suspend_always();
        }

        enumerable<type_t> get_return_object()
        {
            return enumerable<type_t>(std::coroutine_handle<enumerator<type_t>>::from_promise(*this));
        }

        std::suspend_never return_void()
        {
            return std::suspend_never();
        }
      
        std::suspend_always yield_value(const type_t value)
        {
            current_value = value;
            return std::suspend_always();
        }

        void unhandled_exception()
        {
            std::terminate();
        }

        type_t get_current()
        {
            return current_value;
        }
    };
}