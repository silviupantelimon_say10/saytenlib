#ifndef SAYTEN_CONTRAVARIANT_HPP
#define SAYTEN_CONTRAVARIANT_HPP

#include <type_traits>
#include "concepts.hpp"
#include "functor.hpp"

namespace sayten::types
{
    template<typename function_t>
    concept contramap_parameter = fmap_parameter<function_t>;

    template<typename input_t, typename function_t>
    concept contramap_requirement = contramap_parameter<function_t> &&
    std::same_as<input_t, typename unwrap_function<function_t>::result_type>;

    template<template<typename> typename contravariant_t>
    concept has_contramap = 
    requires (contravariant_t<placeholders::placeholder<0>> contravariant)
    {
        { contravariant.contramap(placeholders::placeholder_function<0, 1>) } -> std::convertible_to<contravariant_t<placeholders::placeholder<1>>>;
        { contravariant.contramap(placeholders::placeholder_lambda<0, 1>) } -> std::convertible_to<contravariant_t<placeholders::placeholder<1>>>;
        { contravariant.contramap(placeholders::placeholder_std_function<0, 1>) } -> std::convertible_to<contravariant_t<placeholders::placeholder<1>>>;
    };

    template<template<typename> typename contravariant_t>
    concept contravariant = has_contramap<contravariant_t>;

    template<template<typename> typename contravariant_t, typename function_t>
    requires contravariant<contravariant_t>
    [[nodiscard]]
    contravariant_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> call_contramap(const contravariant_t<typename unwrap_function<function_t>::result_type> &contravariant, const function_t &function)
    {
        return contravariant.contramap(function);
    }
}

#endif