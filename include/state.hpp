#ifndef SAYTEN_STATE_HPP
#define SAYTEN_STATE_HPP

#include "monad.hpp"
#include "product.hpp"
#include "identity.hpp"
#include "function_concepts.hpp"

namespace sayten::types
{
    namespace
    {
        template<typename value_t>
        struct run_state
        {
            static constexpr bool value = false;
        };

        template<typename incapsule_t>
        concept is_run_state_function =
        callable<incapsule_t> &&
        (unwrap_function<incapsule_t>::args_size == 1) &&
        run_state<typename unwrap_function<incapsule_t>::result_type>::value && 
        std::same_as<typename run_state<typename unwrap_function<incapsule_t>::result_type>::state_type, typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>>;
    }

    template<typename state_t, template<typename> typename monad_t, typename output_t, typename incapsule_t = std::function<monad_t<product<output_t, state_t>>(const state_t&)>>
    requires monad<monad_t> && is_run_state_function<incapsule_t>
    class state_transformer;

    namespace
    {
        template<template<typename> typename monad_t, typename output_t, typename state_t>
        requires monad<monad_t>
        struct run_state<monad_t<product<output_t, state_t>>>
        {
            static constexpr bool value = true;

            using output_type = output_t;
            using state_type = state_t;

            template<typename type_t>
            using monad_type = monad_t<type_t>;

            template<typename incapsule_t>
            requires 
            callable<incapsule_t> &&
            (unwrap_function<incapsule_t>::args_size == 1) &&
            std::same_as<state_t, typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>> &&
            std::same_as<typename unwrap_function<incapsule_t>::result_type, monad_t<product<output_t, state_t>>>
            using state_transformer_from = 
            state_transformer<state_t, monad_t, output_t, incapsule_t>;
        };

        template<typename incapsule_t>
        requires is_run_state_function<incapsule_t>
        using state_transformer_from = typename run_state<typename unwrap_function<incapsule_t>::result_type>::state_transformer_from<incapsule_t>;
    
        template<typename type_a_t, typename type_b_t>
        struct is_same_state_eval
        {
            static constexpr bool value = false;
        };

        template<typename output_a_t, typename output_b_t, typename state_t, template<typename> typename monad_a_t, template<typename> typename monad_b_t>
        requires monad<monad_a_t> && monad<monad_b_t>
        struct is_same_state_eval<monad_a_t<product<output_a_t, state_t>>, monad_b_t<product<output_b_t, state_t>>>
        {
            static constexpr bool value = true;
        };
        
        template<typename type_a_t, typename type_b_t>
        concept same_state_eval = is_same_state_eval<type_a_t, type_b_t>::value;

        template<typename type_a_t, typename type_b_t>
        struct is_same_monad_class
        {
            static constexpr bool value = false;
        };

        template<typename output_a_t, typename output_b_t, typename state_t, template<typename> typename monad_t, typename incapsule_a_t, typename incapsule_b_t>
        requires monad<monad_t>
        struct is_same_monad_class<state_transformer<state_t, monad_t, output_a_t, incapsule_a_t>, state_transformer<state_t, monad_t, output_b_t, incapsule_b_t>>
        {
            static constexpr bool value = true;
        };

        template<typename type_a_t, typename type_b_t>
        concept same_monad_class = is_same_monad_class<type_a_t, type_b_t>::value;
    }
    
    template<typename state_t, typename output_t, typename incapsule_t = std::function<identity<product<output_t, state_t>>(const state_t&)>>
    using state = state_transformer<state_t, identity, output_t, incapsule_t>;

    template<typename incapsule_t>
    requires is_run_state_function<incapsule_t>
    [[nodiscard]]
    constexpr state_transformer_from<incapsule_t> make_state(const incapsule_t &test)
    {
        return state_transformer_from<incapsule_t>(test);
    }

    template<typename state_t, template<typename> typename monad_t, typename output_t, typename incapsule_t>
    requires monad<monad_t> && is_run_state_function<incapsule_t>
    class state_transformer
    {
    private:
        adjust_function_t<incapsule_t> run;

    public:
        template<typename other_state_t, template<typename> typename other_monad_t, typename other_output_t, typename other_incapsule_t>
        friend class state_transformer;

        state_transformer(const incapsule_t &run) requires (!needs_adjustment<incapsule_t>) : run(run) {}
        state_transformer(incapsule_t run) requires (needs_adjustment<incapsule_t>) : run(run) {}

        state_transformer &operator=(const state_transformer &other)
        {   
            run = other.run;

            return *this;
        }

        state_transformer(const state_transformer &other)
        {
            *this = other;
        }

        [[nodiscard]]
        constexpr monad_t<product<output_t, state_t>> run_state(const state_t &state) const 
        {
            if constexpr (unwrap_function<incapsule_t>::is_raw_function)
                return (*run)(state);
            else
                return run(state);
        }

        [[nodiscard]]
        constexpr monad_t<product<output_t, state_t>> operator()(const state_t &state) const
        {
            return run_state(state);
        }
        
        [[nodiscard]]
        constexpr monad_t<output_t> eval_state(const state_t &state) const 
        {
            return run_state(state).fmap([] (const product<output_t, state_t> &pair) { return pair.get_first(); });
        }

        [[nodiscard]]
        constexpr monad_t<state_t> exec_state(const state_t &state) const 
        {
            return run_state(state).fmap([] (const product<output_t, state_t> &pair) { return pair.get_second(); });
        }

        template<typename function_t>
        requires callable<function_t> &&
        (unwrap_function<function_t>::args_size == 1) &&
        same_state_eval<typename unwrap_function<function_t>::result_type, typename unwrap_function<function_t>::arg_no_cvref_type<0>>
        [[nodiscard]]
        constexpr auto map_state(const function_t &transfer) const 
        {
            return make_state([&prev = *this, &transfer] (const state_t &state) {
                return transfer(prev.run_state(state));
            });
        }

        template<typename function_t>
        requires callable<function_t> &&
        (unwrap_function<function_t>::args_size == 1) &&
        std::same_as<typename unwrap_function<function_t>::result_type, typename unwrap_function<function_t>::arg_no_cvref_type<0>>
        [[nodiscard]]
        constexpr monad_t<state_t> with_state(const function_t &transfer) const 
        {
            return make_state([&prev = *this, &transfer] (const state_t &state) {
                return prev.run_state(state).fmap([&transfer] (const product<output_t, state_t> &pair) { 
                    return transfer(pair.get_second());
                });
            });
        }

        template<typename function_t>
        requires fmap_requirement<output_t, function_t>
        [[nodiscard]]
        constexpr auto fmap(const function_t &map) const
        {
            return make_state([&prev = *this, &map] (const state_t &state) { 
                return prev.run_state(state).fmap([&map] (const product<output_t, state_t> &pair) { 
                    return pair.first_map([&map] (const output_t &value) { 
                        return map(value); 
                    });
                });
            });
        }

        [[nodiscard]]
        constexpr static auto pure(const output_t &value)
        {
            return make_state([&value] (const state_t &state) { 
                return monad_t<product<output_t, state_t>>::pure(product<output_t, state_t>(value, state));
            });
        }

        template<typename function_t, typename other_incapsule_t>
        requires apply_requirement<output_t, function_t>
        [[nodiscard]]
        constexpr auto reverse_apply(const state_transformer<state_t, monad_t, function_t, other_incapsule_t> &other_state) const
        {
            return make_state([&prev = *this, &other_state] (const state_t &state) {
                return other_state.run_state(state).bind([&prev] (const product<function_t, state_t> &function_wrapper) {
                    return prev.run_state(function_wrapper.get_second()).fmap([&function_wrapper] (const product<output_t, state_t> &result) {
                        return result.first_map([&function_wrapper] (const output_t &output) {
                            return function_wrapper.get_first()(output);
                        });
                    });
                });
            });
        }

        [[nodiscard]]
        constexpr auto join() const
        requires same_monad_class<state_transformer<state_t, monad_t, output_t, incapsule_t>, output_t>
        {
            return make_state([&prev = *this] (const state_t &state) {
                return prev.run_state(state).bind([] (const product<output_t, state_t> &inner) {
                    return inner.get_first().run_state(inner.get_second());
                });
            });
        }

        template<typename function_t>
        requires fmap_requirement<output_t, function_t> &&
        same_monad_class<state_transformer<state_t, monad_t, output_t, incapsule_t>, typename unwrap_function<function_t>::result_type>
        [[nodiscard]]
        constexpr auto bind(const function_t &map) const
        {
            return make_state([&prev = *this, &map] (const state_t &state) { 
                return prev.run_state(state).fmap([&map] (const product<output_t, state_t> &pair) { 
                    return pair.first_map([&map] (const output_t &value) { 
                        return map(value); 
                    });
                });
            }).join();
        }
    };
}

#endif