#ifndef SAYTEN_FUNCTION_VIEW_HPP
#define SAYTEN_FUNCTION_VIEW_HPP

#include "function_concepts.hpp"

namespace sayten
{
    template<typename signature_t>
    requires signature<signature_t>
    class function_view;

    template<typename result_t, typename ...args>
    class function_view<result_t(args...)> final
    {
    private:
        using signature_type = result_t(void *, args...);

        void *source_ptr = nullptr;
        result_t (*function_ptr) (void *, args...) = nullptr;

    public:
        function_view() = delete;
        function_view(const function_view &other) = delete;

        template<typename function_t>
        function_view(function_t function) noexcept
        requires invocable_and_resulting<function_t, result_t, args...> && (!std::same_as<function_t, function_view<result_t(args...)>>)
        {
            if constexpr (unwrap_function<function_t>::is_raw_function) {
                source_ptr = reinterpret_cast<void *>(function);
                function_ptr = [](void *ptr, args... params) -> result_t {
                    return (*reinterpret_cast<function_t>(ptr))(params...);
                };
            } else {
                source_ptr = reinterpret_cast<void *>(std::addressof(function));
                function_ptr = [](void *ptr, args... params) -> result_t {
                    return (*reinterpret_cast<std::add_pointer_t<function_t>>(ptr))(params...);
                };
            }
        }

        inline result_t operator()(args... params) const
        {
            return function_ptr(source_ptr, params...);
        }
    };
}

#endif