#ifndef SAYTEN_TEMPLATE_CONCEPTS_HPP
#define SAYTEN_TEMPLATE_CONCEPTS_HPP

#include <vector>
#include <list>
#include <forward_list>
#include <deque>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>

namespace sayten
{
    template<typename type>
    struct is_template_type
    {
        static constexpr bool value = false;
    };

    template<template<typename...> typename template_type, typename ...args>
    struct is_template_type<template_type<args...>>
    {
        static constexpr bool value = true;
    };

    template<typename type>
    concept is_template = is_template_type<type>::value;

    template<typename template_t, typename value_t, typename replacer_t>
    requires is_template<template_t>
    struct template_argument_replacer;

    template<typename template_t, typename value_t, typename replacer_t>
    requires is_template<template_t>
    using template_argument_replacer_t = template_argument_replacer<template_t, value_t, replacer_t>::result_type;

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::vector<value_t, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::vector<replacer_t, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::list<value_t, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::list<replacer_t, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::forward_list<value_t, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::forward_list<replacer_t, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::deque<value_t, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::deque<replacer_t, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::set<value_t, std::less<value_t>, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::set<replacer_t, std::less<replacer_t>, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::multiset<value_t, std::less<value_t>, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::multiset<replacer_t, std::less<replacer_t>, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::unordered_set<value_t, std::hash<value_t>, std::equal_to<value_t>, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::unordered_set<replacer_t, std::hash<replacer_t>, std::equal_to<replacer_t>, std::allocator<replacer_t>>;
    };

    template<typename value_t, typename replacer_t>
    struct template_argument_replacer<std::unordered_multiset<value_t, std::hash<value_t>, std::equal_to<value_t>, std::allocator<value_t>>, value_t, replacer_t>
    {
        using result_type = std::unordered_multiset<replacer_t, std::hash<replacer_t>, std::equal_to<replacer_t>, std::allocator<replacer_t>>;
    };

    template<typename key_t, typename value_t, typename replacer_key_t, typename replacer_value_t>
    struct template_argument_replacer<std::map<key_t, value_t, std::less<key_t>, std::allocator<std::pair<const key_t, value_t>>>, std::pair<const key_t, value_t>, std::pair<const replacer_key_t, replacer_value_t>>
    {
        using result_type = std::map<replacer_key_t, replacer_value_t, std::less<replacer_key_t>, std::allocator<std::pair<const replacer_key_t, replacer_value_t>>>;
    };

    template<typename key_t, typename value_t, typename replacer_key_t, typename replacer_value_t>
    struct template_argument_replacer<std::multimap<key_t, value_t, std::less<key_t>, std::allocator<std::pair<const key_t, value_t>>>, std::pair<const key_t, value_t>, std::pair<const replacer_key_t, replacer_value_t>>
    {
        using result_type = std::multimap<replacer_key_t, replacer_value_t, std::less<replacer_key_t>, std::allocator<std::pair<const replacer_key_t, replacer_value_t>>>;
    };

    template<typename key_t, typename value_t, typename replacer_key_t, typename replacer_value_t>
    struct template_argument_replacer<std::unordered_map<key_t, value_t, std::hash<key_t>, std::equal_to<key_t>, std::allocator<std::pair<const key_t, value_t>>>, std::pair<const key_t, value_t>, std::pair<const replacer_key_t, replacer_value_t>>
    {
        using result_type = std::unordered_map<replacer_key_t, replacer_value_t, std::hash<replacer_key_t>, std::equal_to<replacer_key_t>, std::allocator<std::pair<const replacer_key_t, replacer_value_t>>>;
    };

    template<typename key_t, typename value_t, typename replacer_key_t, typename replacer_value_t>
    struct template_argument_replacer<std::unordered_multimap<key_t, value_t, std::hash<key_t>, std::equal_to<key_t>, std::allocator<std::pair<const key_t, value_t>>>, std::pair<const key_t, value_t>, std::pair<const replacer_key_t, replacer_value_t>>
    {
        using result_type = std::unordered_multimap<replacer_key_t, replacer_value_t, std::hash<replacer_key_t>, std::equal_to<replacer_key_t>, std::allocator<std::pair<const replacer_key_t, replacer_value_t>>>;
    };

    template<template<typename> typename class_t, typename value_t, typename replacer_t>
    struct template_argument_replacer<class_t<value_t>, value_t, replacer_t>
    {
        using result_type = class_t<replacer_t>;
    };
}

#endif