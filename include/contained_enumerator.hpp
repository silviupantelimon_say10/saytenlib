#ifndef SAYTEN_CONTAINED_ENUMERATOR_HPP
#define SAYTEN_CONTAINED_ENUMERATOR_HPP

#include <coroutine>

namespace sayten
{
    template<typename container_t>
    requires qualified_container<container_t>
    class contained_enumerable;

    template<typename container_t>
    requires qualified_container<container_t>
    class contained_enumerator
    {
    public:
        using value_type = container_t::value_type;

    private:
        std::shared_ptr<value_type> current_value;
        std::exception_ptr error = nullptr;

    public:
        std::suspend_always initial_suspend()
        {                                
            return std::suspend_always();
        }


        std::suspend_always final_suspend() noexcept(true)
        {
            return std::suspend_always();
        }

        contained_enumerable<container_t> get_return_object()
        {
            return contained_enumerable<container_t>(std::coroutine_handle<contained_enumerator<container_t>>::from_promise(*this));
        }

        std::suspend_never return_void()
        {
            return std::suspend_never();
        }
      
        std::suspend_always yield_value(const value_type value)
        {
            current_value = std::make_shared<value_type>(value);
            return std::suspend_always();
        }

        void unhandled_exception()
        {
            error = std::current_exception();
        }

        value_type get_current()
        {
            return *current_value;
        }

        std::exception_ptr get_error()
        {
            return error;
        }

        constexpr bool has_error()
        {
            return error != nullptr;
        }
    };
}

#endif