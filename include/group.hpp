#ifndef SAYTEN_GROUP_HPP
#define SAYTEN_GROUP_HPP

#include "monoid.hpp"

namespace sayten::types
{
    template<typename type_t>
    concept has_inverse_operation = 
    requires (const type_t &value)
    {
        { value.inverse() } -> std::same_as<type_t>;
    };

    template<typename type_t>
    concept is_group = 
    is_monoid<type_t> && has_inverse_operation<type_t>;

    template<typename type_t>
    requires has_inverse_operation<type_t>
    constexpr type_t get_inverse(const type_t &value)
    {
        return value.inverse();
    }

    template<typename type_t, type_t cross_op(const type_t&, const type_t&), type_t identity, type_t inverse_op(const type_t&)>
    struct group
    {
        static const group identity_element;

        type_t value;

        group(const type_t &value) : value(value) {}

        constexpr group cross(const group &other) const
        {
            return group(cross_op(value, other.value));
        }

        constexpr group inverse() const
        {
            return group(inverse_op(value));
        }

        constexpr operator type_t() const 
        {
            return value; 
        }

        constexpr bool operator==(const group &other) const
        requires std::equality_comparable<type_t>
        {
            return value == other.value;
        }

        constexpr bool operator==(const type_t &other) const
        requires std::equality_comparable<type_t>
        {
            return value == other;
        }

        constexpr auto operator<=>(const group &other) const
        requires std::three_way_comparable<type_t>
        {
            return value <=> other.value;
        }

        constexpr auto operator<=>(const type_t &other) const
        requires std::three_way_comparable<type_t>
        {
            return value <=> other;
        }
    };

    template<typename type_t, type_t cross_op(const type_t&, const type_t&), type_t identity, type_t inverse_op(const type_t&)>
    const group<type_t, cross_op, identity, inverse_op> group<type_t, cross_op, identity, inverse_op>::identity_element = group{identity};
}

namespace std
{
    template<typename type_t, type_t cross_op(const type_t&, const type_t&), type_t identity, type_t inverse_op(const type_t&)>
    struct hash<sayten::types::group<type_t, cross_op, identity, inverse_op>>
    {
        size_t operator()(const sayten::types::group<type_t, cross_op, identity, inverse_op> &obj) const noexcept
        {
            hash<type_t> hasher;

            return hasher(obj.value);
        }
    };
}

#endif