#include <optional>
#include <functional>
#include <vector>
#include <deque>
#include <list>
#include <set>
#include <unordered_set>
#include "concepts.hpp"
#include "functor.hpp"
#include "applicative.hpp"
#include "contained_enumerable.hpp"

namespace sayten::types
{
    template<typename derived_t, typename container_t>
    requires qualified_container<container_t>
    class stream_base
    {
    public:
        using generate_type = std::function<contained_enumerable<container_t>()>;
        using container_type = container_t;
        using value_type = container_t::value_type;
        using enumerable_type = contained_enumerable<container_t>;

    protected:
        generate_type enumerable_generator;

        template<typename map_function_t>
        requires fmap_requirement<value_type, map_function_t>
        static contained_enumerable<template_argument_replacer_t<container_t, value_type, typename unwrap_function<map_function_t>::result_type>> do_fmap(contained_enumerable<container_t> enumerable, map_function_t map_function)
        {
            for (value_type value : enumerable.collect()) {
                co_yield map_function(value);
            }

            do {
                if (!enumerable.has_next())
                    break;

                std::optional<value_type> maybe_value = enumerable.move_next();

                if (!maybe_value.has_value())
                    break;

                co_yield map_function(maybe_value.value());
            } while (true);
        }

        [[nodiscard]]
        static contained_enumerable<template_argument_replacer_t<container_t, value_type, unit>> do_unit_transform(const unit &value)
        {
            co_yield value;
        }

        template<typename other_value_t>
        [[nodiscard]]
        static contained_enumerable<template_argument_replacer_t<container_t, value_type, product<value_type, other_value_t>>> do_merge(contained_enumerable<container_t> enumerable, contained_enumerable<template_argument_replacer_t<container_t, value_type, other_value_t>> other_enumerable)
        {
            for (value_type value : enumerable.collect()) {
                for (other_value_t other_value : other_enumerable.collect()) {
                    co_yield product<value_type, other_value_t>(value, other_value);
                }

                do {
                    if (!other_enumerable.has_next())
                        break;

                    std::optional<other_value_t> maybe_other_value = other_enumerable.move_next();

                    if (!maybe_other_value.has_value())
                        break;

                    co_yield product<value_type, other_value_t>(value, maybe_other_value.value());
                } while (true);
            }

            do {
                if (!enumerable.has_next())
                    break;

                std::optional<value_type> maybe_value = enumerable.move_next();

                if (!maybe_value.has_value())
                    break;

                for (other_value_t other_value : other_enumerable.collect()) {
                    co_yield product<value_type, other_value_t>(maybe_value.value(), other_value);
                }

                do {
                    if (!other_enumerable.has_next())
                        break;

                    std::optional<other_value_t> maybe_other_value = other_enumerable.move_next();

                    if (!maybe_other_value.has_value())
                        break;

                    co_yield product<value_type, other_value_t>(maybe_value.value(), maybe_other_value.value());
                } while (true);
            } while (true);
        }

        static contained_enumerable<container_t> do_pure(value_type value)
        {
            co_yield value;
        }

        template<typename function_t>
        requires apply_requirement<value_type, function_t>
        static contained_enumerable<template_argument_replacer_t<container_t, value_type, typename unwrap_function<function_t>::result_type>> do_reverse_apply(contained_enumerable<container_t> enumerable, contained_enumerable<template_argument_replacer_t<container_t, value_type, function_t>> applicative_enumerable)
        {
            for (value_type value : enumerable.collect()) {
                for (function_t function : applicative_enumerable.collect()) {
                    co_yield function(value);
                }

                do {
                    if (!applicative_enumerable.has_next())
                        break;

                    std::optional<function_t> maybe_function = applicative_enumerable.move_next();

                    if (!maybe_function.has_value())
                        break;

                    co_yield maybe_function.value()(value);
                } while (true);
            }

            do {
                if (!enumerable.has_next())
                    break;

                std::optional<value_type> maybe_value = enumerable.move_next();

                if (!maybe_value.has_value())
                    break;

                for (function_t function : applicative_enumerable.collect()) {
                    co_yield function(maybe_value.value());
                }

                do {
                    if (!applicative_enumerable.has_next())
                        break;

                    std::optional<function_t> maybe_function = applicative_enumerable.move_next();

                    if (!maybe_function.has_value())
                        break;

                    co_yield maybe_function.value()(maybe_value.value());
                } while (true);
            } while (true);
        }

        template<typename function_t>
        requires lift2_requirement<value_type, function_t>
        static contained_enumerable<template_argument_replacer_t<container_t, value_type, typename unwrap_function<function_t>::result_type>> do_lift2(contained_enumerable<container_t> enumerable_a, function_t function, contained_enumerable<template_argument_replacer_t<container_t, value_type, typename unwrap_function<function_t>::arg_no_cvref_type<1>>> enumerable_b)
        {
            using other_value_t = typename unwrap_function<function_t>::arg_no_cvref_type<1>;

            for (value_type value : enumerable_a.collect()) {
                for (other_value_t other_value : enumerable_b.collect()) {
                    co_yield function(value, other_value);
                }

                do {
                    if (!enumerable_b.has_next())
                        break;

                    std::optional<other_value_t> maybe_other_value = enumerable_b.move_next();

                    if (!maybe_other_value.has_value())
                        break;

                    co_yield function(value, maybe_other_value.value());
                } while (true);
            }

            do {
                if (!enumerable_a.has_next())
                    break;

                std::optional<value_type> maybe_value = enumerable_a.move_next();

                if (!maybe_value.has_value())
                    break;

                for (other_value_t other_value : enumerable_b.collect()) {
                    co_yield function(maybe_value.value(), other_value);
                }

                do {
                    if (!enumerable_b.has_next())
                        break;

                    std::optional<other_value_t> maybe_other_value = enumerable_b.move_next();

                    if (!maybe_other_value.has_value())
                        break;

                    co_yield function(maybe_value.value(), maybe_other_value.value());
                } while (true);
            } while (true);
        }

        template<typename new_derived_t, typename new_value_t>
        requires with_value_type<new_value_t>
        using replace_value_type_t = typename new_derived_t::replace_value_type_t<typename new_value_t::value_type>;

        template<typename new_derived_t, typename new_value_t>
        using replace_value_type_for_enumerable_t = contained_enumerable<typename replace_value_type_t<new_derived_t, new_value_t>::container_type>;

        template<typename new_value_t = value_type, typename new_derived_t = derived_t>
        requires
        std::same_as<new_value_t, value_type> &&
        same_template_class_as<new_value_t, derived_t>
        static replace_value_type_for_enumerable_t<new_derived_t, new_value_t> do_join(contained_enumerable<typename new_derived_t::container_type> enumerable) {
            for (new_value_t value : enumerable.collect()) {
                replace_value_type_for_enumerable_t<new_derived_t, new_value_t> embedded_enumerable = value();

                for (get_template_parameter_t<0, new_value_t> embedded_value : embedded_enumerable.collect()) {
                    co_yield embedded_value;
                }

                do {
                    if (!embedded_enumerable.has_next())
                        break;

                    std::optional<get_template_parameter_t<0, new_value_t>> maybe_embedded_value = embedded_enumerable.move_next();

                    if (!maybe_embedded_value.has_value())
                        break;

                    co_yield maybe_embedded_value.value();
                } while (true);
            }

            do {
                if (!enumerable.has_next())
                    break;

                std::optional<new_value_t> maybe_value = enumerable.move_next();

                if (!maybe_value.has_value())
                    break;

                replace_value_type_for_enumerable_t<new_derived_t, new_value_t> embedded_enumerable = maybe_value.value()();

                for (get_template_parameter_t<0, new_value_t> embedded_value : embedded_enumerable.collect()) {
                    co_yield embedded_value;
                }

                do {
                    if (!embedded_enumerable.has_next())
                        break;

                    std::optional<get_template_parameter_t<0, new_value_t>> maybe_embedded_value = embedded_enumerable.move_next();

                    if (!maybe_embedded_value.has_value())
                        break;

                    co_yield maybe_embedded_value.value();
                } while (true);
            } while (true);
        }
        
        template<typename function_t>
        requires
        callable<function_t> && 
        (unwrap_function<function_t>::args_size == 1) &&
        std::same_as<typename unwrap_function<function_t>::arg_type<0>, value_type> &&
        (!std::is_void_v<typename unwrap_function<function_t>::result_type>) &&
        same_template_class_as<typename unwrap_function<function_t>::result_type, derived_t>
        static contained_enumerable<typename unwrap_function<function_t>::result_type::container_type> do_bind(contained_enumerable<container_t> enumerable, function_t function)
        {
            using result_stream_t = typename unwrap_function<function_t>::result_type;

            for (value_type value : enumerable.collect()) {
                contained_enumerable<typename result_stream_t::container_type> embedded_enumerable = function(value)();

                for (typename result_stream_t::value_type embedded_value : embedded_enumerable.collect()) {
                    co_yield embedded_value;
                }

                do {
                    if (!embedded_enumerable.has_next())
                        break;

                    std::optional<typename result_stream_t::value_type> maybe_embedded_value = embedded_enumerable.move_next();

                    if (!maybe_embedded_value.has_value())
                        break;

                    co_yield maybe_embedded_value.value();
                } while (true);
            }

            do {
                if (!enumerable.has_next())
                    break;

                std::optional<value_type> maybe_value = enumerable.move_next();

                if (!maybe_value.has_value())
                    break;

                contained_enumerable<typename result_stream_t::container_type> embedded_enumerable = function(maybe_value.value())();

                for (typename result_stream_t::value_type embedded_value : embedded_enumerable.collect()) {
                    co_yield embedded_value;
                }

                do {
                    if (!embedded_enumerable.has_next())
                        break;

                    std::optional<typename result_stream_t::value_type> maybe_embedded_value = embedded_enumerable.move_next();

                    if (!maybe_embedded_value.has_value())
                        break;

                    co_yield maybe_embedded_value.value();
                } while (true);
            } while (true);
        }

    public:
        template<typename other_derived_t, typename other_container_t>
        friend class stream_base;

        stream_base(const generate_type &enumerable_generator) : enumerable_generator(enumerable_generator) {}

        constexpr contained_enumerable<container_t> operator()()
        {
            return enumerable_generator();
        }

        template<typename map_function_t>
        requires fmap_requirement<value_type, map_function_t>
        auto fmap(const map_function_t &map_function)
        {
            auto mapping = [enumerable_generator = enumerable_generator, map_function] () { return do_fmap<map_function_t>(enumerable_generator(), map_function); };

            return typename derived_t::replace_value_type_t<typename unwrap_function<map_function_t>::result_type>(mapping);
        }

        [[nodiscard]]
        static constexpr auto unit_transform(const unit &value)
        {
            auto make_unit_transform = [&value] () { return do_unit_transform(value); };

            return typename derived_t::replace_value_type_t<unit>(make_unit_transform);
        }

        template<typename other_stream_t>
        [[nodiscard]]
        constexpr auto merge(const other_stream_t &other_stream)
        {
            auto merging = [&enumerable_generator = enumerable_generator, &other_stream] () { return do_merge<typename other_stream_t::value_type>(enumerable_generator(), other_stream.enumerable_generator()); };
        
            return typename derived_t::replace_value_type_t<product<value_type, typename other_stream_t::value_type>>(merging);
        }

        static constexpr derived_t pure(value_type value)
        {
            auto make_pure = [value = value] () { return do_pure(value); };

            return derived_t(make_pure);
        }

        template<typename applicative_stream_t>
        requires apply_requirement<value_type, typename applicative_stream_t::value_type>
        constexpr auto reverse_apply(applicative_stream_t applicative_stream)
        {
            auto reverse_applying = [enumerable_generator = enumerable_generator, applicative_stream = applicative_stream] () { return do_reverse_apply<typename applicative_stream_t::value_type>(enumerable_generator(), applicative_stream.enumerable_generator()); };
        
            return typename derived_t::replace_value_type_t<typename unwrap_function<typename applicative_stream_t::value_type>::result_type>(reverse_applying);
        }

        template<typename other_stream_t>
        requires apply_requirement<typename other_stream_t::value_type, value_type>
        constexpr auto apply(other_stream_t other_stream)
        {
            return other_stream.reverse_apply(*this);
        }

        template<typename function_t>
        requires lift2_requirement<value_type, function_t>
        constexpr auto lift2(function_t function, template_argument_replacer_t<derived_t, value_type, typename unwrap_function<function_t>::arg_no_cvref_type<1>> other_stream)
        {
            auto lifting = [enumerable_generator = enumerable_generator, function = function, other_stream = other_stream] () { return do_lift2<function_t>(enumerable_generator(), function, other_stream.enumerable_generator()); };

            return typename derived_t::replace_value_type_t<typename unwrap_function<function_t>::result_type>(lifting);
        }

        template<typename new_value_t = value_type>
        requires
        std::same_as<new_value_t, value_type> &&
        same_template_class_as<new_value_t, derived_t>
        constexpr auto join()
        {
            auto joining = [enumerable_generator = enumerable_generator] () { return do_join(enumerable_generator()); };

            return typename derived_t::replace_value_type_t<typename new_value_t::value_type>(joining);
        }

        template<typename function_t>
        requires
        callable<function_t> && 
        (unwrap_function<function_t>::args_size == 1) &&
        std::same_as<typename unwrap_function<function_t>::arg_type<0>, value_type> &&
        (!std::is_void_v<typename unwrap_function<function_t>::result_type>) &&
        same_template_class_as<typename unwrap_function<function_t>::result_type, derived_t>
        constexpr auto bind(function_t function)
        {
            auto binding = [enumerable_generator = enumerable_generator, function = function] () { return do_bind<function_t>(enumerable_generator(), function); };

            return typename unwrap_function<function_t>::result_type(binding);
        }
    };

    template<typename container_t>
    requires qualified_container<container_t>
    class stream : public stream_base<stream<container_t>, container_t>
    {
        using base_type = stream_base<stream<container_t>, container_t>;

    public:
        using generate_type = base_type::generate_type;
        using value_type = base_type::value_type;
        using container_type = base_type::container_type;
        using enumerable_type = base_type::enumerable_type;

        template<typename other_value_t>
        using replace_value_type_t = stream<template_argument_replacer_t<container_t, value_type, other_value_t>>;

        stream(const generate_type &enumerable_generator) : base_type(enumerable_generator) {}
    };

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using vector_stream = stream<std::vector<value_t, allocator_t>>;

    template<typename value_t>
    class vector_stream_functor : public stream_base<vector_stream_functor<value_t>, std::vector<value_t>>
    {
        using base_type = stream_base<vector_stream_functor<value_t>, std::vector<value_t>>;

    public:
        using generate_type = base_type::generate_type;
        using value_type = base_type::value_type;
        using container_type = base_type::container_type;
        using enumerable_type = base_type::enumerable_type;

        template<typename other_value_t>
        using replace_value_type_t = vector_stream_functor<other_value_t>;

        vector_stream_functor(const generate_type &enumerable_generator) : base_type(enumerable_generator) {}
    };

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using list_stream = stream<std::list<value_t, allocator_t>>;

    template<typename value_t>
    class list_stream_functor : public stream_base<list_stream_functor<value_t>, std::list<value_t>>
    {
        using base_type = stream_base<list_stream_functor<value_t>, std::list<value_t>>;

    public:
        using generate_type = base_type::generate_type;
        using value_type = base_type::value_type;
        using container_type = base_type::container_type;
        using enumerable_type = base_type::enumerable_type;

        template<typename other_value_t>
        using replace_value_type_t = list_stream_functor<other_value_t>;

        list_stream_functor(const generate_type &enumerable_generator) : base_type(enumerable_generator) {}
    };

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using forward_list_stream = stream<std::forward_list<value_t, allocator_t>>;

    template<typename value_t>
    class forward_list_stream_functor : public stream_base<forward_list_stream_functor<value_t>, std::forward_list<value_t>>
    {
        using base_type = stream_base<forward_list_stream_functor<value_t>, std::forward_list<value_t>>;

    public:
        using generate_type = base_type::generate_type;
        using value_type = base_type::value_type;
        using container_type = base_type::container_type;
        using enumerable_type = base_type::enumerable_type;

        template<typename other_value_t>
        using replace_value_type_t = forward_list_stream_functor<other_value_t>;

        forward_list_stream_functor(const generate_type &enumerable_generator) : base_type(enumerable_generator) {}
    };

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using deque_stream = stream<std::deque<value_t, allocator_t>>;

    template<typename value_t>
    class deque_stream_functor : public stream_base<deque_stream_functor<value_t>, std::deque<value_t>>
    {
        using base_type = stream_base<deque_stream_functor<value_t>, std::deque<value_t>>;

    public:
        using generate_type = base_type::generate_type;
        using value_type = base_type::value_type;
        using container_type = base_type::container_type;
        using enumerable_type = base_type::enumerable_type;

        template<typename other_value_t>
        using replace_value_type_t = deque_stream_functor<other_value_t>;

        deque_stream_functor(const generate_type &enumerable_generator) : base_type(enumerable_generator) {}
    };
}