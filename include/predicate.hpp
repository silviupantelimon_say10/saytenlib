#ifndef SAYTEN_PREDICATE_HPP
#define SAYTEN_PREDICATE_HPP

#include "function_concepts.hpp"

namespace sayten::types
{
    template<typename input_t, typename incapsule_t = std::function<bool(input_t)>>
    requires
    callable<incapsule_t> &&
    (unwrap_function<incapsule_t>::args_size == 1) &&
    std::same_as<typename unwrap_function<incapsule_t>::result_type, bool> &&
    std::same_as<typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>, input_t>
    class predicate;

    template<typename incapsule_t>
    requires
    callable<incapsule_t> &&
    (unwrap_function<incapsule_t>::args_size == 1) &&
    std::same_as<typename unwrap_function<incapsule_t>::result_type, bool>
    constexpr predicate<typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>, incapsule_t> make_predicate(const incapsule_t &test);

    template<typename input_t, typename incapsule_t>
    requires
    callable<incapsule_t> &&
    (unwrap_function<incapsule_t>::args_size == 1) &&
    std::same_as<typename unwrap_function<incapsule_t>::result_type, bool> &&
    std::same_as<typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>, input_t>
    class predicate
    {
    private:
        adjust_function_t<incapsule_t> test;

    public:
        template<typename other_input_t, typename other_incapsule_t>
        friend class predicate;

        predicate(const incapsule_t &test) requires (!needs_adjustment<incapsule_t>) : test(test) {}
        predicate(incapsule_t test) requires (needs_adjustment<incapsule_t>) : test(test) {}

        predicate &operator=(const predicate &other)
        {   
            test = other.test;

            return *this;
        }

        predicate(const predicate &other)
        {
            *this = other;
        }

        constexpr bool operator()(const input_t &value) const
        {
            if constexpr (unwrap_function<incapsule_t>::is_raw_function)
                return (*test)(value);
            else
                return test(value);
        } 

        template<typename function_t>
        requires contramap_requirement<input_t, function_t>
        constexpr auto contramap(const function_t &contra_map) const
        {
            return make_predicate(compose<adjust_function_t<incapsule_t>, function_t, typename unwrap_function<function_t>::arg_type<0>>(test, contra_map));
        }

        operator predicate<input_t, std::function<bool(input_t)>>() const
        {
            return predicate<input_t, std::function<bool(input_t)>>(this->test);
        }
    };

    template<typename incapsule_t>
    requires
    callable<incapsule_t> &&
    (unwrap_function<incapsule_t>::args_size == 1) &&
    std::same_as<typename unwrap_function<incapsule_t>::result_type, bool>
    constexpr predicate<typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>, incapsule_t> make_predicate(const incapsule_t &test)
    {
        return predicate<typename unwrap_function<incapsule_t>::arg_no_cvref_type<0>, incapsule_t>(test);
    }
}

#endif