#include <ranges>
#include <memory>
#include <optional>
#include <iostream>
#include <concepts>
#include <iterator>

namespace sayten
{
    template<typename type_t>
    class enumerator;

    template<typename type_t>
    class enumerable
    {
    public:
        using promise_type = enumerator<type_t>;
    
    private:
        std::coroutine_handle<promise_type> enumerator_hadle;
    
    public:
        enumerable(std::coroutine_handle<promise_type> handle): enumerator_hadle(handle) {}

        ~enumerable()
        {
            if (enumerator_hadle)
                enumerator_hadle.destroy();
        }

        enumerable(const enumerable &) = delete;
        enumerable &operator=(const enumerable &) = delete;
        enumerable(enumerable &&other) noexcept : enumerator_hadle(other.enumerator_hadle)
        {
            other.enumerator_hadle = nullptr;
        }

        enumerable &operator=(enumerable &&other) noexcept
        {
            enumerator_hadle = other.enumerator_hadle;
            other.enumerator_hadle = nullptr;
            return *this;
        }

        type_t get_value() 
        {
            return enumerator_hadle.promise().get_current();
        }

        bool next()
        {
            enumerator_hadle.resume();
            return !enumerator_hadle.done();
        }

        bool has_next()
        {
            return !enumerator_hadle.done();
        }

        template<template<typename> class U>
        U<type_t> to() {
            U<type_t> container;

            while (next())
                container.push_back(get_value());

            return container;
        }
    };
}