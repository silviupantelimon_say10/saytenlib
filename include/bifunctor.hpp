#ifndef SAYTEN_BIFUNCTOR_HPP
#define SAYTEN_BIFUNCTOR_HPP

#include <type_traits>
#include "concepts.hpp"
#include "placeholder.hpp"
#include "functor.hpp"

namespace sayten::types
{
    template<typename first_function_t, typename second_function_t>
    concept bimap_parameters = 
    fmap_parameter<first_function_t> && fmap_parameter<second_function_t>;

    template<template<typename, typename> typename bifunctor_t>
    concept has_bimap =
    requires (bifunctor_t<placeholders::placeholder<2>, placeholders::placeholder<3>> bifunctor)
    {
        { bifunctor.bimap(placeholders::placeholder_function<0, 2>, placeholders::placeholder_function<1, 3>) } -> std::same_as<bifunctor_t<placeholders::placeholder<0>, placeholders::placeholder<1>>>;
        { bifunctor.bimap(placeholders::placeholder_lambda<0, 2>, placeholders::placeholder_lambda<1, 3>) } -> std::same_as<bifunctor_t<placeholders::placeholder<0>, placeholders::placeholder<1>>>;
        { bifunctor.bimap(placeholders::placeholder_std_function<0, 2>, placeholders::placeholder_std_function<1, 3>) } -> std::same_as<bifunctor_t<placeholders::placeholder<0>, placeholders::placeholder<1>>>;
    };

    template<template<typename, typename> typename bifunctor_t>
    concept has_first_map =
    requires (bifunctor_t<placeholders::placeholder<1>, placeholders::placeholder<2>> bifunctor)
    {
        { bifunctor.first_map(placeholders::placeholder_function<0, 1>) } -> std::same_as<bifunctor_t<placeholders::placeholder<0>, placeholders::placeholder<2>>>;
        { bifunctor.first_map(placeholders::placeholder_lambda<0, 1>) } -> std::same_as<bifunctor_t<placeholders::placeholder<0>, placeholders::placeholder<2>>>;
        { bifunctor.first_map(placeholders::placeholder_std_function<0, 1>) } -> std::same_as<bifunctor_t<placeholders::placeholder<0>, placeholders::placeholder<2>>>;
    };

    template<template<typename, typename> typename bifunctor_t>
    concept has_second_map =
    requires (bifunctor_t<placeholders::placeholder<1>, placeholders::placeholder<2>> bifunctor)
    {
        { bifunctor.second_map(placeholders::placeholder_function<0, 2>) } -> std::same_as<bifunctor_t<placeholders::placeholder<1>, placeholders::placeholder<0>>>;
        { bifunctor.second_map(placeholders::placeholder_lambda<0, 2>) } -> std::same_as<bifunctor_t<placeholders::placeholder<1>, placeholders::placeholder<0>>>;
        { bifunctor.second_map(placeholders::placeholder_std_function<0, 2>) } -> std::same_as<bifunctor_t<placeholders::placeholder<1>, placeholders::placeholder<0>>>;
    };

    template<template<typename, typename> typename bifunctor_t>
    concept bifunctor = has_bimap<bifunctor_t> || (has_first_map<bifunctor_t> && has_second_map<bifunctor_t>);

    namespace
    {
        template<typename class_t>
        struct is_bifunctor_instance
        {
            static constexpr bool value = false;
        };

        template<template<typename, typename> typename bifunctor_t, typename first_value_t, typename second_value_t>
        requires bifunctor<bifunctor_t>
        struct is_bifunctor_instance<bifunctor_t<first_value_t, second_value_t>>
        {
            static constexpr bool value = true;

            using first_value_type = first_value_t;
            using second_value_type = second_value_t;

            template<typename other_first_value_t, typename other_second_value_t>
            using replaced_values_instance_type = bifunctor_t<other_first_value_t, other_second_value_t>;

            template<typename other_first_value_t>
            using replaced_first_value_instance_type = bifunctor_t<other_first_value_t, second_value_t>;

            template<typename other_second_value_t>
            using replaced_second_value_instance_type = bifunctor_t<first_value_t, other_second_value_t>;
        };
    }

    template<typename class_t>
    concept bifunctor_instance = is_bifunctor_instance<class_t>::value;

    template<typename class_t, typename first_value_t, typename second_value_t>
    requires bifunctor_instance<class_t>
    using replaced_values_instance_type = typename is_bifunctor_instance<class_t>::replaced_values_instance_type<first_value_t, second_value_t>;

    template<typename class_t, typename first_value_t>
    requires bifunctor_instance<class_t>
    using replaced_first_value_instance_type = typename is_bifunctor_instance<class_t>::replaced_first_value_instance_type<first_value_t>;

    template<typename class_t, typename second_value_t>
    requires bifunctor_instance<class_t>
    using replaced_second_value_instance_type = typename is_bifunctor_instance<class_t>::replaced_second_value_instance_type<second_value_t>;

    template<template<typename, typename> typename bifunctor_t, typename first_function_t, typename second_function_t>
    requires bimap_parameters<first_function_t, second_function_t> && bifunctor<bifunctor_t>
    constexpr bifunctor_t<typename unwrap_function<first_function_t>::result_type, typename unwrap_function<second_function_t>::result_type> call_bimap(const bifunctor_t<typename unwrap_function<first_function_t>::arg_no_cvref_type<0>, typename unwrap_function<second_function_t>::arg_no_cvref_type<0>> &bifunctor, const first_function_t &first_function, const second_function_t &second_function)
    {
        if constexpr (has_bimap<bifunctor_t>)
            return bifunctor.bimap(first_function, second_function);
        else
            return bifunctor.first_map(first_function).second_map(second_function);
    }
}

#endif