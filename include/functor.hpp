#ifndef SAYTEN_FUNCTOR_HPP
#define SAYTEN_FUNCTOR_HPP

#include <type_traits>
#include "concepts.hpp"
#include "placeholder.hpp"

namespace sayten::types
{
    template<typename function_t>
    concept fmap_parameter = callable<function_t> &&
    (unwrap_function<function_t>::args_size == 1) && 
    (!std::is_void_v<typename unwrap_function<function_t>::result_type>);

    template<typename input_t, typename function_t>
    concept fmap_requirement = fmap_parameter<function_t> && 
    std::same_as<input_t, typename unwrap_function<function_t>::arg_no_cvref_type<0>>;

    template<template<typename> typename functor_t>
    concept has_fmap =
    requires (functor_t<placeholders::placeholder<1>> functor)
    {
        { functor.fmap(placeholders::placeholder_function<0, 1>) } -> std::convertible_to<functor_t<placeholders::placeholder<0>>>;
        { functor.fmap(placeholders::placeholder_lambda<0, 1>) } -> std::convertible_to<functor_t<placeholders::placeholder<0>>>;
        { functor.fmap(placeholders::placeholder_std_function<0, 1>) } -> std::convertible_to<functor_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename functor_t>
    concept functor = has_fmap<functor_t>;

    namespace
    {
        template<typename class_t>
        struct is_functor_instance
        {
            static constexpr bool value = false;
        };

        template<template<typename> typename functor_t, typename value_t>
        requires functor<functor_t>
        struct is_functor_instance<functor_t<value_t>>
        {
            static constexpr bool value = true;

            using value_type = value_t;

            template<typename other_value_t>
            using replaced_value_instance_type = functor_t<other_value_t>;
        };
    }

    template<typename class_t>
    concept functor_instance = is_functor_instance<class_t>::value;

    template<typename class_t>
    requires functor_instance<class_t>
    using functor_instance_value_t = typename is_functor_instance<class_t>::value_type;

    template<typename class_t, typename other_value_t>
    requires functor_instance<class_t>
    using functor_replaced_value_instance_t = typename is_functor_instance<class_t>::replaced_value_instance_type<other_value_t>;

    template<template<typename> typename functor_t, typename function_t>
    requires functor<functor_t> && fmap_parameter<function_t>
    [[nodiscard]]
    constexpr functor_t<typename unwrap_function<function_t>::result_type> call_fmap(const functor_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> &functor, const function_t &function)
    {
        if constexpr (has_fmap<functor_t>)
            return functor.fmap(function);
        else
            return call_apply(functor, functor_t<function_t>::pure(function));
    }
}

namespace sayten::placeholders
{
    template<typename value_t>
    struct placeholder_functor
    {
        template<typename function_t>
        requires sayten::types::fmap_requirement<value_t, function_t>
        [[nodiscard]]
        constexpr placeholder_functor<typename unwrap_function<function_t>::result_type> fmap(const function_t &) const
        {
            return placeholder_functor<typename unwrap_function<function_t>::result_type>();
        }
    };
}

#endif