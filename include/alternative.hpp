#ifndef SAYTEN_ALTERNATIVE_HPP
#define SAYTEN_ALTERNATIVE_HPP

#include "applicative.hpp"

namespace sayten::types
{
    template<template<typename> typename alternative_t>
    concept has_empty =
    requires
    {
        std::same_as<std::remove_cv_t<decltype(alternative_t<placeholders::placeholder<0>>::empty)>, placeholders::placeholder<0>>;
    };

    template<template<typename> typename alternative_t>
    concept has_alternative =
    requires (alternative_t<placeholders::placeholder<0>> alt, alternative_t<placeholders::placeholder<0>> other)
    {
        { alt.alternative(other) } -> std::same_as<alternative_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename alternative_t>
    concept alternative = applicative<alternative_t> && has_empty<alternative_t> && has_alternative<alternative_t>;

    template<template<typename> typename alternative_t, typename value_t>
    requires has_empty<alternative_t>
    constexpr alternative_t<value_t> get_empty()
    {
        return alternative_t<value_t>::empty;
    }

    template<template<typename> typename alternative_t, typename value_t>
    requires has_alternative<alternative_t>
    constexpr alternative_t<value_t> call_alternative(const alternative_t<value_t> &initial, const alternative_t<value_t> &other)
    {
        return initial.alternative(other);
    }
}

#endif