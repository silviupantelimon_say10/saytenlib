#ifndef SAYTEN_IDENTITY_HPP
#define SAYTEN_IDENTITY_HPP

#include <concepts>

#include "functor.hpp"
#include "monoidal.hpp"
#include "applicative.hpp"
#include "monad.hpp"

namespace sayten::types
{
    template<typename value_t>
    class identity
    {
    private:
        value_t value;

    public:
        template<typename other_value_t>
        friend class identity;

        identity() = delete;
        constexpr identity(const value_t &value) : value(value) {}
        constexpr identity &operator=(const identity &other)
        {   
            value = other.value;

            return *this;
        }

        constexpr identity(const identity &other) : value(other.value) {}

        [[nodiscard]]
        constexpr value_t extract() const
        {
            return value;
        }

        [[nodiscard]]
        constexpr bool operator==(const identity &value_b) const
        requires std::equality_comparable<value_t>
        {
            return value == value_b.value;
        }

        [[nodiscard]]
        constexpr auto operator<=>(const identity &value_b) const
        requires std::three_way_comparable<value_t>
        {
            return value <=> value_b.value;
        }

        template<typename map_function_t>
        requires fmap_requirement<value_t, map_function_t>
        [[nodiscard]]
        constexpr identity<typename unwrap_function<map_function_t>::result_type> fmap(const map_function_t &map_function) const
        {
            return identity<typename unwrap_function<map_function_t>::result_type>(map_function(value));
        }

        [[nodiscard]]
        constexpr static identity<unit> unit_transform(const unit &value) 
        { 
            return identity<unit>(value); 
        }

        template<typename other_value_t>
        [[nodiscard]]
        constexpr identity<product<value_t, other_value_t>> merge(const identity<other_value_t> &other) const
        { 
            return identity<product<value_t, other_value_t>>(product<value_t, other_value_t>(value, other.value)); 
        }

        [[nodiscard]]
        constexpr static identity<value_t> pure(const value_t &value)
        {
            return identity<value_t>(value);
        }

        template<typename function_t>
        requires apply_requirement<value_t, function_t>
        [[nodiscard]]
        constexpr identity<typename unwrap_function<function_t>::result_type> reverse_apply(const identity<function_t> &identity_value) const
        {
            return identity<typename unwrap_function<function_t>::result_type>(identity_value.extract()(value));
        }

        template<typename other_value_t>
        requires apply_requirement<other_value_t, value_t>
        [[nodiscard]]
        constexpr auto apply(const identity<other_value_t> &other) const
        {
            return identity<typename unwrap_function<value_t>::result_type>(value(other.value));
        }

        template<typename function_t>
        requires lift2_requirement<value_t, function_t>
        [[nodiscard]]
        constexpr identity<typename unwrap_function<function_t>::result_type> lift2(const function_t &function, const identity<typename unwrap_function<function_t>::arg_no_cvref_type<1>> &identity_value) const
        {
            return identity<typename unwrap_function<function_t>::result_type>(function(value, identity_value.extract()));
        }

        template<typename new_value_t = value_t>
        requires std::same_as<new_value_t, value_t> &&
        of_template<new_value_t, identity>
        [[nodiscard]]
        constexpr identity<get_template_parameter_t<0, new_value_t>> join() const
        {
            return extract();
        }

        template<typename function_t>
        requires bind_requirement<value_t, function_t, identity>
        [[nodiscard]]
        constexpr typename unwrap_function<function_t>::result_type bind(const function_t &function) const
        {
            return function(extract());
        }

        template<typename other_value>
        [[nodiscard]]
        constexpr identity<other_value> then(const identity<other_value> &other_identity) const
        {
            return other_identity;
        }
    };
}

#endif