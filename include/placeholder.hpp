#ifndef SAYTEN_PLACEHOLDER_HPP
#define SAYTEN_PLACEHOLDER_HPP

#include "semigroup.hpp"
#include "monoid.hpp"

namespace sayten::placeholders
{
    template<size_t index>
    struct placeholder
    {
        std::strong_ordering operator<=>(const placeholder&) const
        {
            return std::strong_ordering::equal;
        }

        bool operator==(const placeholder&) const
        {
            return true;
        }
    };

    template<size_t result, size_t ...args>
    placeholder<result> placeholder_function(const placeholder<args>&...)
    {
        return placeholder<result>{};
    }

    template<size_t result, size_t ...args>
    auto placeholder_lambda = [] (const placeholder<args>&...) -> placeholder<result> { return placeholder<result>{}; };

    template<size_t result, size_t ...args>
    std::function<placeholder<result>(const placeholder<args>&...)> placeholder_std_function = placeholder_lambda<result, args...>;

    template<typename result_t, typename ...args>
    result_t placeholder_function_of(const args&...)
    {
        return *static_cast<result_t *>(nullptr);
    }

    template<typename result_t, typename ...args>
    auto placeholder_lambda_of = [] (const args&...) -> result_t { return *static_cast<result_t *>(nullptr); };

    template<typename result_t, typename ...args>
    std::function<result_t(const args&...)> placeholder_std_function_of = placeholder_lambda_of<result_t, args...>;

    template<size_t index>
    using placeholder_semigroup = sayten::types::semigroup<placeholder<index>, placeholder_function<index, index, index>>;

    template<size_t index>
    using placeholder_monoid = sayten::types::monoid<placeholder<index>, placeholder_function<index, index, index>, placeholder<index>{}>;
}

namespace std
{
    template<size_t index>
    struct hash<sayten::placeholders::placeholder<index>>
    {
        size_t operator()(const sayten::placeholders::placeholder<index>&) const
        {
            return 0;
        }
    };
}

#endif