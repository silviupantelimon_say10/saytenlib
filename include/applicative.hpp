#ifndef SAYTEN_APPLICATIVE_HPP
#define SAYTEN_APPLICATIVE_HPP

#include "concepts.hpp"
#include "functor.hpp"
#include "monoidal.hpp"

namespace sayten::types
{
    template<typename function_t>
    concept applicative_function_value = fmap_parameter<function_t>;

    template<typename input_t, typename function_t>
    concept apply_requirement = fmap_requirement<input_t, function_t>;

    template<typename function_t>
    concept lift2_parameter = callable<function_t> &&
    (unwrap_function<function_t>::args_size == 2) &&
    (!std::is_void_v<typename unwrap_function<function_t>::result_type>);

    template<typename input_t, typename function_t>
    concept lift2_requirement = lift2_parameter<function_t> &&
    std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<0>, input_t>;

    template<template<typename> typename applicative_t>
    concept has_pure =
    requires (const placeholders::placeholder<0> &value)
    {
        { applicative_t<placeholders::placeholder<0>>::pure(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename applicative_t>
    concept has_reverse_apply = 
    requires (applicative_t<placeholders::placeholder<1>> applicative, applicative_t<decltype(&placeholders::placeholder_function<0, 1>)> value)
    {
        { applicative.reverse_apply(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    } &&
    requires (applicative_t<placeholders::placeholder<1>> applicative, applicative_t<decltype(placeholders::placeholder_lambda<0, 1>)> value)
    {
        { applicative.reverse_apply(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    } &&
    requires (applicative_t<placeholders::placeholder<1>> applicative, applicative_t<decltype(placeholders::placeholder_std_function<0, 1>)> value)
    {
        { applicative.reverse_apply(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename applicative_t>
    concept has_apply = 
    requires (applicative_t<decltype(&placeholders::placeholder_function<0, 1>)> applicative, applicative_t<placeholders::placeholder<1>> value)
    {
        { applicative.apply(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    } &&
    requires (applicative_t<decltype(placeholders::placeholder_lambda<0, 1>)> applicative, applicative_t<placeholders::placeholder<1>> value)
    {
        { applicative.apply(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    } &&
    requires (applicative_t<decltype(placeholders::placeholder_std_function<0, 1>)> applicative, applicative_t<placeholders::placeholder<1>> value)
    {
        { applicative.apply(value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename applicative_t>
    concept has_lift2 =
    requires (applicative_t<placeholders::placeholder<1>> applicative, applicative_t<placeholders::placeholder<2>> value)
    {
        { applicative.lift2(placeholders::placeholder_function<0, 1, 2>, value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
        { applicative.lift2(placeholders::placeholder_lambda<0, 1, 2>, value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
        { applicative.lift2(placeholders::placeholder_std_function<0, 1, 2>, value) } -> std::same_as<applicative_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename applicative_t>
    concept applicative = monoidal<applicative_t>;

    namespace
    {
        template<typename class_t>
        struct is_applicative_instance
        {
            static constexpr bool value = false;
        };

        template<template<typename> typename applicative_t, typename value_t>
        requires applicative<applicative_t>
        struct is_applicative_instance<applicative_t<value_t>>
        {
            static constexpr bool value = true;

            using value_type = value_t;

            template<typename other_value_t>
            using replaced_value_instance_type = applicative_t<other_value_t>;
        };
    }

    template<typename class_t>
    concept applicative_instance = is_applicative_instance<class_t>::value;

    template<typename class_t>
    requires applicative_instance<class_t>
    using applicative_instance_value_t = typename is_applicative_instance<class_t>::value_type;

    template<typename class_t, typename other_value_t>
    requires applicative_instance<class_t>
    using applicative_replaced_value_instance_t = typename is_applicative_instance<class_t>::replaced_value_instance_type<other_value_t>;

    template<template<typename> typename applicative_t, typename value_t>
    requires monoidal<applicative_t>
    [[nodiscard]]
    constexpr applicative_t<value_t> call_pure(const value_t &value)
    {
        if constexpr (has_pure<applicative_t>)
            return applicative_t<value_t>::pure(value);
        else
            return call_fmap(call_unit_transform<applicative_t, value_t>(unit{}), from_unit(value));
    }

    template<template<typename> typename applicative_t, typename function_t>
    requires monoidal<applicative_t>
    [[nodiscard]]
    constexpr applicative_t<typename unwrap_function<function_t>::result_type> call_reverse_apply(const applicative_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> &applicative, const applicative_t<function_t> &applicative_function)
    {
        if constexpr (has_reverse_apply<applicative_t>)
            return applicative.reverse_apply(applicative_function);
        else if constexpr (has_apply<applicative_t>)
            return applicative_function.apply(applicative);
        else
            return call_fmap(applicative_function.merge(applicative), [] (const product<function_t, typename unwrap_function<function_t>::arg_no_cvref_type<0>> &pair) { return pair.evaluate(); });
    }

    template<template<typename> typename applicative_t, typename function_t>
    requires monoidal<applicative_t>
    [[nodiscard]]
    constexpr applicative_t<typename unwrap_function<function_t>::result_type> call_apply(const applicative_t<function_t> &applicative_function, const applicative_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> &applicative)
    {
        if constexpr (has_apply<applicative_t>)
            return applicative_function.apply(applicative);
        else if constexpr (has_reverse_apply<applicative_t>)
            return applicative.reverse_apply(applicative_function);
        else
            return call_fmap(applicative_function.merge(applicative), [] (const product<function_t, typename unwrap_function<function_t>::arg_no_cvref_type<0>> &pair) { return pair.evaluate(); });
    }

    template<template<typename> typename applicative_t, typename function_t>
    requires applicative<applicative_t> && lift2_parameter<function_t>
    [[nodiscard]]
    constexpr applicative_t<typename unwrap_function<function_t>::result_type> call_lift2(const applicative_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> &applicative, const function_t &function, const applicative_t<typename unwrap_function<function_t>::arg_no_cvref_type<1>> &value)
    {
        if constexpr (has_lift2<applicative_t>) {
            return applicative.lift2(function, value);
        } else if constexpr (has_apply<applicative_t>) {
            return call_fmap(applicative, [&function] (const typename unwrap_function<function_t>::arg_no_cvref_type<0> &a)
                {
                    return [&a, &function] (const typename unwrap_function<function_t>::arg_no_cvref_type<1> &b)
                    {
                        return function(a, b);
                    };
                }).apply(value);
        } else if constexpr (has_reverse_apply<applicative_t>) {
            return applicative.reverse_apply(
                call_fmap(value, [&function] (const typename unwrap_function<function_t>::arg_no_cvref_type<1> &a)
                {
                    return [&a, &function] (const typename unwrap_function<function_t>::arg_no_cvref_type<0> &b)
                    {
                        return function(b, a);
                    };
                })
            );
        } else {
            return call_apply(
                call_fmap(applicative, [&function] (const typename unwrap_function<function_t>::arg_no_cvref_type<0> &a)
                {
                    return [&a, &function] (const typename unwrap_function<function_t>::arg_no_cvref_type<1> &b)
                    {
                        return function(a, b);
                    };
                }), value);
        }
    }
}

namespace sayten::placeholders
{
    template<typename value_t>
    struct placeholder_applicative
    {
        template<typename function_t>
        requires sayten::types::fmap_requirement<value_t, function_t>
        [[nodiscard]]
        constexpr placeholder_applicative<typename unwrap_function<function_t>::result_type> fmap(const function_t &) const
        {
            return placeholder_applicative<typename unwrap_function<function_t>::result_type>();
        }

        [[nodiscard]]
        constexpr static placeholder_applicative<sayten::types::unit> unit_transform(const sayten::types::unit &value) 
        { 
            return placeholder_applicative<sayten::types::unit>(value); 
        }

        template<typename other_value_t>
        [[nodiscard]]
        constexpr placeholder_applicative<sayten::types::product<value_t, other_value_t>> merge(const placeholder_applicative<other_value_t> &other) const
        { 
            return placeholder_applicative<sayten::types::product<value_t, other_value_t>>(); 
        }

        [[nodiscard]]
        static constexpr placeholder_applicative pure(const value_t&)
        {
            return placeholder_applicative{};
        }

        template<typename function_t>
        requires sayten::types::apply_requirement<value_t, function_t>
        [[nodiscard]]
        constexpr placeholder_applicative<typename unwrap_function<function_t>::result_type> reverse_apply(const placeholder_applicative<function_t> &) const
        {
            return placeholder_applicative<typename unwrap_function<function_t>::result_type>{};
        }
    };
}

#endif