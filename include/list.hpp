#ifndef SAYTEN_LIST_MONAD_HPP
#define SAYTEN_LIST_MONAD_HPP

#include <vector>
#include <list>
#include <deque>

#include "functor.hpp"
#include "applicative.hpp"
#include "monad.hpp"
#include "concepts.hpp"
#include "maybe.hpp"
#include "monoid.hpp"
#include "traversable.hpp"

namespace sayten::types
{
    namespace {
        template<typename container_t, typename new_value_type>
        requires qualified_container<container_t>
        struct replace_container_value_type {};

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::vector<value_type>, new_value_type>
        {    
            using type = std::vector<new_value_type>;
        };

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::list<value_type>, new_value_type>
        {    
            using type = std::list<new_value_type>;
        };

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::deque<value_type>, new_value_type>
        {    
            using type = std::deque<new_value_type>;
        };

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::set<value_type>, new_value_type>
        {    
            using type = std::set<new_value_type>;
        };

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::unordered_set<value_type>, new_value_type>
        {    
            using type = std::unordered_set<new_value_type>;
        };

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::multiset<value_type>, new_value_type>
        {    
            using type = std::multiset<new_value_type>;
        };

        template<typename value_type, typename new_value_type>
        struct replace_container_value_type<std::unordered_multiset<value_type>, new_value_type>
        {    
            using type = std::unordered_multiset<new_value_type>;
        };
    }

    template<typename container_t, typename new_value_type>
    using replace_container_value_type_t = typename replace_container_value_type<container_t, new_value_type>::type;

    template<typename container_t>
    requires qualified_container<container_t>
    class list_monad_base
    {
    public:
        using container_type = container_t;
        using value_type = container_t::value_type;

    protected:
        container_t container;

    public:
        template<typename other_container_t>
        friend class list_monad_base;

        list_monad_base() = delete;
        list_monad_base(const container_t &container) : container(container) {}

        const container_t &extract() const
        {
            return container;
        }
        
        template<typename function_t>
        requires fmap_requirement<value_type, function_t>
        [[nodiscard]]
        constexpr auto fmap(const function_t &function) const
        {
            using result_t = typename unwrap_function<function_t>::result_type;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;

            result_container_t mapped;

            for (const auto &value : container) {
                add(mapped, function(value));
            }

            return list_monad_base<result_container_t>(mapped);
        }

        [[nodiscard]]
        constexpr static auto unit_transform(const unit &value) 
        { 
            using result_container_t = replace_container_value_type_t<container_t, unit>;

            return list_monad_base<result_container_t>(result_container_t({value}));
        }

        template<typename monoidal_list_monad_base_t>
        requires same_template_class_as<list_monad_base, monoidal_list_monad_base_t>
        [[nodiscard]]
        constexpr auto merge(const monoidal_list_monad_base_t &other) const
        { 
            using result_t = product<value_type, typename monoidal_list_monad_base_t::value_type>;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;

            result_container_t crossed;

            for (const value_type &value : container) {
                for (const auto &other_value : other.container) {
                    add(crossed, result_t(value, other_value));
                }
            }

            return list_monad_base<result_container_t>(crossed); 
        }

        [[nodiscard]]
        constexpr static list_monad_base pure(const value_type &value)
        {
            return list_monad_base(container_t({value}));
        }

        template<typename applicative_list_monad_base_t>
        requires apply_requirement<value_type, typename applicative_list_monad_base_t::value_type> && 
        same_template_class_as<list_monad_base, applicative_list_monad_base_t>
        constexpr auto reverse_apply(const applicative_list_monad_base_t &function_list_monad_base) const
        {
            using result_t = typename unwrap_function<typename applicative_list_monad_base_t::value_type>::result_type;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;

            result_container_t applied;

            for (const value_type &value : container) {
                for (const auto &function : function_list_monad_base.container) {
                    add(applied, function(value));
                }
            }

            return list_monad_base<result_container_t>(applied);
        }

        template<typename applicative_list_monad_base_t>
        requires apply_requirement<typename applicative_list_monad_base_t::value_type, value_type> && 
        same_template_class_as<list_monad_base, applicative_list_monad_base_t>
        constexpr auto apply(const applicative_list_monad_base_t &function_list_monad_base) const
        {
            using result_t = typename unwrap_function<value_type>::result_type;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;

            result_container_t applied;

            for (const value_type &value : container) {
                for (const auto &function : function_list_monad_base.container) {
                    add(applied, value(function));
                }
            }

            return list_monad_base<result_container_t>(applied);
        }

        template<typename function_t>
        requires lift2_requirement<value_type, function_t>
        constexpr auto lift2(const function_t &function, 
            const list_monad_base<replace_container_value_type_t<container_t, typename unwrap_function<function_t>::arg_no_cvref_type<1>>> &other) const
        {
            using result_t = typename unwrap_function<function_t>::result_type;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;

            result_container_t lifted;
            
            for (const value_type &value : container) {
                for (const auto &other_value : other.container) {
                    add(lifted, function(value, other_value));
                }
            }

            return list_monad_base<result_container_t>(lifted);
        }

        constexpr auto join() const
        requires same_template_class_as<list_monad_base, value_type>
        {
            using inner_container_t = typename value_type::container_type;
            inner_container_t joined;

            for (const auto &inner_list_monad_base : container) {
                for (const auto &value : inner_list_monad_base.container) {
                    add(joined, value);
                }
            }

            return value_type(joined);
        }

        template<typename function_t>
        requires fmap_requirement<value_type, function_t> && 
        same_template_class_as<list_monad_base, typename unwrap_function<function_t>::result_type>
        constexpr auto bind(const function_t &function) const
        {
            using result_container_t = typename unwrap_function<function_t>::result_type;
            typename result_container_t::container_type bound;

            for (const auto &value : container) {
                for (const auto &result_value : function(value).container) {
                    add(bound, result_value);
                }
            }

            return typename unwrap_function<function_t>::result_type(bound);
        }

        template<typename other_t>
        requires same_template_class_as<list_monad_base, other_t>
        constexpr auto then(const other_t &other) const
        {
            return other;
        }

        static const list_monad_base<container_t> empty;
        
        constexpr list_monad_base alternative(const list_monad_base &other) const
        {
            return container.begin() != container.end() ? list_monad_base(container) : other;
        }

        template<typename function_t>
        requires
        callable<function_t> &&
        (unwrap_function<function_t>::args_size == 2) &&
        std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<0>, value_type> &&
        std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<1>, typename unwrap_function<function_t>::result_type>
        constexpr typename unwrap_function<function_t>::result_type foldr(function_t reducer, const typename unwrap_function<function_t>::result_type &init) const
        {
            typename container_t::const_iterator it = container.cbegin();
            typename unwrap_function<function_t>::result_type accumulator = init;

            while (it != container.cend()) {
                accumulator = reducer(*it, accumulator);

                ++it;
            }

            return accumulator;
        }

        template<typename function_t>
        requires
        callable<function_t> &&
        (unwrap_function<function_t>::args_size == 2) &&
        std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<1>, value_type> &&
        std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<0>, typename unwrap_function<function_t>::result_type>
        constexpr typename unwrap_function<function_t>::result_type foldl(function_t reducer, const typename unwrap_function<function_t>::result_type &init) const
        {
            typename container_t::const_iterator it = container.cbegin();
            typename unwrap_function<function_t>::result_type accumulator = init;

            while (it != container.cend()) {
                accumulator = reducer(accumulator, *it);

                ++it;
            }

            return accumulator;
        }

        constexpr value_type fold() const
        requires is_monoid<value_type>
        {
            typename container_t::const_iterator it = container.cbegin();
            auto accumulator = value_type::identity_element;

            while (it != container.cend()) {
                accumulator = it->cross(accumulator);

                ++it;
            }

            return accumulator;
        }

        template<typename function_t>
        requires callable<function_t> &&
        (unwrap_function<function_t>::args_size == 1) &&
        is_monoid<typename unwrap_function<function_t>::result_type>
        constexpr typename unwrap_function<function_t>::result_type fold_map(const function_t &mapper) const
        {
            using result_t = typename unwrap_function<function_t>::result_type;
            typename container_t::const_iterator it = container.cbegin();
            auto accumulator = result_t::identity_element;

            while (it != container.cend()) {
                accumulator = mapper(*it).cross(accumulator);

                ++it;
            }

            return accumulator;
        }

        template<typename function_t>
        requires
        callable<function_t> &&
        (unwrap_function<function_t>::args_size == 2) &&
        std::same_as<value_type, typename unwrap_function<function_t>::result_type> &&
        std::same_as<value_type, typename unwrap_function<function_t>::arg_no_cvref_type<0>> &&
        std::same_as<value_type, typename unwrap_function<function_t>::arg_no_cvref_type<1>>
        constexpr maybe<value_type> reduce(const function_t &reducer) const
        {
            typename container_t::const_iterator it = container.cbegin();

            if (it == container.cend()) {
                return maybe<value_type>();
            }

            value_type accumulator = *it;
            ++it;

            while (it != container.cend()) {
                accumulator = reducer(accumulator, *it);

                ++it;
            }

            return maybe<value_type>(accumulator);
        }
        
        auto sequence() const
        requires applicative_instance<value_type>
        {
            using result_t = applicative_instance_value_t<value_type>;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;
            using new_applicative_t = applicative_replaced_value_instance_t<value_type, list_monad_base<result_container_t>>;

            typename container_t::const_iterator it = container.cbegin();
            new_applicative_t accumulator = new_applicative_t::pure(list_monad_base<result_container_t>(result_container_t{}));

            auto combine = [] (const result_t &a, const list_monad_base<result_container_t> &b)
            {
                result_container_t result_container = b.container;
                add(result_container, a);

                return list_monad_base<result_container_t>(result_container);
            };

            while (it != container.cend()) {
                accumulator = call_lift2(*it, combine, accumulator);

                ++it;
            }

            return accumulator;
        }

        template<typename function_t>
        requires traverse_parameter<function_t> && 
        std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<0>, value_type>
        constexpr auto traverse(const function_t &traverser) const
        {
            using result_applicative_t = typename unwrap_function<function_t>::result_type;
            using result_t = applicative_instance_value_t<result_applicative_t>;
            using result_container_t = replace_container_value_type_t<container_t, result_t>;
            using new_list_t = list_monad_base<result_container_t>;
            using new_applicative_t = applicative_replaced_value_instance_t<result_applicative_t, new_list_t>;

            auto it = container.cbegin();
            new_applicative_t accumulator = new_applicative_t::pure(new_list_t(result_container_t{}));

            auto combine = [] (const result_t &a, const new_list_t &b)
            {
                result_container_t result_container = b.container;
                add(result_container, a);

                return new_list_t(result_container);
            };

            while (it != container.cend()) {
                accumulator = call_lift2(traverser(*it), combine, accumulator);

                ++it;
            }

            return accumulator;
        }

        template<typename predicate_t>
        requires callable<predicate_t> && 
        std::same_as<typename unwrap_function<predicate_t>::arg_no_cvref_type<0>, value_type> &&
        std::same_as<typename unwrap_function<predicate_t>::result_type, bool>
        constexpr list_monad_base filter(const predicate_t &predicate) const
        {
            container_type filtered;

            for (const auto &value : container) {
                if (predicate(value))
                    add(filtered, value);
            }

            return list_monad_base(filtered);
        }

        constexpr maybe<value_type> first() const
        {
            return container.size() > 0 ? maybe<value_type>() : maybe<value_type>(container[0]);
        }

        constexpr bool contains(const value_type &to_find) const
        requires std::equality_comparable<value_type>
        {
            for (const auto &value : container) {
                if (value == to_find)
                    return true;
            }

            return false;
        }

        template<typename predicate_t>
        requires callable<predicate_t> && 
        std::same_as<typename unwrap_function<predicate_t>::arg_no_cvref_type<0>, value_type> &&
        std::same_as<typename unwrap_function<predicate_t>::result_type, bool>
        constexpr maybe<value_type> first(const predicate_t &predicate) const
        {
            for (const auto &value : container) {
                if (predicate(value))
                    return maybe<value_type>(value);
            }

            return maybe<value_type>();
        }

        constexpr maybe<size_t> get_index(const value_type &to_find) const
        requires std::equality_comparable<value_type>
        {
            size_t i = 0;

            for (const auto &value : container) {
                if (value == to_find)
                    return maybe<size_t>(i);
                
                ++i;
            }

            return maybe<size_t>();
        }

        bool operator==(const list_monad_base &other) const
        requires std::equality_comparable<value_type>
        {
            auto it_a = container.cbegin();
            auto it_b = other.container.cbegin();

            for (; it_a != container.cend() && it_b != other.container.cend(); ++it_a, ++it_b) {
                if (*it_a != *it_b) {
                    return false;
                }
            }

            if (it_a != container.cend() && it_b != other.container.cend()) {
                return false;
            }

            return true;
        }

        std::partial_ordering operator<=>(const list_monad_base &other) const
        requires std::three_way_comparable<value_type>
        {
            auto it_a = container.cbegin();
            auto it_b = other.container.cbegin();

            for (; it_a != container.cend() && it_b != other.container.cend(); ++it_a, ++it_b) {
                if (*it_a == *it_b) {
                    continue;
                } else if (*it_a < *it_b) {
                    return std::partial_ordering::less;
                } else if (*it_a > *it_b) {
                    return std::partial_ordering::greater;
                } else {
                    return std::partial_ordering::unordered;
                }
            }

            if (it_a != container.cend()) {
                return std::partial_ordering::greater;
            }

            if (it_b != other.container.cend()) {
                return std::partial_ordering::less;
            }

            return std::partial_ordering::equivalent;
        }
    };

    template<typename container_t>
    const list_monad_base<container_t> list_monad_base<container_t>::empty = list_monad_base(container_t());

    template<typename value_t>
    using vector_monad = list_monad_base<std::vector<value_t>>;
    
    template<typename value_t>
    using list_monad = list_monad_base<std::list<value_t>>;

    template<typename value_t>
    using deque_monad = list_monad_base<std::deque<value_t>>;

    template<typename value_t>
    using set_monad = list_monad_base<std::set<value_t>>;

    template<typename value_t>
    using unordered_set_monad = list_monad_base<std::unordered_set<value_t>>;

    template<typename value_t>
    using multiset_monad = list_monad_base<std::multiset<value_t>>;

    template<typename value_t>
    using unordered_multiset_monad = list_monad_base<std::unordered_multiset<value_t>>;
}

namespace std
{
    template<typename container_t>
    requires sayten::qualified_container<container_t>
    struct hash<sayten::types::list_monad_base<container_t>>
    {
        size_t operator()(const sayten::types::list_monad_base<container_t> &obj) const noexcept
        {
            hash<typename container_t::value_type> hasher;
            size_t result = 0;

            auto end = obj.extract().cend();

            for (auto it = obj.extract().cbegin(); it != end; ++it) {
                result ^= hasher(*it);
            }

            return result;
        }
    };
}

#endif