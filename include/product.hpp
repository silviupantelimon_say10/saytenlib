#ifndef SAYTEN_PRODUCT_HPP
#define SAYTEN_PRODUCT_HPP

#include <tuple>
#include "placeholder.hpp"
#include "bifunctor.hpp"
#include "function_concepts.hpp"

namespace sayten::types
{
    template<typename first_t, typename second_t>
    class product
    {
    private:
        first_t first;
        second_t second;

    public:
        using first_type = first_t;
        using second_type = second_t;

        template<typename other_first_t, typename other_second_t>
        friend class product;

        constexpr product(const first_t &first, const second_t &second) : first(first), second(second) {}
        explicit product(first_t &&first, second_t &&second) : first(first), second(second) {}

        inline first_t get_first() const noexcept
        {
            return first;
        }

        inline second_t get_second() const noexcept
        {
            return second;
        }

        template<typename first_function_t, typename second_function_t>
        requires bimap_parameters<first_function_t, second_function_t>
        constexpr product<typename unwrap_function<first_function_t>::result_type, typename unwrap_function<second_function_t>::result_type> bimap(const first_function_t &first_function, const second_function_t &second_function) const noexcept
        {
            return product<typename unwrap_function<first_function_t>::result_type, typename unwrap_function<second_function_t>::result_type>(first_function(first), second_function(second));
        }

        template<typename first_function_t>
        requires fmap_parameter<first_function_t>
        constexpr product<typename unwrap_function<first_function_t>::result_type, second_t> first_map(const first_function_t &first_function) const
        {
            return product<typename unwrap_function<first_function_t>::result_type, second_t>(first_function(first), second);
        }

        template<typename second_function_t>
        requires fmap_parameter<second_function_t>
        constexpr product<first_t, typename unwrap_function<second_function_t>::result_type> second_map(const second_function_t &second_function) const
        {
            return product<first_t, typename unwrap_function<second_function_t>::result_type>(first, second_function(second));
        }

        constexpr bool operator==(const product &other) const
        requires std::equality_comparable<first_t> && std::equality_comparable<second_t>
        {
            return first == other.first && second == other.second;
        }

        constexpr auto operator<=>(const product &other) const
        requires std::three_way_comparable<first_t> && std::three_way_comparable<second_t>
        {
            if (first <=> other.first == std::strong_ordering::equal) {
                return second <=> other.second;
            }

            return first <=> other.first;
        }

        constexpr auto evaluate() const 
        requires callable<first_t> && 
        (unwrap_function<first_t>::args_size == 1) &&  
        (!std::is_void_v<typename unwrap_function<first_t>::result_type>) && 
        std::same_as<typename unwrap_function<first_t>::arg_no_cvref_type<0>, second_t> 
        { 
            return first(second); 
        }

        constexpr auto reverse_evaluate() const 
        requires callable<second_t> && 
        (unwrap_function<second_t>::args_size == 1) &&  
        (!std::is_void_v<typename unwrap_function<second_t>::result_type>) && 
        std::same_as<first_t, typename unwrap_function<second_t>::arg_no_cvref_type<0>> 
        { 
            return second(first); 
        }

        constexpr auto assoc_first() const
        requires of_template<first_t, product>
        {
            return product<typename first_t::first_type, product<typename first_t::second_type, second_t>>(first.first, product<typename first_t::second_type, second_t>(first.second, second));
        }

        constexpr auto assoc_second() const
        requires of_template<second_t, product>
        {
            return product<product<first_t, typename second_t::first_type>, typename second_t::second_type>(product<first_t, typename second_t::first_type>(first, second.first), second.second);
        }

        constexpr second_t first_unitor() const
        requires std::same_as<first_t, unit>
        {
            return second;
        }

        constexpr first_t second_unitor() const
        requires std::same_as<second_t, unit>
        {
            return first;
        }
    };

    template<typename first_t, typename second_t>
    constexpr product<first_t, second_t> make_product(const first_t &first, const second_t &second)
    {
        return product<first_t, second_t>(first, second);
    }

        namespace
    {
        template<typename class_t>
        struct is_product_instance
        {
            static constexpr bool value = false;
        };

        template<typename value_a_t, typename value_b_t>
        struct is_functor_instance<product<value_a_t, value_b_t>>
        {
            static constexpr bool value = true;

            using first_value_type = value_a_t;
            using second_value_type = value_b_t;
        };
    }

    template<typename class_t>
    concept product_instance = is_functor_instance<class_t>::value;

    template<typename class_t>
    requires product_instance<class_t>
    using product_first_value_type = typename is_functor_instance<class_t>::first_value_type;

    template<typename class_t>
    requires product_instance<class_t>
    using product_second_value_type = typename is_functor_instance<class_t>::second_value_type;
}

namespace sayten::placeholders
{
    template<size_t index_a, size_t index_b> 
    using placeholder_product = sayten::types::product<placeholder<index_a>, placeholder<index_b>>; 
}

namespace std
{
    template<typename value_a_t, typename value_b_t>
    requires sayten::hashed<value_a_t> && sayten::hashed<value_b_t>
    struct hash<sayten::types::product<value_a_t, value_b_t>>
    {
        size_t operator()(const sayten::types::product<value_a_t, value_b_t> &pair) const noexcept
        {
            return hash<value_a_t>()(pair.get_first()) ^ hash<value_b_t>()(pair.get_second());
        }
    };
}

#endif