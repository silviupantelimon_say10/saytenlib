#ifndef SAYTEN_MONOID_HPP
#define SAYTEN_MONOID_HPP

#include "semigroup.hpp"

namespace sayten::types
{
    template<typename type_t>
    concept has_identity_element = 
    requires
    {
        std::same_as<std::remove_cv_t<decltype(type_t::identity_element)>, type_t>;
    };

    template<typename type_t>
    concept is_monoid = 
    is_semigroup<type_t> && has_identity_element<type_t>;

    template<typename type_t>
    requires has_identity_element<type_t>
    constexpr type_t get_identity_element()
    {
        return type_t::identity_element;
    }

    template<typename type_t, type_t cross_op(const type_t&, const type_t&), type_t identity>
    struct monoid
    {
        static const monoid identity_element;

        type_t value;

        monoid(const type_t &value) : value(value) {}
        constexpr monoid cross(const monoid &other) const
        {
            return monoid(cross_op(value, other.value));
        }

        constexpr operator type_t() const 
        {
            return value; 
        }

        constexpr bool operator==(const monoid &other) const
        requires std::equality_comparable<type_t>
        {
            return value == other.value;
        }

        constexpr bool operator==(const type_t &other) const
        requires std::equality_comparable<type_t>
        {
            return value == other;
        }

        constexpr auto operator<=>(const monoid &other) const
        requires std::three_way_comparable<type_t>
        {
            return value <=> other.value;
        }

        constexpr auto operator<=>(const type_t &other) const
        requires std::three_way_comparable<type_t>
        {
            return value <=> other;
        }
    };

    template<typename type_t, type_t cross_op(const type_t&, const type_t&), type_t identity>
    const monoid<type_t, cross_op, identity> monoid<type_t, cross_op, identity>::identity_element = monoid{identity};
}

namespace std
{
    template<typename type_t, type_t cross_op(const type_t&, const type_t&), type_t identity>
    struct hash<sayten::types::monoid<type_t, cross_op, identity>>
    {
        size_t operator()(const sayten::types::monoid<type_t, cross_op, identity> &obj) const noexcept
        {
            hash<type_t> hasher;

            return hasher(obj.value);
        }
    };
}

#endif