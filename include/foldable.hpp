#ifndef SAYTEN_FOLDABLE_HPP
#define SAYTEN_FOLDABLE_HPP

#include "concepts.hpp"
#include "monoid.hpp"

namespace sayten::types
{
    template<typename function_t>
    concept foldr_parameter =
    callable<function_t> &&
    (unwrap_function<function_t>::args_size == 2) &&
    std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<1>, typename unwrap_function<function_t>::result_type>;

    template<typename function_t>
    concept foldl_parameter =
    callable<function_t> &&
    (unwrap_function<function_t>::args_size == 2) &&
    std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<0>, typename unwrap_function<function_t>::result_type>;

    template<typename function_t>
    concept fold_map_parameter =
    callable<function_t> &&
    (unwrap_function<function_t>::args_size == 1) &&
    is_monoid<typename unwrap_function<function_t>::result_type>;

    template<template<typename> typename foldable_t>
    concept has_foldr = 
    requires (foldable_t<placeholders::placeholder<1>> foldable)
    {
        { foldable.foldr(placeholders::placeholder_function<0, 1, 0>, placeholders::placeholder<0>{}) } -> std::same_as<placeholders::placeholder<0>>;
        { foldable.foldr(placeholders::placeholder_lambda<0, 1, 0>, placeholders::placeholder<0>{}) } -> std::same_as<placeholders::placeholder<0>>;
        { foldable.foldr(placeholders::placeholder_std_function<0, 1, 0>, placeholders::placeholder<0>{}) } -> std::same_as<placeholders::placeholder<0>>;
    };

    template<template<typename> typename foldable_t>
    concept has_foldl =
    requires (foldable_t<placeholders::placeholder<1>> foldable)
    {
        { foldable.foldl(placeholders::placeholder_function<0, 0, 1>, placeholders::placeholder<0>{}) } -> std::same_as<placeholders::placeholder<0>>;
        { foldable.foldl(placeholders::placeholder_lambda<0, 0, 1>, placeholders::placeholder<0>{}) } -> std::same_as<placeholders::placeholder<0>>;
        { foldable.foldl(placeholders::placeholder_std_function<0, 0, 1>, placeholders::placeholder<0>{}) } -> std::same_as<placeholders::placeholder<0>>;
    };

    template<template<typename> typename foldable_t>
    concept has_fold =
    requires (foldable_t<placeholders::placeholder_monoid<0>> foldable)
    {
        { foldable.fold() } -> std::same_as<placeholders::placeholder_monoid<0>>;
    };

    template<template<typename> typename foldable_t>
    concept has_fold_map =
    requires (foldable_t<placeholders::placeholder<1>> foldable)
    {
        { foldable.fold_map(placeholders::placeholder_function_of<placeholders::placeholder_monoid<0>, placeholders::placeholder<1>>) } -> std::same_as<placeholders::placeholder_monoid<0>>;
        { foldable.fold_map(placeholders::placeholder_lambda_of<placeholders::placeholder_monoid<0>, placeholders::placeholder<1>>) } -> std::same_as<placeholders::placeholder_monoid<0>>;
        { foldable.fold_map(placeholders::placeholder_std_function_of<placeholders::placeholder_monoid<0>, placeholders::placeholder<1>>) } -> std::same_as<placeholders::placeholder_monoid<0>>;
    };
    
    template<template<typename> typename foldable_t, typename function_t>
    requires has_foldr<foldable_t> && foldr_parameter<function_t>
    typename unwrap_function<function_t>::result_type call_foldr(foldable_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> foldable, function_t folder, typename unwrap_function<function_t>::arg_no_cvref_type<1> init)
    {
        return foldable.foldr(folder, init);
    }

    template<template<typename> typename foldable_t, typename function_t>
    requires has_foldl<foldable_t> && foldl_parameter<function_t>
    typename unwrap_function<function_t>::result_type call_foldl(foldable_t<typename unwrap_function<function_t>::arg_no_cvref_type<1>> foldable, function_t folder, typename unwrap_function<function_t>::arg_no_cvref_type<0> init)
    {
        return foldable.foldl(folder, init);
    }

    template<template<typename> typename foldable_t, typename monoid_t>
    requires has_fold<foldable_t> && is_monoid<monoid_t>
    monoid_t call_fold(foldable_t<monoid_t> foldable)
    {
        return foldable.fold();
    }

    template<template<typename> typename foldable_t, typename function_t>
    requires has_fold_map<foldable_t> && 
    fold_map_parameter<function_t> && 
    is_monoid<typename unwrap_function<function_t>::result_type>
    typename unwrap_function<function_t>::result_type call_fold_map(foldable_t<typename unwrap_function<function_t>::arg_no_cvref_type<0>> foldable, function_t mapper)
    {
        return foldable.fold_map(mapper);
    }

    template<template<typename> typename foldable_t>
    concept foldable = has_foldr<foldable_t> || has_fold_map<foldable_t>;
}

#endif