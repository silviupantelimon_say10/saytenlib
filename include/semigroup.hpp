#ifndef SAYTEN_SEMIGROUP_HPP
#define SAYTEN_SEMIGROUP_HPP

#include <concepts>
#include <functional>

namespace sayten::types
{
    template<typename type_t>
    concept has_cross_operation =
    requires (const type_t &value_a, const type_t &value_b)
    {
        { value_a.cross(value_b) } -> std::same_as<type_t>;
    };

    template<typename type_t>
    concept is_semigroup = has_cross_operation<type_t>;
    

    template<typename type_t>
    requires is_semigroup<type_t>
    constexpr type_t call_cross(const type_t &value_a, const type_t &value_b)
    {
        return value_a.cross(value_b);
    }

    template<typename type_t, type_t operation(const type_t&, const type_t&)>
    struct semigroup
    {
        type_t value;

        semigroup(const type_t &value) : value(value) {}
        constexpr semigroup cross(const semigroup &other) const
        {
            return semigroup(operation(value, other.value));
        }

        constexpr operator type_t() const 
        {
            return value; 
        }

        constexpr bool operator==(const semigroup &other) const
        requires std::equality_comparable<type_t>
        {
            return value == other.value;
        }

        constexpr bool operator==(const type_t &other) const
        requires std::equality_comparable<type_t>
        {
            return value == other;
        }

        constexpr bool operator!=(const semigroup &other) const
        requires std::equality_comparable<type_t>
        {
            return value != other.value;
        }

        constexpr bool operator!=(const type_t &other) const
        requires std::equality_comparable<type_t>
        {
            return value != other;
        }
    };
}

namespace std
{
    template<typename type_t, type_t operation(const type_t&, const type_t&)>
    struct hash<sayten::types::semigroup<type_t, operation>>
    {
        size_t operator()(const sayten::types::semigroup<type_t, operation> &obj) const noexcept
        {
            hash<type_t> hasher;

            return hasher(obj.value);
        }
    };
}

#endif