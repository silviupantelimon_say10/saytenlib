#ifndef SAYTEN_CONTAINER_CONCEPTS_HPP
#define SAYTEN_CONTAINER_CONCEPTS_HPP

#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>

namespace sayten
{
    enum class container_add_type
    {
        push_front,
        push_back,
        insert
    };

    template<typename container_t>
    concept with_value_type = 
    requires ()
    {
        typename container_t::value_type;
    };

    template<typename container_t>
    concept with_key_type = 
    requires ()
    {
        typename container_t::key_type;
    };

    template<typename container_t>
    concept with_allocator_type =
    with_value_type<container_t> &&
    requires ()
    {
        typename container_t::allocator_type;
    } &&
    with_value_type<typename container_t::allocator_type> &&
    std::same_as<typename container_t::value_type, typename container_t::allocator_type::value_type>;

    template<typename container_t>
    concept with_value_compare = 
    with_value_type<container_t> &&
    requires ()
    {
        typename container_t::value_compare;
    } &&
    std::predicate<typename container_t::value_compare, typename container_t::value_type, typename container_t::value_type>;

    template<typename container_t>
    concept with_key_compare = 
    with_key_type<container_t> &&
    requires ()
    {
        typename container_t::key_compare;
    } &&
    std::predicate<typename container_t::key_compare, typename container_t::key_type, typename container_t::key_type>;

    template<typename container_t>
    concept with_key_equal = 
    with_key_type<container_t> &&
    requires ()
    {
        typename container_t::key_equal;
    } &&
    std::predicate<typename container_t::key_equal, typename container_t::key_type, typename container_t::key_type>;

    template<typename container_t>
    concept with_hasher = 
    with_value_type<container_t> &&
    requires ()
    {
        typename container_t::hasher;
    } &&
    requires (typename container_t::hasher hasher, typename container_t::value_type value)
    {
        { hasher(value) } -> std::same_as<size_t>;
    };
    
    template<typename container_t>
    concept with_mapped_type =
    with_value_type<container_t> &&
    with_key_type<container_t> &&
    requires ()
    {
        typename container_t::mapped_type;
    };

    template<typename container_t>
    concept with_iterator =
    requires ()
    {
        typename container_t::iterator;
    };

    template<typename container_t>
    concept with_const_iterator =
    requires ()
    {
        typename container_t::const_iterator;
    };

    template<typename container_t>
    concept with_begin_and_end =
    with_iterator<container_t> &&
    requires (container_t container)
    {
        { container.begin() } -> std::same_as<typename container_t::iterator>;
    } &&
    requires (container_t container)
    {
        { container.end() } -> std::same_as<typename container_t::iterator>;
    };

    template<typename container_t>
    concept with_const_begin_and_end =
    with_const_iterator<container_t> &&
    requires (container_t container)
    {
        { container.cbegin() } -> std::same_as<typename container_t::const_iterator>;
    } &&
    requires (container_t container)
    {
        { container.cend() } -> std::same_as<typename container_t::const_iterator>;
    };

    template<typename container_t>
    concept with_size = 
    requires (container_t container)
    {
        { container.size() } -> std::same_as<size_t>;
    };

    template<typename container_t>
    concept with_find = 
    with_iterator<container_t> &&
    requires (container_t container, typename container_t::value_type value)
    {
        { container.find(value) } -> std::same_as<typename container_t::iterator>;
    };

    template<typename container_t>
    struct has_unique_keys
    {
        static constexpr bool value = false;
    };

    template<typename value_t, typename compare_t, typename allocator_t>
    struct has_unique_keys<std::set<value_t, compare_t, allocator_t>>
    {
        static constexpr bool value = true;
    };

    template<typename value_t, typename hasher_t, typename equal_t, typename allocator_t>
    struct has_unique_keys<std::unordered_set<value_t, hasher_t, equal_t, allocator_t>>
    {
        static constexpr bool value = true;
    };

    template<typename key_t, typename value_t, typename compare_t, typename allocator_t>
    struct has_unique_keys<std::map<key_t, value_t, compare_t, allocator_t>>
    {
        static constexpr bool value = true;
    };

    template<typename key_t, typename value_t, typename hasher_t, typename equal_t, typename allocator_t>
    struct has_unique_keys<std::unordered_map<key_t, value_t, hasher_t, equal_t, allocator_t>>
    {
        static constexpr bool value = true;
    };

    template<typename container_t>
    concept unique_keys = 
    with_key_type<container_t> && has_unique_keys<container_t>::value;

    template<typename container_t>
    concept container_with_push_front = with_value_type<container_t> &&
    requires (container_t container, typename container_t::value_type value)
    {
        container.push_front(value);
    };

    template<typename container_t>
    concept container_with_push_back = with_value_type<container_t> &&
    requires (container_t container, typename container_t::value_type value)
    {
        container.push_back(value);
    };

    template<typename container_t>
    concept container_with_insert = with_value_type<container_t> &&
    requires (container_t container, typename container_t::value_type value)
    {
        container.insert(value);
    };

    template<typename container_t>
    concept container_with_add = container_with_push_front<container_t> || 
                                container_with_push_back<container_t> || 
                                container_with_insert<container_t>;

    template<typename container_t, container_add_type preferred_add_method = container_add_type::push_back> 
    requires container_with_add<container_t>
    void constexpr add(container_t &container, typename container_t::value_type value)
    {
        if constexpr (container_with_push_front<container_t> && preferred_add_method == container_add_type::push_front)
            container.push_front(value);
        else if constexpr (container_with_push_back<container_t> && preferred_add_method == container_add_type::push_back)
            container.push_back(value);
        else if constexpr (container_with_insert<container_t> && preferred_add_method == container_add_type::insert)
            container.insert(value);
        else if constexpr (container_with_push_front<container_t>)
            container.push_front(value);
        else if constexpr (container_with_push_back<container_t>)
            container.push_back(value);
        else if constexpr (container_with_insert<container_t>)
            container.insert(value);
    }

    template<typename container_t>
    concept qualified_container = container_with_add<container_t> &&
    with_begin_and_end<container_t> &&
    with_iterator<container_t> &&
    with_const_begin_and_end<container_t> &&
    with_const_iterator<container_t>;
    
    template<typename container_t>
    concept is_map_container = with_key_type<container_t> && 
    with_mapped_type<container_t> && 
    std::constructible_from<typename container_t::value_type, typename container_t::key_type, typename container_t::mapped_type>;

    template<typename container_t>
    requires with_mapped_type<container_t>
    struct key_of_element;
    
    template<template<typename...> typename class_t, typename ...args>
    requires 
    std::same_as<class_t<args...>, std::map<args...>> || std::same_as<class_t<args...>, std::multimap<args...>> ||
    std::same_as<class_t<args...>, std::unordered_map<args...>> || std::same_as<class_t<args...>, std::unordered_multimap<args...>>
    struct key_of_element<class_t<args...>>
    {
        static constexpr typename class_t<args...>::key_type get(typename class_t<args...>::value_type value)
        {
            return value.first;
        }
    };

    template<typename value_t>
    struct value_type_extractor
    {
        using value_type = value_t;
    };

    template<template<typename, typename...> class container_t, typename value_t, typename ...args>
    struct value_type_extractor<container_t<value_t, args...>>
    {
        using value_type = value_t;
    };

    template<typename result_container_t, typename container_t, container_add_type preferred_add_method = container_add_type::push_back> 
    requires qualified_container<container_t> && 
    qualified_container<result_container_t> && 
    std::same_as<typename container_t::value_type, typename result_container_t::value_type>
    result_container_t morph(const container_t &container)
    {
        result_container_t morphed;

        for (const auto &value : container) {
            add(morphed, value);
        }

        return morphed;
    }
}

#endif