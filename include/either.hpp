#ifndef SAYTEN_EITHER_HPP
#define SAYTEN_EITHER_HPP

#include <variant>
#include <concepts>
#include "placeholder.hpp"
#include "bifunctor.hpp"
#include "applicative.hpp"

namespace sayten::types
{
    template<typename first_t, typename second_t>
    requires (!std::same_as<first_t, second_t>)
    class either
    {
    protected:
        std::variant<first_t, second_t> value;

    public:
        template<typename other_first_t, typename other_second_t>
        friend class either;

        using first_type = first_t;
        using second_type = second_t;

        either(const first_t &first) : value(first) {}
        either(const second_t &second) : value(second) {}

        explicit either(first_t &&first) : value(first) {}
        explicit either(second_t &&second) : value(second) {}

        either(const either &other) : value(other.value) {}
        either &operator=(const either &other) 
        {
            value = other.value;

            return *this;
        };
        
        constexpr bool has_first() const noexcept
        {
            return value.index() == 0;
        }

        constexpr bool has_second() const noexcept
        {
            return value.index() == 1;
        }

        constexpr first_t get_first() const
        {
            return std::get<first_t>(value);
        }

        constexpr second_t get_second() const
        {
            return std::get<second_t>(value);
        }

        constexpr const first_t &first_or(first_t&& value) noexcept
        {
            return has_first() ? std::get<first_t>(value) : value;
        }

        constexpr const second_t &second_or(second_t&& value) noexcept
        {
            return has_second() ? std::get<second_t>(value) : value;
        }

        template<typename function_t>
        requires std::is_invocable<function_t, first_t>::value
        constexpr std::invoke_result_t<function_t, first_t> bind(function_t function)
        {
            return has_first() ? 
            function(get_first()) : 
            std::invoke_result_t<function_t, first_t>(get_second());
        }

        template<typename first_function_t, typename second_function_t>
        requires bimap_parameters<first_function_t, second_function_t>
        constexpr either<typename unwrap_function<first_function_t>::result_type, typename unwrap_function<second_function_t>::result_type> bimap(const first_function_t &first_function, const second_function_t &second_function) const
        {
            return has_first() ? 
            either<typename unwrap_function<first_function_t>::result_type, typename unwrap_function<second_function_t>::result_type>(first_function(get_first())) : 
            either<typename unwrap_function<first_function_t>::result_type, typename unwrap_function<second_function_t>::result_type>(second_function(get_second()));
        }

        template<typename first_function_t>
        requires fmap_parameter<first_function_t>
        constexpr either<typename unwrap_function<first_function_t>::result_type, second_t> first_map(const first_function_t &first_function) const
        {
            return has_first() ? 
            either<typename unwrap_function<first_function_t>::result_type, second_t>(first_function(get_first())) : 
            either<typename unwrap_function<first_function_t>::result_type, second_t>(get_second());
        }

        template<typename second_function_t>
        requires fmap_parameter<second_function_t>
        constexpr either<first_t, typename unwrap_function<second_function_t>::result_type> second_map(const second_function_t &second_function) const
        {
            return has_first() ? 
            either<first_t, typename unwrap_function<second_function_t>::result_type>(get_first()) : 
            either<first_t, typename unwrap_function<second_function_t>::result_type>(second_function(get_second()));
        }
        
        constexpr first_t first_join() const
        requires of_template<first_t, either> &&
        std::same_as<get_template_parameter_t<1, first_t>, second_t>
        {
            return has_first() ? get_first() : first_t(get_second());
        }

        constexpr second_t second_join() const
        requires of_template<second_t, either> &&
        std::same_as<get_template_parameter_t<0, second_t>, first_t>
        {
            return has_first() ? second_t(get_first()) : get_second();
        }

        template<typename first_bind_function_t, typename second_bind_function_t>
        requires callable<first_bind_function_t> && callable<second_bind_function_t> &&
        std::same_as<typename unwrap_function<first_bind_function_t>::result_type, typename unwrap_function<second_bind_function_t>::result_type> &&
        of_template<typename unwrap_function<first_bind_function_t>::result_type, either> &&
        std::same_as<typename unwrap_function<first_bind_function_t>::arg_no_cvref_type<0>, first_t> &&
        std::same_as<typename unwrap_function<second_bind_function_t>::arg_no_cvref_type<0>, second_t>
        constexpr auto bibind(const first_bind_function_t &first_bind_function, const second_bind_function_t &second_bind_function) const
        {
            return has_first() ? first_bind_function(get_first()) : second_bind_function(get_second());
        }

        template<typename first_bind_function_t>
        requires callable<first_bind_function_t> &&
        of_template<typename unwrap_function<first_bind_function_t>::result_type, either> &&
        std::same_as<get_template_parameter_t<1, typename unwrap_function<first_bind_function_t>::result_type>, second_t> &&
        std::same_as<typename unwrap_function<first_bind_function_t>::arg_no_cvref_type<0>, first_t>
        constexpr auto first_bind(const first_bind_function_t &first_bind_function) const
        {
            return has_first() ? first_bind_function(get_first()) : get_second();
        }

        template<typename second_bind_function_t>
        requires callable<second_bind_function_t> &&
        of_template<typename unwrap_function<second_bind_function_t>::result_type, either> &&
        std::same_as<get_template_parameter_t<0, typename unwrap_function<second_bind_function_t>::result_type>, first_t> &&
        std::same_as<typename unwrap_function<second_bind_function_t>::arg_no_cvref_type<0>, second_t>
        constexpr auto second_bind(const second_bind_function_t &second_bind_function) const
        {
            return has_first() ? get_first() : second_bind_function(get_second());
        }

        template<typename combine_t>
        requires callable<combine_t> &&
        (unwrap_function<combine_t>::args_size == 2) &&
        std::same_as<typename unwrap_function<combine_t>::arg_no_cvref_type<0>, first_t> &&
        std::same_as<typename unwrap_function<combine_t>::arg_no_cvref_type<1>, first_t>
        constexpr either<first_t, second_t> combine_first(const combine_t &combine, const either<first_t, second_t> &other) const
        {
            return has_first() && other.has_first() ? 
                either<first_t, second_t>(combine(get_first(), other.get_first())) : 
                either<first_t, second_t>(!has_first() ? get_second() : other.get_second());
        }

        template<typename combine_t>
        requires callable<combine_t> &&
        (unwrap_function<combine_t>::args_size == 2) &&
        std::same_as<typename unwrap_function<combine_t>::arg_no_cvref_type<0>, second_t> &&
        std::same_as<typename unwrap_function<combine_t>::arg_no_cvref_type<1>, second_t>
        constexpr either<first_t, second_t> combine_second(const combine_t &combine, const either<first_t, second_t> &other) const
        {
            return has_second() && other.has_second() ? 
                either<first_t, second_t>(combine(get_second(), other.get_second())) : 
                either<first_t, second_t>(!has_second() ? get_first() : other.get_first());
        }

        template<typename first_apply_t>
        requires apply_requirement<first_apply_t, first_t>
        constexpr either<typename unwrap_function<first_apply_t>::result_type, second_t> apply_first(either<first_apply_t, second_t> function_container)
        {
            if (has_second() || function_container.has_second())
                return either<typename unwrap_function<first_apply_t>::result_type, second_t>(get_second());

            return either<typename unwrap_function<first_apply_t>::result_type, second_t>(function_container.get_first()(get_first()));
        }

        template<typename second_apply_t>
        requires apply_requirement<second_apply_t, second_t>
        constexpr either<first_t, typename unwrap_function<second_apply_t>::result_type> apply_second(either<first_t, second_apply_t> function_container)
        {
            if (has_first() || function_container.has_first())
                return either<first_t, typename unwrap_function<second_apply_t>::result_type>(get_first());

            return either<first_t, typename unwrap_function<second_apply_t>::result_type>(function_container.get_second()(get_second()));
        }

        template<typename first_function_t>
        requires fmap_parameter<first_function_t>
        constexpr inline auto map(const first_function_t &first_function) const
        {
            return first_map(first_function);
        }

        template<typename first_apply_t>
        requires apply_requirement<first_apply_t, first_t>
        constexpr inline auto apply(either<first_apply_t, second_t> function_container)
        {
            return first_apply(function_container);
        }

        constexpr bool operator==(const either &other) const
        requires std::equality_comparable<first_t> && std::equality_comparable<second_t>
        {
            return (has_first() && other.has_first() && get_first() == other.get_first()) || (has_second() && other.has_second() && get_second() == other.get_second());
        }

        constexpr bool operator!=(const either &other) const
        requires std::equality_comparable<first_t> && std::equality_comparable<second_t>
        {
            return !(*this == other);
        }
    };

    template<typename expected_t, typename unexpected_t = std::exception_ptr>
    requires (!std::same_as<expected_t, unexpected_t>)
    class expected : private either<expected_t, unexpected_t>
    {
    public:
        using expected_type = expected_t;
        using unexpected_type = unexpected_t;

        expected(const expected_t &expected) : either<expected_t, unexpected_t>(expected) {}
        expected(const unexpected_t &unexpected) : either<expected_t, unexpected_t>(unexpected) {}

        explicit expected(expected_t &&expected) : either<expected_t, unexpected_t>(expected) {}
        explicit expected(unexpected_t &&unexpected) : either<expected_t, unexpected_t>(unexpected) {}

        expected(const expected &other) : either<expected_t, unexpected_t>(other) {}
        
        constexpr bool has_expected() noexcept
        {
            return either<expected_t, unexpected_t>::has_first();
        }

        constexpr bool has_unexpected() noexcept
        {
            return either<expected_t, unexpected_t>::has_second();
        }

        constexpr expected_t get_expected()
        {
            return either<expected_t, unexpected_t>::get_first();
        }

        constexpr unexpected_t get_unexpected()
        {
            return either<expected_t, unexpected_t>::get_second();
        }

        constexpr const expected_t &expected_or(expected_t&& value) noexcept
        {
            return has_expected() ? std::get<expected_t>(value) : value;
        }

        constexpr const unexpected_t &unexpected_or(unexpected_t&& value) noexcept
        {
            return has_unexpected() ? std::get<unexpected_t>(value) : value;
        }

        template<typename map_t>
        requires std::is_invocable<map_t, expected_t>::value
        expected<std::invoke_result_t<map_t, expected_t>, unexpected_t> map(map_t map_function)
        {
            return has_expected() ? 
            expected<std::invoke_result_t<map_t, expected_t>, unexpected_t>(map_function(get_expected())) : 
            expected<std::invoke_result_t<map_t, expected_t>, unexpected_t>(get_unexpected());
        }

        template<typename apply_t>
        requires std::is_invocable<apply_t, expected_t>::value
        expected<std::invoke_result_t<apply_t, expected_t>, unexpected_t> apply(expected<apply_t, unexpected_t> function_container)
        {
            if (function_container.has_unexpected())
                return expected<std::invoke_result_t<apply_t, expected_t>, unexpected_t>(function_container.get_unexpected());

            return has_expected() ? 
            expected<std::invoke_result_t<apply_t, expected_t>, unexpected_t>(function_container.get_expected()(get_expected())) : 
            expected<std::invoke_result_t<apply_t, expected_t>, unexpected_t>(get_unexpected());
        }

        template<typename function_t>
        requires std::is_invocable<function_t, expected_t>::value
        std::invoke_result_t<function_t, expected_t> bind(function_t function)
        {
            return has_expected() ? 
            function(get_expected()) : 
            std::invoke_result_t<function_t, expected_t>(get_unexpected());
        }
    };
}

namespace sayten::placeholders
{
    template<size_t index_a, size_t index_b> 
    using placeholder_either = sayten::types::either<placeholder<index_a>, placeholder<index_b>>; 
}

#endif