#ifndef SAYTEN_FUNCTION_CONCEPTS_HPP
#define SAYTEN_FUNCTION_CONCEPTS_HPP

#include <functional>
#include <tuple>

namespace sayten
{
    namespace
    {
        template<typename signature_t>
        struct is_signature
        {
            static constexpr bool value = false;
        };

        template<typename result_t, typename ...args>
        struct is_signature<result_t(args...)>
        {
            static constexpr bool value = true;
        };

        template<typename result_t, typename ...args>
        struct is_signature<result_t(args...) noexcept>
        {
            static constexpr bool value = true;
        };
    }

    template<typename signature_t>
    concept signature = is_signature<signature_t>::value;

    template<typename value_t, typename signature_t>
    requires signature<signature_t>
    struct add_parameter_to_signature;

    template<typename value_t, typename result_t, typename ...args>
    struct add_parameter_to_signature<value_t, result_t(args...)>
    {
        using type = result_t(value_t, args...);
    };

    template<typename value_t, typename result_t, typename ...args>
    struct add_parameter_to_signature<value_t, result_t(args...) noexcept>
    {
        using type = result_t(value_t, args...) noexcept;
    };

    template<typename value_t, typename signature_t>
    requires signature<signature_t>
    using add_parameter_to_signature_t = add_parameter_to_signature<value_t, signature_t>::type;

    template<typename type_t>
    concept has_call_operator =
    requires
    {
        &type_t::operator();
    };

    template<typename type_t>
    struct is_callable
    {
        static constexpr bool value = false;
    };

    template<typename result_t, typename ...args>
    struct is_callable<std::function<result_t(args...)>>
    {
        static constexpr bool value = true;
    };

    template<typename result_t, typename ...args>
    struct is_callable<const std::function<result_t(args...)>&>
    {
        static constexpr bool value = true;
    };

    template<typename result_t, typename ...args>
    struct is_callable<result_t(args...)>
    {
        static constexpr bool value = true;
    };

    template<typename result_t, typename ...args>
    struct is_callable<result_t(args...) noexcept>
    {
        static constexpr bool value = true;
    };

    template<typename class_t, typename result_t, typename ...args>
    struct is_callable<result_t (class_t::*)(args...)>
    {
        static constexpr bool value = true;
    };

    template<typename class_t, typename result_t, typename ...args>
    struct is_callable<result_t (class_t::*)(args...) const>
    {
        static constexpr bool value = true;
    };

    template<typename result_t, typename ...args>
    struct is_callable<result_t (*)(args...)>
    {
        static constexpr bool value = true;
    };

    template<typename result_t, typename ...args>
    struct is_callable<result_t (*)(args...) noexcept>
    {
        static constexpr bool value = true;
    };

    template<typename class_t, typename result_t, typename ...args>
    struct is_callable<result_t (class_t::*)(args...) const noexcept>
    {
        static constexpr bool value = true;
    };

    template<typename class_t> 
    requires has_call_operator<class_t>
    struct is_callable<class_t>
    {
        static constexpr bool value = true;
    };

    template<typename class_t>
    concept callable = is_callable<class_t>::value;
    
    template<typename class_t>
    requires callable<class_t>
    struct unwrap_function
    {
        using call_operator_type = decltype(&class_t::operator());

        using result_type = typename unwrap_function<call_operator_type>::result_type;

        static constexpr size_t args_size = unwrap_function<call_operator_type>::args_size;

        template <size_t i>
        using arg_type = typename unwrap_function<call_operator_type>::arg_type<i>;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = false;
        static constexpr bool is_function_pointer = false;
    };

    template<typename result_t, typename ...args> 
    struct unwrap_function<std::function<result_t(args...)>>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = false;
        static constexpr bool is_function_pointer = false;
    };

    template<typename result_t, typename ...args> 
    struct unwrap_function<const std::function<result_t(args...)> &>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = false;
        static constexpr bool is_function_pointer = false;
    };

    template<typename result_t, typename ...args> 
    struct unwrap_function<result_t(args...)>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = false;
    };

    template<typename result_t, typename ...args> 
    struct unwrap_function<result_t(args...) noexcept>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = false;
    };

    template<typename result_t, typename ...args> 
    struct unwrap_function<result_t (*)(args...)>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = true;
    };

    template<typename result_t, typename ...args> 
    struct unwrap_function<result_t (*)(args...) noexcept>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = true;
    };

    template<typename class_t, typename result_t, typename ...args> 
    struct unwrap_function<result_t (class_t::*)(args...)>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = false;
    };

    template<typename class_t, typename result_t, typename ...args> 
    struct unwrap_function<result_t (class_t::*)(args...) const>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = false;
    };

    template<typename class_t, typename result_t, typename ...args> 
    struct unwrap_function<result_t (class_t::*)(args...) const noexcept>
    {
        using result_type = result_t;

        static constexpr size_t args_size = sizeof...(args);

        template <size_t i>
        using arg_type = std::tuple_element<i, std::tuple<args...>>::type;

        template <size_t i>
        using arg_no_cv_type = std::remove_cv_t<arg_type<i>>;

        template <size_t i>
        using arg_no_cvref_type = std::remove_cvref_t<arg_type<i>>;

        static constexpr bool is_raw_function = true;
        static constexpr bool is_function_pointer = false;
    };

    template<typename function_t, typename result_t, typename ...input_args>
    concept invocable_and_resulting =
    requires (function_t function, input_args... args)
    {
        { std::invoke(&function_t::operator(), function, args...) } -> std::same_as<result_t>;
    } || 
    requires (function_t function, input_args... args)
    {
        { std::invoke(function, args...) } -> std::same_as<result_t>;
    };

    template<typename function_a_t, typename function_b_t, typename ...args>
    requires callable<function_a_t> && callable<function_b_t> && 
    (unwrap_function<function_a_t>::args_size == 1) && 
    invocable_and_resulting<function_b_t, typename unwrap_function<function_b_t>::result_type, args...> &&
    std::same_as<typename unwrap_function<function_b_t>::result_type, typename unwrap_function<function_a_t>::arg_no_cvref_type<0>>
    auto compose(const function_a_t &function_a, const function_b_t &function_b)
    {
        return [&function_a, &function_b] (const args &...values) { return function_a(function_b(values...)); };
    }

    template<typename function_a_t, typename function_b_t, typename ...args>
    requires callable<function_a_t> && callable<function_b_t> && 
    (unwrap_function<function_a_t>::args_size == 1) && 
    invocable_and_resulting<function_b_t, typename unwrap_function<function_b_t>::result_type, args...> &&
    std::same_as<typename unwrap_function<function_b_t>::result_type, typename unwrap_function<function_a_t>::arg_type<0>>
    auto compose_and_apply(function_a_t function_a, function_b_t function_b, args... values)
    {
        return function_a(function_b(values...));
    }

    namespace
    {
        template<callable function_t>
        struct adjust_function
        {
            using type = function_t;
        };

        template<callable function_t>
        requires unwrap_function<function_t>::is_raw_function && 
        (!unwrap_function<function_t>::is_function_pointer)
        struct adjust_function<function_t>
        {
            using type = function_t*;
        };
    }

    template<callable function_t>
    using adjust_function_t = typename adjust_function<function_t>::type;

    template<typename function_t>
    concept needs_adjustment =
    callable<function_t> && (!std::same_as<function_t, adjust_function<function_t>>);
}

#endif