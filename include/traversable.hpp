#ifndef SAYTEN_TRAVERSABLE_HPP
#define SAYTEN_TRAVERSABLE_HPP

#include "applicative.hpp"
#include "foldable.hpp"

namespace sayten::types
{
    template<typename function_t>
    concept traverse_parameter = fmap_parameter<function_t> &&
    applicative_instance<typename unwrap_function<function_t>::result_type>;

    template<template<typename> typename traversable_t>
    concept has_traverse =
    requires (traversable_t<placeholders::placeholder<1>> traversable)
    {
        { traversable.traverse(placeholders::placeholder_function_of<placeholders::placeholder_applicative<placeholders::placeholder<0>>, placeholders::placeholder<1>>) } -> std::same_as<placeholders::placeholder_applicative<traversable_t<placeholders::placeholder<0>>>>;
        { traversable.traverse(placeholders::placeholder_lambda_of<placeholders::placeholder_applicative<placeholders::placeholder<0>>, placeholders::placeholder<1>>) } -> std::same_as<placeholders::placeholder_applicative<traversable_t<placeholders::placeholder<0>>>>;
        { traversable.traverse(placeholders::placeholder_std_function_of<placeholders::placeholder_applicative<placeholders::placeholder<0>>, placeholders::placeholder<1>>) } -> std::same_as<placeholders::placeholder_applicative<traversable_t<placeholders::placeholder<0>>>>;
    };

    template<template<typename> typename traversable_t>
    concept has_sequence =
    requires (traversable_t<placeholders::placeholder_applicative<placeholders::placeholder<0>>> traversable)
    {
        { traversable.sequence() } -> std::same_as<placeholders::placeholder_applicative<traversable_t<placeholders::placeholder<0>>>>;
    };

    template<template<typename> typename traversable_t>
    concept traversable = has_traverse<traversable_t> || has_sequence<traversable_t>;
}

#endif