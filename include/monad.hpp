#ifndef SAYTEN_MONAD_HPP
#define SAYTEN_MONAD_HPP

#include "template_concepts.hpp"
#include "applicative.hpp"
#include "concepts.hpp"

namespace sayten::types
{
    template<template<typename> typename container_t, typename function_t>
    concept bind_parameter = callable<function_t> &&
    (unwrap_function<function_t>::args_size == 1) && 
    (!std::is_void_v<typename unwrap_function<function_t>::result_type>) &&
    of_template<typename unwrap_function<function_t>::result_type, container_t>;

    template<typename input_t, typename function_t, template<typename> typename container_t>
    concept bind_requirement = 
    bind_parameter<container_t, function_t> &&
    std::same_as<typename unwrap_function<function_t>::arg_no_cvref_type<0>, input_t>;

    template<template<typename> typename monad_t>
    concept has_join = 
    requires (monad_t<monad_t<placeholders::placeholder<0>>> monad)
    {
        { monad.join() } -> std::same_as<monad_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename monad_t, typename value_t>
    requires has_join<monad_t>
    monad_t<value_t> call_join(monad_t<monad_t<value_t>> monad)
    {
        return monad.join();
    }

    template<template<typename> typename monad_t>
    concept has_bind =
    requires (monad_t<placeholders::placeholder<1>> monad)
    {
        { monad.bind(placeholders::placeholder_function_of<monad_t<placeholders::placeholder<0>>, placeholders::placeholder<1>>) } -> std::same_as<monad_t<placeholders::placeholder<0>>>;
        { monad.bind(placeholders::placeholder_lambda_of<monad_t<placeholders::placeholder<0>>, placeholders::placeholder<1>>) } -> std::same_as<monad_t<placeholders::placeholder<0>>>;
        { monad.bind(placeholders::placeholder_std_function_of<monad_t<placeholders::placeholder<0>>, placeholders::placeholder<1>>) } -> std::same_as<monad_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename monad_t, typename function_t>
    requires has_bind<monad_t>
    typename unwrap_function<function_t>::result_type call_bind(monad_t<typename unwrap_function<function_t>::arg_type<0>> monad, function_t function)
    {
        return monad.bind(function);
    }

    template<template<typename> typename monad_t>
    concept has_then =
    requires (monad_t<placeholders::placeholder<1>> monad, monad_t<placeholders::placeholder<0>> other_monad)
    {
        { monad.then(other_monad) } -> std::same_as<monad_t<placeholders::placeholder<0>>>;
    };

    template<template<typename> typename monad_t, typename value_t, typename other_value_t>
    requires
    has_then<monad_t> || has_bind<monad_t>
    monad_t<other_value_t> call_then(monad_t<value_t> monad, monad_t<other_value_t> other_monad)
    {
        if constexpr (has_then<monad_t>) {
            return monad.then(other_monad);
        } else {
            auto discarding = [other_monad = other_monad] (value_t value) -> monad_t<other_value_t> { return other_monad; };

            return call_bind<monad_t, decltype(discarding)>(monad, discarding);
        }
    }

    template<template<typename> typename monad_t>
    concept monad = 
    ((has_join<monad_t> && functor<monad_t>) || has_bind<monad_t>) && has_pure<monad_t>;

    namespace
    {
        template<typename class_t>
        struct is_monad_instance
        {
            static constexpr bool value = false;
        };

        template<template<typename> typename monad_t, typename value_t>
        requires monad<monad_t>
        struct is_monad_instance<monad_t<value_t>>
        {
            static constexpr bool value = true;

            using value_type = value_t;
            
            template<typename other_value_t>
            using replaced_value_instance_type = monad_t<other_value_t>;
        };
    }

    template<typename class_t>
    concept monad_instance = is_monad_instance<class_t>::value;

    template<typename class_t>
    requires monad_instance<class_t>
    using monad_instance_value_t = typename is_monad_instance<class_t>::value_type;

    template<typename class_t, typename other_value_t>
    requires monad_instance<class_t>
    using monad_replaced_value_instance_t = typename is_monad_instance<class_t>::replaced_value_instance_type<other_value_t>;
}

namespace sayten::placeholders
{
    template<typename value_t>
    struct placeholder_monad
    {
        static constexpr placeholder_monad pure(const value_t&)
        {
            return placeholder_monad{};
        }

        template<typename function_t>
        requires sayten::types::bind_requirement<value_t, function_t, placeholder_monad>
        placeholder_monad<typename unwrap_function<function_t>::result_type> bind(function_t binder) const
        {
            return typename unwrap_function<function_t>::result_type{};
        }
    };
}

#endif