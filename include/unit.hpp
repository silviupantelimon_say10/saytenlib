#ifndef SAYTEN_SINGLETON_HPP
#define SAYTEN_SINGLETON_HPP

#include <compare>

namespace sayten::types
{
    struct unit
    {
        constexpr bool operator==(const unit &) const
        {
            return true;
        }

        constexpr std::strong_ordering operator<=>(const unit &) const
        {
            return std::strong_ordering::equal;
        }
    };

    template<typename value_t>
    constexpr unit to_unit(const value_t &)
    {
        return unit();
    }

    template<typename value_t>
    constexpr auto from_unit(const value_t &value)
    {
        return [value] (const unit &) { return value; };
    }
}

namespace std
{
    template<>
    struct hash<sayten::types::unit>
    {
        size_t operator()(const sayten::types::unit&) const noexcept
        {
            return 0;
        }
    };
}

#endif