#ifndef SAYTEN_CONTAINED_ENUMERABLE_HPP
#define SAYTEN_CONTAINED_ENUMERABLE_HPP

#include <memory>
#include <optional>
#include <concepts>
#include <iterator>
#include <coroutine>

#include <vector>
#include <list>
#include <forward_list>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

#include "concepts.hpp"
#include "contained_enumerator.hpp"

namespace sayten
{
    template<typename container_t>
    requires qualified_container<container_t>
    class contained_enumerator;

    template<typename container_t>
    requires qualified_container<container_t>
    class contained_enumerable
    {
    public:
        using promise_type = contained_enumerator<container_t>;
        using value_type = container_t::value_type;
        using iterator = container_t::iterator;
        using container_type = container_t;
    
    private:
        struct state_t
        {
            container_t container;
            std::coroutine_handle<promise_type> enumerator_hadle;

            state_t(std::coroutine_handle<promise_type> enumerator_hadle) : 
            enumerator_hadle(enumerator_hadle) {}

            ~state_t()
            {
                if (enumerator_hadle)
                    enumerator_hadle.destroy();
            }
        };

        std::shared_ptr<state_t> state_ptr;

        value_type get_value() 
        {
            return state_ptr->enumerator_hadle.promise().get_current();
        }
    
    public:
        contained_enumerable(std::coroutine_handle<promise_type> handle)
        {
            state_ptr = std::shared_ptr<state_t>(new state_t(handle));
        }

        contained_enumerable(const contained_enumerable &other) : state_ptr(other.state_ptr) {}
        contained_enumerable &operator=(const contained_enumerable &other)
        {
            state_ptr = other.state_ptr;

            return *this;
        }

        bool next()
        {
            if (state_ptr->enumerator_hadle.promise().has_error())
                return false;

            state_ptr->enumerator_hadle.resume();

            if (!state_ptr->enumerator_hadle.done()) {
                if constexpr (!unique_keys<container_t>)
                    add(state_ptr->container, get_value());
                else {
                    value_type value = get_value();
                    iterator found;

                    if constexpr (!with_mapped_type<container_t>)
                        found = state_ptr->container.find(value);
                    else
                        found = state_ptr->container.find(key_of_element<container_t>::get(value));
                    
                    if (found == state_ptr->container.end())
                        add(state_ptr->container, value);
                }
                
                return true;
            }

            return false;
        }

        std::optional<value_type> move_next()
        {
            if (state_ptr->enumerator_hadle.promise().has_error())
                return std::optional<value_type>();

            state_ptr->enumerator_hadle.resume();

            if constexpr (!unique_keys<container_t>) {
                if (!state_ptr->enumerator_hadle.done()) {
                    value_type value = get_value();
                    add(state_ptr->container, value);

                    return std::make_optional<value_type>(value);
                }
            } else {
                while (!state_ptr->enumerator_hadle.done()) {
                    value_type value = get_value();
                    iterator found;

                    if constexpr (!with_mapped_type<container_t>)
                        found = state_ptr->container.find(value);
                    else
                        found = state_ptr->container.find(key_of_element<container_t>::get(value));
                    
                    if (found == state_ptr->container.end()) {
                        add(state_ptr->container, value);
                        
                        return std::make_optional<value_type>(value);
                    }

                    state_ptr->enumerator_hadle.resume();
                }
            }

            return std::optional<value_type>();
        }

        bool has_next()
        {
            return !state_ptr->enumerator_hadle.promise().has_error() && !state_ptr->enumerator_hadle.done();
        }
        
        bool has_error()
        {
            return state_ptr->enumerator_hadle.promise().has_error();
        }

        std::exception_ptr get_error()
        {
            return state_ptr->enumerator_hadle.promise().get_error();
        }

        iterator begin()
        {
            while (has_next())
                next();

            return state_ptr->container.begin();
        }

        iterator end()
        {
            while (has_next())
                next();

            return state_ptr->container.end();
        }

        container_t collect()
        {
            return state_ptr->container;
        }

        container_t collect_all()
        {
            while (has_next())
                next();

            return state_ptr->container;
        }
    };

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using vector_enumerable = contained_enumerable<std::vector<value_t, allocator_t>>;
    
    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using list_enumerable = contained_enumerable<std::list<value_t, allocator_t>>;

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using forward_list_enumerable = contained_enumerable<std::forward_list<value_t, allocator_t>>;

    template<typename value_t, typename allocator_t = std::allocator<value_t>>
    using deque_enumerable = contained_enumerable<std::deque<value_t, allocator_t>>;

    template<typename value_t, typename compare_t = std::less<value_t>, typename allocator_t = std::allocator<value_t>>
    using set_enumerable = contained_enumerable<std::set<value_t, compare_t, allocator_t>>;

    template<typename value_t, typename compare_t = std::less<value_t>, typename allocator_t = std::allocator<value_t>>
    using multiset_enumerable = contained_enumerable<std::multiset<value_t, compare_t, allocator_t>>;

    template<typename value_t, typename hasher_t = std::hash<value_t>, typename equal_t = std::equal_to<value_t>, typename allocator_t = std::allocator<value_t>>
    using unordered_set_enumerable = contained_enumerable<std::unordered_set<value_t, hasher_t, equal_t, allocator_t>>;

    template<typename value_t, typename hasher_t = std::hash<value_t>, typename equal_t = std::equal_to<value_t>, typename allocator_t = std::allocator<value_t>>
    using unordered_multiset_enumerable = contained_enumerable<std::unordered_multiset<value_t, hasher_t, equal_t, allocator_t>>;

    template<typename key_t, typename value_t, typename compare_t = std::less<value_t>, typename allocator_t = std::allocator<std::pair<const key_t, value_t>>>
    using map_enumerable = contained_enumerable<std::map<key_t, value_t, compare_t, allocator_t>>;

    template<typename key_t, typename value_t, typename compare_t = std::less<key_t>, typename allocator_t = std::allocator<std::pair<const key_t, value_t>>>
    using multimap_enumerable = contained_enumerable<std::multimap<key_t, value_t, compare_t, allocator_t>>;

    template<typename key_t, typename value_t, typename hasher_t = std::hash<key_t>, typename equal_t = std::equal_to<key_t>, typename allocator_t = std::allocator<std::pair<const key_t, value_t>>>
    using unordered_map_enumerable = contained_enumerable<std::unordered_map<key_t, value_t, hasher_t, equal_t, allocator_t>>;

    template<typename key_t, typename value_t, typename hasher_t = std::hash<key_t>, typename equal_t = std::equal_to<key_t>, typename allocator_t = std::allocator<std::pair<const key_t, value_t>>>
    using unordered_multimap_enumerable = contained_enumerable<std::unordered_multimap<key_t, value_t, hasher_t, equal_t, allocator_t>>;
}

#endif