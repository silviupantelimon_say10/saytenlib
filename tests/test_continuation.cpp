#include <string>
#include <iostream>

#include "test.hpp"
#include "../sayten.hpp"

template<typename input_t>
using cont_str = sayten::types::continuation<std::string, input_t>;

int main()
{
    cont_str<int> cont = cont_str<int>::pure(5);
    cont_str<int> cont2 = cont_str<int>::pure(7);
    auto inc = [] (const int &x) -> long { return x + 1L; };

    cont_str<cont_str<int>> double_cont = cont_str<cont_str<int>>::pure(cont);

    if (cont.fmap(inc).run_cont([] (const long &x) { return sayten::types::identity(std::to_string(x * x)); }).extract() != "36") {
        return 1;
    }

    if (cont.reverse_apply(cont_str<decltype(inc)>::pure(inc)).run_cont([] (const long &x) { return sayten::types::identity(std::to_string(x * x)); }).extract() != "36") {
        return 1;
    }

    if (cont_str<decltype(inc)>::pure(inc).apply(cont).run_cont([] (const long &x) { return sayten::types::identity(std::to_string(x * x)); }).extract() != "36") {
        return 1;
    }

    if (double_cont.join().run_cont([] (const int &x) { return sayten::types::identity(std::to_string(x * x)); }).extract() != "25") {
        return 1;
    }

    if (cont.bind([] (const int &x) { return cont_str<sayten::types::identity<long>>::pure(sayten::types::identity<long>(x + 1)); }).run_cont([] (sayten::types::identity<long> x) -> sayten::types::identity<std::string> { return sayten::types::identity(std::to_string(x.extract() * x.extract())); }).extract() != "36") {
        return 1;
    }

    if (cont.then(cont2).run_cont([] (sayten::types::identity<long> x) { return sayten::types::identity(std::to_string(x.extract() * x.extract())); }).extract() != "49") {
        return 1;
    }

    auto unit_lambda = [] (const sayten::types::identity<sayten::types::unit> &) { return sayten::types::identity(std::string("unit")); };

    assert(cont_str<int>::unit_transform(sayten::types::unit{}).run_cont(unit_lambda) == cont_str<sayten::types::unit>::pure(sayten::types::unit{}).run_cont(unit_lambda), 
           "Contituation unit did not produce the proper value!");

    auto product_lambda = [] (const sayten::types::identity<sayten::types::product<int, std::string>> &prod) { return sayten::types::identity(std::to_string(prod.extract().get_first()) + prod.extract().get_second()); };

    assert(cont_str<int>::pure(1).merge(cont_str<std::string>::pure("test")).run_cont(product_lambda) == sayten::types::identity<std::string>("1test"), 
           "Contituation merge did not produce the proper value!");

    return 0;
}