#include <string>
#include "test.hpp"
#include "../sayten.hpp"

struct not_hashed
{

};

int main()
{
    sayten::types::product<int, std::string> pair1(5, "test");
    sayten::types::product<int, long> pair2(5, 6L);

    assert_const<sayten::types::has_bimap<sayten::types::product>>("Product should have bimap!");
    assert_const<sayten::types::has_first_map<sayten::types::product>>("Product should have first_map!");
    assert_const<sayten::types::has_second_map<sayten::types::product>>("Product should have second_map!");
    assert_const<sayten::types::bifunctor<sayten::types::product>>("Product should be a bifunctor!");
    assert_const<sayten::types::bifunctor_instance<sayten::types::product<int, std::string>>>("Product<int, string> should be a bifunctor instance!");
    assert(pair1.get_first() == 5, "Getting first didn't have the expected value!");
    assert(pair1.get_second() == "test", "Getting second didn't have the expected value!");
    assert(pair1.first_map([] (const int &x) { return x + 1; }) == sayten::types::product<int, std::string>(6, "test") &&
           pair2.first_map([] (const int &x) { return std::to_string(x); }) == sayten::types::product<std::string, long>("5", 6L), "First map didn't produce the expected value!");
    assert(pair1.second_map([] (const std::string &str) { return str + str; }) == sayten::types::product<int, std::string>(5, "testtest") &&
           pair2.second_map([] (const long &x) { return std::to_string(x); }) == sayten::types::product<int, std::string>(5, "6"), "Second map didn't produce the expected value!");
    assert(pair1.bimap([] (const int &x) { return x + 1; }, [] (const std::string &str) { return str + str; }) == sayten::types::product<int, std::string>(6, "testtest") &&
           pair2.bimap([] (const int &x) { return std::to_string(x); }, [] (const long &x) { return std::to_string(x); }) == sayten::types::product<std::string, std::string>("5", "6"), "Bimap didn't produce the expected value!");

    sayten::types::product<sayten::types::product<long, int>, std::string> triple_first(sayten::types::product<long, int>(0L, 1), "test");
    sayten::types::product<long, sayten::types::product<int, std::string>> triple_second(0L, sayten::types::product<int, std::string>(1, "test"));

    assert(triple_first.assoc_first() == triple_second, "The product ((long, int) string) should associate to (long, (int, string)!");
    assert(triple_second.assoc_second() == triple_first, "The product (long, (int, string) should associate to ((long, int) string)!");

    sayten::types::product<sayten::types::unit, int> first_unit(sayten::types::unit(), 1);
    sayten::types::product<int, sayten::types::unit> second_unit(1, sayten::types::unit());

    assert(first_unit.first_unitor() == 1, "The product (unit, int) should reduce to int!");
    assert(second_unit.second_unitor() == 1, "The product (int unit) should reduce to int!");

    assert_const<sayten::hashed<sayten::types::product<int, std::string>>>("The product (int, string) should have a hash!");
    assert_const<!sayten::hashed<sayten::types::product<not_hashed, std::string>>>("The product (not_hashed, string) should not have a hash!");
    assert_const<!sayten::hashed<sayten::types::product<int, not_hashed>>>("The product (int, not_hashed) should not have a hash!");
    assert_const<!sayten::hashed<sayten::types::product<not_hashed, not_hashed>>>("The product (not_hashed, not_hashed) should not have a hash!");

    return 0;
}