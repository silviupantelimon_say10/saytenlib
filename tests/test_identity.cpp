#include "test.hpp"
#include "../sayten.hpp"

std::string double_int_and_string(const std::string &str, const int &x) noexcept
{
    return str + str + std::to_string(2 * x);
}

int main()
{
    auto lambda_func = [] (int x) { return x + 1; };
    auto lambda_bifunc = [] (int x, std::string str) { return std::to_string(x) + str; };
    std::function<std::string(std::string)> simple_func(double_string);
    std::function<std::string(std::string, int)> simple_bifunc(double_int_and_string);
    auto identity_to_string = [] (int x) { return sayten::types::identity(std::to_string(x)); };
    const std::string test_str = "abc";
	const sayten::types::identity<int> identity_one(1);
	const sayten::types::identity<std::string> identity_string(test_str);

    assert_const<sayten::types::functor<sayten::types::identity>>("Template 'identity' should be a functor!");
    assert_const<sayten::types::monoidal<sayten::types::identity>>("Template 'identity' should be a monoidal functor!");
    assert_const<sayten::types::applicative<sayten::types::identity>>("Template 'identity' should be an applicative!");
    assert_const<sayten::types::monad<sayten::types::identity>>("Template 'identity' should be a monad!");
    assert(identity_one.fmap(lambda_func).fmap(int_to_string).fmap(simple_func).extract() == simple_func(int_to_string(lambda_func(1))),
           "The correct 'fmap' result was not produced for the 'identity' functor!");
    assert(sayten::types::call_fmap(identity_one, int_to_string).extract() == int_to_string(1) &&
           sayten::types::call_fmap(sayten::types::identity(int_to_string(1)), simple_func).extract() == simple_func(int_to_string(1)) &&
           sayten::types::call_fmap(identity_one, lambda_func).extract() == lambda_func(1),
           "The correct generic 'fmap' call result was not produced for the 'identity' functor!");
    assert(sayten::types::identity<int>::unit_transform(sayten::types::unit()) == sayten::types::identity(sayten::types::unit()), 
		   "The correct 'unit_transform' was not produced!");
	assert(sayten::types::call_unit_transform<sayten::types::identity, int>(sayten::types::unit()) == sayten::types::identity(sayten::types::unit()), 
		   "The correct generic 'unit_transform' was not produced!");
	assert(identity_one.merge(identity_string) == sayten::types::identity(sayten::types::product(1, test_str)), 
		   "The correct 'merge' was not produced!");
	assert(sayten::types::call_merge<sayten::types::identity, int, std::string>(identity_one, identity_string) == sayten::types::identity(sayten::types::product(1, test_str)), 
		   "The correct generic 'merge' was not produced!");
    assert(sayten::types::identity<int>::pure(1) == identity_one &&
           sayten::types::identity<std::string>::pure(test_str) == identity_string &&
           sayten::types::identity<decltype(lambda_func)>::pure(lambda_func) == sayten::types::identity(lambda_func),
           "The correct 'pure' result was not produced for the 'identity' applicative!");
    assert(sayten::types::identity<int>::pure(1) == identity_one &&
           sayten::types::identity<std::string>::pure(test_str) == identity_string &&
           sayten::types::identity<decltype(lambda_func)>::pure(lambda_func) == sayten::types::identity(lambda_func),
           "The correct 'pure' result was not produced for the 'identity' applicative!");
    assert(sayten::types::call_pure<sayten::types::identity>(1) == identity_one &&
           sayten::types::call_pure<sayten::types::identity>(test_str) == identity_string &&
           sayten::types::call_pure<sayten::types::identity>(lambda_func) == sayten::types::identity(lambda_func),
           "The correct generic 'pure' call result was not produced for the 'identity' applicative!");
    assert(identity_one.reverse_apply(sayten::types::identity<decltype(lambda_func)>::pure(lambda_func)).extract() == lambda_func(1) &&
           identity_one.reverse_apply(sayten::types::identity<std::function<std::string(int)>>::pure(int_to_string)).extract() == int_to_string(1),
           "The correct 'reverse_apply' result was not produced for the 'identity' applicative!");
    assert(sayten::types::call_reverse_apply<sayten::types::identity>(sayten::types::identity<int>::pure(1), sayten::types::identity<std::function<std::string(int)>>(int_to_string)).extract() == int_to_string(1),
           "The correct generic 'reverse_apply' call result was not produced for the 'identity' applicative!");

    assert(sayten::types::identity<decltype(lambda_func)>::pure(lambda_func).apply(identity_one).extract() == lambda_func(1) &&
           sayten::types::identity<std::function<std::string(int)>>::pure(int_to_string).apply(identity_one).extract() == int_to_string(1),
           "The correct 'reverse_apply' result was not produced for the 'identity' applicative!");
    assert(sayten::types::call_apply<sayten::types::identity>(sayten::types::identity<std::function<std::string(int)>>(int_to_string), sayten::types::identity<int>::pure(1)).extract() == int_to_string(1),
           "The correct generic 'reverse_apply' call result was not produced for the 'identity' applicative!");

    assert(identity_one.lift2(lambda_bifunc, identity_string).extract() == lambda_bifunc(1, test_str) &&
           sayten::types::identity<std::string>(test_str).lift2(simple_bifunc, identity_one).extract() == simple_bifunc(test_str, 1),
           "The correct 'lift2' result was not produced for the 'identity' applicative!");
    assert(sayten::types::call_lift2<sayten::types::identity>(identity_string, simple_bifunc, identity_one).extract() == simple_bifunc(test_str, 1),
           "The correct generic 'lift2' call result was not produced for the 'identity' applicative!");
    assert(sayten::types::identity<sayten::types::identity<int>>(identity_one).join().extract() == 1, 
		   "The correct 'join' result was not produced for the 'identity' monad!");
    assert(sayten::types::call_join(sayten::types::identity<sayten::types::identity<std::string>>(identity_string)).extract() == test_str, 
	       "The correct generic 'join' call result was not produced for the 'identity' monad!");
    assert(identity_one.bind(identity_to_string).extract() == int_to_string(1), 
		   "The correct 'bind' result was not produced for the 'identity' monad!");
    assert(sayten::types::call_bind(identity_one, identity_to_string).extract() == int_to_string(1), 
	       "The correct generic 'bind' call result was not produced for the 'identity' monad!");
    assert(identity_one.bind(identity_to_string).extract() == int_to_string(1), 
		   "The correct 'bind' result was not produced for the 'identity' monad!");
    assert(sayten::types::call_bind(identity_one, identity_to_string).extract() == int_to_string(1), 
	       "The correct generic 'bind' call result was not produced for the 'identity' monad!");
    assert(identity_one.then(identity_string).extract() == test_str, 
		   "The correct 'then' result was not produced for the 'identity' monad!");
    assert(sayten::types::call_then(identity_one, identity_string).extract() == test_str, 
		   "The correct generic 'then' call result was not produced for the 'identity' monad!");

    return 0;
}