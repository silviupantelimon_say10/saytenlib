#include "test.hpp"
#include "../sayten.hpp"

constexpr char monoidal_name[] = "Test monoidal";
constexpr char monoidal_with_pure_name[] = "Test monoidal with 'pure'";
constexpr char monoidal_with_apply_name[] = "Test monoidal with 'apply'";
constexpr char monoidal_with_reverse_apply_name[] = "Test monoidal with 'reverse_apply'";
constexpr char monoidal_with_apply_and_reverse_apply_name[] = "Test monoidal with 'apply' and 'reverse_apply'";
constexpr char monoidal_with_lift2_name[] = "Test monoidal with 'lift2'";
constexpr char monoidal_with_apply_and_lift2_name[] = "Test monoidal with 'apply' and 'lift2'";
constexpr char monoidal_with_reverse_apply_and_lift2_name[] = "Test monoidal with 'reverse_apply' and 'lift2'";
constexpr char monoidal_with_apply_and_reverse_apply_and_lift2_name[] = "Test monoidal with 'apply', 'reverse_apply' and 'lift2'";
constexpr char applicative_name[] = "Test applicative";

template<template<typename> typename test_t, bool should_have_pure, bool should_have_apply, bool should_have_reverse_apply, bool should_have_lift2, const char *name>
void test_template_applicative()
{
	assert_const<sayten::types::applicative<test_t>>(std::string(name) + " should be an applicative functor!");

	if constexpr (should_have_pure) {
		assert_const<sayten::types::has_pure<test_t>>(std::string(name) + " should have a proper 'pure'!");
	} else {
		assert_const<!sayten::types::has_pure<test_t>>(std::string(name) + " should not have a proper 'pure'!");
	}

	if constexpr (should_have_apply) {
		assert_const<sayten::types::has_apply<test_t>>(std::string(name) + " should have a proper 'apply'!");
	} else {
		assert_const<!sayten::types::has_apply<test_t>>(std::string(name) + " should not have a proper 'apply'!");
	}

	if constexpr (should_have_reverse_apply) {
		assert_const<sayten::types::has_reverse_apply<test_t>>(std::string(name) + " should have a proper 'reverse_apply'!");
	} else {
		assert_const<!sayten::types::has_reverse_apply<test_t>>(std::string(name) + " should not have a proper 'reverse_apply'!");
	}

	if constexpr (should_have_lift2) {
		assert_const<sayten::types::has_lift2<test_t>>(std::string(name) + " should have a proper 'lift2'!");
	} else {
		assert_const<!sayten::types::has_lift2<test_t>>(std::string(name) + " should not have a proper 'lift2'!");
	}

	test_t<int> int_test(1);
	test_t<double> double_test(0.5f);
	test_t<int> int_test_result(2);
	test_t<std::string> string_test(test_string);
	test_t<std::string> string_test_result(test_string_doubled);
	test_t<std::string> string_test_result_bifunction(test_string + " 1 0.5");

	assert(sayten::types::call_pure<test_t>(1) == int_test &&
		   sayten::types::call_pure<test_t>(test_string) == string_test,
           std::string(name) + " should produce correct value for 'call_pure'!");

	auto plus_one_test = sayten::types::call_pure<test_t>(&plus_one_int);
	auto double_string_test = sayten::types::call_pure<test_t>(&double_string);
	auto lambda_plus_one_test = sayten::types::call_pure<test_t>(lambda_plus_one_int);
	auto lambda_double_string_test = sayten::types::call_pure<test_t>(lambda_double_string);

	assert(sayten::types::call_apply(plus_one_test, int_test) == int_test_result &&
		   sayten::types::call_apply(double_string_test, string_test) == string_test_result &&
		   sayten::types::call_apply(lambda_plus_one_test, int_test) == int_test_result &&
		   sayten::types::call_apply(lambda_double_string_test, string_test) == string_test_result,
           std::string(name) + " should produce correct value for 'call_apply'!");
	assert(sayten::types::call_reverse_apply(int_test, plus_one_test) == int_test_result &&
		   sayten::types::call_reverse_apply(string_test, double_string_test) == string_test_result &&
		   sayten::types::call_reverse_apply(int_test, lambda_plus_one_test) == int_test_result &&
		   sayten::types::call_reverse_apply(string_test, lambda_double_string_test) == string_test_result,
           std::string(name) + " should produce correct value for 'call_reverse_apply'!");
	assert(sayten::types::call_lift2(int_test, bifunction, double_test) == string_test_result_bifunction &&
		   sayten::types::call_lift2(int_test, lambda_bifunction, double_test) == string_test_result_bifunction,
           std::string(name) + " should produce correct value for 'call_lift2'!");
}

int main()
{
    test_template_applicative<test_monoidal, false, false, false, false, monoidal_name>();
	test_template_applicative<test_monoidal_with_pure, true, false, false, false, monoidal_with_pure_name>();
	test_template_applicative<test_monoidal_with_apply, false, true, false, false, monoidal_with_apply_name>();
	test_template_applicative<test_monoidal_with_reverse_apply, false, false, true, false, monoidal_with_reverse_apply_name>();
	test_template_applicative<test_monoidal_with_apply_and_reverse_apply, false, true, true, false, monoidal_with_apply_and_reverse_apply_name>();
	test_template_applicative<test_monoidal_with_lift2, false, false, false, true, monoidal_with_lift2_name>();
	test_template_applicative<test_monoidal_with_apply_and_lift2, false, true, false, true, monoidal_with_apply_and_lift2_name>();
	test_template_applicative<test_monoidal_with_reverse_apply_and_lift2, false, false, true, true, monoidal_with_reverse_apply_and_lift2_name>();
	test_template_applicative<test_monoidal_with_apply_and_reverse_apply_and_lift2, false, true, true, true, monoidal_with_apply_and_reverse_apply_and_lift2_name>();
    test_template_applicative<test_applicative, true, true, true, true, applicative_name>();

    return 0;
}