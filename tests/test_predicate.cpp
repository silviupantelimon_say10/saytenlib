#include "test.hpp"
#include "../sayten.hpp"

inline bool test_string_fun(const std::string &x)
{
    return x.size() % 2 == 0;
}

inline std::string to_string_fun(const int &x) {
    return std::to_string(x);
}

auto get_comparer(std::string x)
{
    return [x] (const std::string &y) { return x == y; };
}

int main()
{
    auto double_int = [] (int x) { return 2 * x; };
    auto to_string_lambda = [] (int x) { return std::to_string(x); };
    std::function<bool(const std::string &x)> test_string_std(test_string_fun);

    assert_const<sayten::types::contravariant<sayten::types::predicate>>("Template 'predicate' should be a contravariant functor!");
    assert(
        sayten::types::make_predicate(test_string_fun).contramap(to_string_lambda).contramap(double_int)(13) == test_string_fun(to_string_lambda(double_int(13))) &&
        sayten::types::make_predicate(get_comparer(std::to_string(double_int(13)))).contramap(to_string_lambda).contramap(double_int)(13),
        "The correct 'contramap' result was not produced for the 'predicate' contravariant functor!"
    );

    return 0;
}