#include <vector>
#include <list>
#include <deque>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <forward_list>
#include <string>
#include "../sayten.hpp"

template<template<typename...> typename container_t>
sayten::contained_enumerable<container_t<int>> simple_generator() noexcept
{
	static int j = 0;

    for (int i = 0; i < 10; ++i) {
        co_yield ++j;
    }
}

template<template<typename...> typename container_t>
sayten::contained_enumerable<container_t<std::string, int>> map_generator() noexcept
{
	static int j = 0;

    for (int i = 0; i < 10; ++i) {
        ++j;
        co_yield std::pair(std::to_string(j), j);
    }
}

template<template<typename...> typename container_t>
sayten::contained_enumerable<container_t<int>> simple_generator_double() noexcept
{
	static int j = 0;

    for (int i = 0; i < 10; ++i) {
        co_yield ++j;
        co_yield j;
    }
}

template<template<typename...> typename container_t>
sayten::contained_enumerable<container_t<std::string, int>> map_generator_double() noexcept
{
	static int j = 0;

    for (int i = 0; i < 10; ++i) {
        ++j;
        co_yield std::pair(std::to_string(j), j);
        co_yield std::pair(std::to_string(j), j + 1);
    }
}

template<template<typename...> typename container_t>
bool test_simple_container(std::function<sayten::contained_enumerable<container_t<int>>()> generator) noexcept
{
    container_t<int> result = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    sayten::contained_enumerable<container_t<int>> enumerable = generator();
    using iterator = container_t<int>::iterator;
    
    int i = 0;
	
    
    while (std::optional<int> value = enumerable.move_next()) {
        if (i == 5)
			break;

		iterator found = std::find(result.begin(), result.end(), value.value());

		if (found == result.end())
			return false;
        
        ++i;
    }

    if (i != 5) {
        return false;
    }
    
	i = 0;

    for (int value : enumerable) {
		iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;
        
        ++i;
    }

	if (i != 10) {
		return false;
    }

	i = 0;

    for (int value : enumerable) {
		iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;
        
        ++i;
    }

    return i == 10;
}

template<template<typename...> typename container_t>
bool test_map_container(std::function<sayten::contained_enumerable<container_t<std::string, int>>()> generator) noexcept
{
    container_t<std::string, int> result = { {"1", 1}, {"2", 2}, {"3", 3}, {"4", 4}, {"5", 5}, {"6", 6}, {"7", 7}, {"8", 8}, {"9", 9}, {"10", 10} };
    sayten::contained_enumerable<container_t<std::string, int>> enumerable = generator();
    using iterator = container_t<std::string, int>::iterator;

    size_t i = 0;

	while (std::optional<std::pair<const std::string, int>> value = enumerable.move_next()) {
		if (i == 5)
			break;

		iterator found = result.find(value.value().first);

		if (found == result.end() || found->second != value.value().second)
			return false;
        
        ++i;
    }

    if (i != 5) {
        return false;
    }

	i = 0;

    for (std::pair<const std::string, int> value : enumerable) {
		iterator found = result.find(value.first);

		if (found == result.end() || found->second != value.second)
			return false;
        
        ++i;
    }

	if (i != 10) {
		return false;
    }

	i = 0;

    for (std::pair<const std::string, int> value : enumerable) {
		iterator found = result.find(value.first);

		if (found == result.end() || found->second != value.second)
			return false;
        
        ++i;
    }

    return i == 10;
}

int main()
{
    if (!test_simple_container<std::vector>(simple_generator<std::vector>)) {
        return 1;
    }

    if (!test_simple_container<std::list>(simple_generator<std::list>)) {
        return 1;
    }

    if (!test_simple_container<std::forward_list>(simple_generator<std::forward_list>)) {
        return 1;
    }

    if (!test_simple_container<std::deque>(simple_generator<std::deque>)) {
        return 1;
    }

    if (!test_simple_container<std::set>(simple_generator<std::set>)) {
        return 1;
    }

    if (!test_simple_container<std::multiset>(simple_generator<std::multiset>)) {
        return 1;
    }

    if (!test_simple_container<std::unordered_set>(simple_generator<std::unordered_set>)) {
        return 1;
    }

    if (!test_simple_container<std::unordered_multiset>(simple_generator<std::unordered_multiset>)) {
        return 1;
    }

	if (!test_map_container<std::map>(map_generator<std::map>)) {
        return 1;
    }

	if (!test_map_container<std::multimap>(map_generator<std::multimap>)) {
        return 1;
    }

	if (!test_map_container<std::unordered_map>(map_generator<std::unordered_map>)) {
        return 1;
    }

	if (!test_map_container<std::unordered_multimap>(map_generator<std::unordered_multimap>)) {
        return 1;
    }

    if (!test_simple_container<std::set>(simple_generator_double<std::set>)) {
        return 1;
    }

    if (!test_simple_container<std::unordered_set>(simple_generator_double<std::unordered_set>)) {
        return 1;
    }

    if (!test_map_container<std::map>(map_generator_double<std::map>)) {
        return 1;
    }

    if (!test_map_container<std::unordered_map>(map_generator_double<std::unordered_map>)) {
        return 1;
    }

    return 0;
}