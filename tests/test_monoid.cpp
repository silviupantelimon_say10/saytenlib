#include "test.hpp"
#include "../sayten.hpp"

struct int_wrapper
{
    int value = 0;

    constexpr int_wrapper(const int &value) : value(value) {}

    static const int_wrapper identity_element;

    int_wrapper cross(const int_wrapper &other) const noexcept
    {
        return int_wrapper(value + other.value);
    }

    constexpr bool operator==(const int_wrapper &other) const noexcept
    {
        return value == other.value;
    }

    constexpr std::strong_ordering operator<=>(const int_wrapper &other) const noexcept
    {
        return value <=> other.value;
    }
};

int prod(const int &a, const int &b) noexcept
{
    return a * b;
}

constexpr const int_wrapper int_wrapper::identity_element = int_wrapper(0);

int main()
{
    assert_const<!sayten::types::is_monoid<int>>("Type 'int' should not be a monoid!");
    assert_const<sayten::types::is_semigroup<int_wrapper>>("Type 'int_wrapper' should be a semigroup!");
    assert_const<sayten::types::is_monoid<int_wrapper>>("Type 'int_wrapper' should be a monoid!");
    assert_const<sayten::types::has_identity_element<int_wrapper>>("Monoid 'int_wrapper' should have an identity element!");
    assert_const<sayten::types::get_identity_element<int_wrapper>() == int_wrapper(0)>("Monoid 'int_wrapper' should have the identity element 'int_wrapper(0)'!");
    assert_const<sayten::types::is_semigroup<sayten::types::monoid<int, prod, 1>>>("Type 'monoid<int, prod, 1>' should be a semigroup!");
    assert_const<sayten::types::is_monoid<sayten::types::monoid<int, prod, 1>>>("Type 'monoid<int, prod, 1>' should be a monoid!");
    assert(int_wrapper(2).cross(int_wrapper(3)) == int_wrapper(5), "Cross product failed for type 'int_wrapper'!");
    assert(sayten::types::call_cross(int_wrapper(2), int_wrapper(3)) == int_wrapper(5), "Generic cross product call failed for type 'int_wrapper'!");
    assert(sayten::types::monoid<int, prod, 1>(2).cross(sayten::types::monoid<int, prod, 1>(3)) == 6, "Cross product failed for type 'monoid<int, prod, 1>'!");
    assert(sayten::types::monoid<int, prod, 1>(2).cross(3) == 6, "Cross product with implicit cast failed for type 'monoid<int, prod, 1>'!");
    assert(sayten::types::call_cross(sayten::types::monoid<int, prod, 1>(2), sayten::types::monoid<int, prod, 1>(3)) == 6, "Generic cross product call failed for type 'monoid<int, prod, 1>'!");
    assert(sayten::types::call_cross<sayten::types::monoid<int, prod, 1>>(2, 3) == 6, "Generic cross product call with implicit cast failed for type 'monoid<int, prod, 1>'!");
    assert_const<sayten::types::has_identity_element<sayten::types::monoid<int, prod, 1>>>("Monoid 'monoid<int, prod, 1>' should have an identity element!");
    assert(sayten::types::monoid<int, prod, 1>::identity_element == 1 && sayten::types::get_identity_element<sayten::types::monoid<int, prod, 1>>() == 1, 
        "Monoid 'monoid<int, prod, 1>' should have the identity element '1'!");

    return 0;
}