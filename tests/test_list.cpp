#include "test.hpp"
#include "../sayten.hpp"

template<template<typename> typename container_t>
requires sayten::qualified_container<container_t<int>>
container_t<int> generate(int start, int end)
{
    container_t<int> result;

    for (int i = start; i < end; ++i) {
        sayten::add(result, i);
    }

    return result;
}

template<template<typename> typename container_t>
requires sayten::qualified_container<container_t<std::string>>
container_t<std::string> generate_result(int start, int end)
{
    container_t<std::string> result;

    for (int i = start; i < end; ++i) {
        sayten::add(result, std::to_string(i * i));
    }

    return result;
}

template<template<typename> typename container_t>
struct is_unordered_container
{
    static const bool value = false;
};

template<>
struct is_unordered_container<std::unordered_set>
{
    static const bool value = true;
    
    template<typename value_t>
    using ordered_equivalent_type = std::set<value_t>;
};

template<>
struct is_unordered_container<std::unordered_multiset>
{
    static const bool value = true;

    template<typename value_t>
    using ordered_equivalent_type = std::multiset<value_t>;
};

template<template<typename> typename container_t,  typename value_t>
requires sayten::qualified_container<container_t<value_t>> && std::equality_comparable<value_t>
bool equal(const container_t<value_t> &a, const container_t<value_t> &b)
{
    if constexpr (is_unordered_container<container_t>::value) {
        auto new_a = sayten::morph<typename is_unordered_container<container_t>::ordered_equivalent_type<value_t>>(a);
        auto new_b = sayten::morph<typename is_unordered_container<container_t>::ordered_equivalent_type<value_t>>(b);

        auto it_a = new_a.cbegin();
        auto it_b = new_b.cbegin();

        for (; it_a != new_a.cend() && it_b != new_b.cend(); ++it_a, ++it_b) {
            if (*it_a != *it_b) {
                return false;
            }
        }

        if (it_a != new_a.cend() && it_b != new_b.cend()) {
            return false;
        }
    } else {
        auto it_a = a.cbegin();
        auto it_b = b.cbegin();

        for (; it_a != a.cend() && it_b != b.cend(); ++it_a, ++it_b) {
            if (*it_a != *it_b) {
                return false;
            }
        }

        if (it_a != a.cend() && it_b != b.cend()) {
            return false;
        }
    }

    return true;
}

template<template<typename> typename container_t>
requires sayten::qualified_container<container_t<std::string>>
std::string produce_foldr(const container_t<int> &a)
{
    std::string result = "";

    for (const int &value : a) {
        result = std::to_string(value) + result;
    }

    return result;
}

template<template<typename> typename container_t>
requires sayten::qualified_container<container_t<std::string>>
std::string produce_foldl(const container_t<int> &a)
{
    std::string result = "";

    for (const int &value : a) {
        result += std::to_string(value);
    }

    return result;
}

int sum(const int &a, const int &b)
{
    return a + b;
}

long sum_long(const long &a, const long &b)
{
    return a + b;
}

long sqr_fun(int x) {
    return x * x;
}

struct sqr_obj
{
    int val = 0;
    
    inline long operator()(int x) const noexcept
    {
        return x * x;
    }

    auto operator<=>(const sqr_obj &other) const noexcept
    {
        return val <=> other.val;
    }

    bool operator==(const sqr_obj &other) const noexcept
    {
        return val == other.val;
    }
};

namespace std
{
    template<>
    struct hash<sqr_obj>
    {
        size_t operator()(const sqr_obj &obj) const noexcept
        {
            hash<int> hasher;

            return hasher(obj.val);
        }
    };
}

constexpr char vector_monad_name[] = "vector_monad";
constexpr char list_monad_name[] = "list_monad";
constexpr char deque_monad_name[] = "deque_monad";
constexpr char set_monad_name[] = "set_monad";
constexpr char multiset_monad_name[] = "multiset_monad";
constexpr char unordered_set_monad_name[] = "unordered_set_monad";
constexpr char unordered_multiset_monad_name[] = "unordered_multiset_monad";

template<template<typename> typename wrapper_t, template<typename> typename container_t, const char *name>
requires sayten::qualified_container<container_t<int>> && std::same_as<typename wrapper_t<int>::container_type, container_t<int>>
void test_container()
{
    auto sqr = [] (int x) -> long { return x * x; };
    auto to_string = [] (long x) { return std::to_string(x); };
    auto increase = [sqr, to_string] (int x) { return wrapper_t(generate<container_t>(10 * x, 10 * (x + 1))).fmap(sqr).fmap(to_string); };
    auto to_monoid = [] (int a) { return sayten::types::monoid<int, sum, 0>(a); };
    auto to_monoid_long = [] (int a) { return sayten::types::monoid<long, sum_long, 0L>(a); };
    auto string_reduce = [] (int a, std::string b) { return std::to_string(a) + b; };
    auto string_reduce_flip = [] (std::string a, int b) { return a + std::to_string(b); };
    auto reducer = [] (int a, int b) { return a + b; };
    auto no_five_to_maybe = [] (int x) { return x != 5 ? sayten::types::maybe(std::to_string(x * x)) : sayten::types::maybe<std::string>(); };
    auto to_maybe = [] (int x) { return sayten::types::maybe(std::to_string(x * x)); };
    
    assert_const<sayten::types::functor<wrapper_t>>("Template '" + std::string(name) + "' should be a functor!");
    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).fmap(sqr).fmap(to_string).extract(), generate_result<container_t>(0, 10)),
        "The correct 'fmap' result was not produced for the '" + std::string(name) +"' functor!"
    );

    container_t<sayten::types::unit> unit_container;
    sayten::add(unit_container, sayten::types::unit());

    assert_const<sayten::types::monoidal<wrapper_t>>("Template '" + std::string(name) + "' should be a monoidal!");

    assert(
        equal(wrapper_t<int>::unit_transform(sayten::types::unit()).extract(), unit_container),
        "The correct 'unit_transform' result was not produced for the '" + std::string(name) + "' monoidal!"
    );

    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).merge(wrapper_t<std::string>::pure("")).fmap([] (const sayten::types::product<int, std::string> &pair) { return std::to_string(pair.get_first() * pair.get_first()) + pair.get_second(); }).extract(), generate_result<container_t>(0, 10)),
        "The correct 'merge' result was not produced for the " + std::string(name) + " monoidal!"
    );
    
    assert_const<sayten::types::applicative<wrapper_t>>("Template '" + std::string(name) + "' should be an applicative!");
    assert(
        equal(wrapper_t<int>::pure(9).fmap(sqr).fmap(to_string).extract(), generate_result<container_t>(9, 10)),
        "The correct 'pure' result was not produced for the '" + std::string(name) + "' applicative!"
    );
    assert(
        equal(wrapper_t<int>::pure(9).fmap(sqr).fmap(to_string).extract(), generate_result<container_t>(9, 10)),
        "The correct 'pure' result was not produced for the '" + std::string(name) + "' applicative!"
    );

    sqr_obj obj_fun = sqr_obj{0};

    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).reverse_apply(wrapper_t<sqr_obj>::pure(obj_fun)).fmap(to_string).extract(), generate_result<container_t>(0, 10)),
        "The correct 'reverse_apply' result was not produced for the " + std::string(name) + " applicative!"
    );
    assert(
        equal(wrapper_t<sqr_obj>::pure(obj_fun).apply(wrapper_t(generate<container_t>(0, 10))).fmap(to_string).extract(), generate_result<container_t>(0, 10)),
        "The correct 'reverse_apply' result was not produced for the " + std::string(name) + " applicative!"
    );
    assert_const<sayten::types::alternative<wrapper_t>>("Template '" + std::string(name) + "' should be an alternative!");
    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).fmap(sqr).fmap(to_string).alternative(wrapper_t(generate<container_t>(10, 20)).fmap(sqr).fmap(to_string)).extract(), generate_result<container_t>(0, 10)) &&
        equal(wrapper_t(generate<container_t>(0, 10)).fmap(sqr).fmap(to_string).alternative(wrapper_t<std::string>::empty).extract(), generate_result<container_t>(0, 10)) &&
        equal(wrapper_t<std::string>::empty.alternative(wrapper_t<int>(generate<container_t>(10, 20)).fmap(sqr).fmap(to_string)).extract(), generate_result<container_t>(10, 20)) &&
        equal(wrapper_t<std::string>::empty.alternative(wrapper_t<std::string>::empty).extract(), generate_result<container_t>(0, 0)),
        "The correct 'alternative' result was not produced for the '" + std::string(name) + "' alternative!"
    );
    assert_const<sayten::types::monad<wrapper_t>>("Template '" + std::string(name) + "' should be a monad!");
    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).fmap(increase).join().extract(), generate_result<container_t>(0, 100)),
        "The correct 'join' result was not produced for the " + std::string(name) + " monad!"
    );
    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).bind(increase).extract(), generate_result<container_t>(0, 100)),
        "The correct 'bind' result was not produced for the " + std::string(name) + " monad!"
    );
    assert(
        equal(wrapper_t(generate<container_t>(0, 10)).then(wrapper_t(generate<container_t>(10, 20)).fmap(sqr).fmap(to_string)).extract(), generate_result<container_t>(10, 20)),
        "The correct 'then' result was not produced for the " + std::string(name) + " monad!"
    );
    assert_const<sayten::types::has_fold<wrapper_t>>("Template '" + std::string(name) + "' should have 'fold'!");
    assert_const<sayten::types::has_foldr<wrapper_t>>("Template '" + std::string(name) + "' should have 'foldr'!");
    assert_const<sayten::types::has_foldl<wrapper_t>>("Template '" + std::string(name) + "' should have 'foldl'!");
    assert_const<sayten::types::has_fold_map<wrapper_t>>("Template '" + std::string(name) + "' should have 'fold_map'!");
    assert_const<sayten::types::foldable<wrapper_t>>("Template '" + std::string(name) + "' should be 'foldable!");
    assert(
        wrapper_t(generate<container_t>(0, 0)).foldr(string_reduce, "") == produce_foldr(generate<container_t>(0, 0)) &&
        wrapper_t(generate<container_t>(0, 10)).foldr(string_reduce, "") == produce_foldr(generate<container_t>(0, 10)),
        "The correct 'foldr' result was not produced for the " + std::string(name) + " foldable!"
    );
    assert(
        wrapper_t(generate<container_t>(0, 0)).foldl(string_reduce_flip, "") == produce_foldl(generate<container_t>(0, 0)) &&
        wrapper_t(generate<container_t>(0, 10)).foldl(string_reduce_flip, "") == produce_foldl(generate<container_t>(0, 10)),
        "The correct 'foldl' result was not produced for the " + std::string(name) + " foldable!"
    );
    assert(
        wrapper_t(generate<container_t>(0, 0)).fmap(to_monoid).fold() == 0 &&
        wrapper_t(generate<container_t>(0, 10)).fmap(to_monoid).fold() == (9 * 10) / 2,
        "The correct 'fold' result was not produced for the " + std::string(name) + " foldable!"
    );
    assert(
        wrapper_t(generate<container_t>(0, 0)).fold_map(to_monoid_long) == 0L &&
        wrapper_t(generate<container_t>(0, 10)).fold_map(to_monoid_long) == (9L * 10L) / 2L,
        "The correct 'fold_map' result was not produced for the " + std::string(name) + " foldable!"
    );

    sayten::types::maybe<int> reduce_result = wrapper_t(generate<container_t>(0, 10)).reduce(reducer);

    assert(
        !wrapper_t(generate<container_t>(0, 0)).reduce(reducer).has_value() &&
        reduce_result.has_value() &&
        reduce_result.value() == (9 * 10) / 2,
        "The correct 'reduce' result was not produced for the " + std::string(name) + " foldable!"
    );
    assert_const<sayten::types::has_sequence<wrapper_t>>("Template '" + std::string(name) + "' should have 'sequence'!");
    assert_const<sayten::types::has_traverse<wrapper_t>>("Template '" + std::string(name) + "' should have 'traverse'!");
    assert_const<sayten::types::traversable<wrapper_t>>("Template '" + std::string(name) + "' should be a traversable!");

    auto maybe_result = wrapper_t(generate<container_t>(0, 10)).fmap(to_maybe).sequence();

    assert(
        !wrapper_t(generate<container_t>(0, 10)).fmap(no_five_to_maybe).sequence().has_value() &&
        maybe_result.has_value() &&
        equal(maybe_result.value().extract(), generate_result<container_t>(0, 10)),
        "The correct 'sequence' result was not produced for the " + std::string(name) + " traversable!");

    maybe_result = wrapper_t(generate<container_t>(0, 10)).traverse(to_maybe);
    
    assert(
        !wrapper_t(generate<container_t>(0, 10)).traverse(no_five_to_maybe).has_value() &&
        maybe_result.has_value() &&
        equal(maybe_result.value().extract(), generate_result<container_t>(0, 10)),
        "The correct 'traverse' result was not produced for the " + std::string(name) + " traversable!");
}

int main() 
{
    test_container<sayten::types::vector_monad, std::vector, vector_monad_name>();
    test_container<sayten::types::list_monad, std::list, list_monad_name>();
    test_container<sayten::types::deque_monad, std::deque, deque_monad_name>();
    test_container<sayten::types::set_monad, std::set, set_monad_name>();
    test_container<sayten::types::multiset_monad, std::multiset, multiset_monad_name>();
    test_container<sayten::types::unordered_set_monad, std::unordered_set, unordered_set_monad_name>();
    test_container<sayten::types::unordered_multiset_monad, std::unordered_multiset, unordered_multiset_monad_name>();

    return 0;
}