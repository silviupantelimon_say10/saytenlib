#include "test.hpp"
#include "../sayten.hpp"

int main()
{
    assert_const<sayten::types::has_fmap<test_functor>>("Test functor should have a proper 'fmap'!");
    assert_const<sayten::types::functor<test_functor>>("Test functor should be a functor!");

    test_functor<int> int_test(1);
    test_functor<std::string> string_test(test_string);
    test_functor<int> int_test_result(2);
    test_functor<std::string> string_test_result(test_string_doubled);
    test_functor<std::string> int_string_test_result("1");

    assert(sayten::types::call_fmap(int_test, &plus_one_int) == int_test_result &&
           sayten::types::call_fmap(int_test, lambda_plus_one_int) == int_test_result &&
           sayten::types::call_fmap(string_test, &double_string) == string_test_result &&
           sayten::types::call_fmap(string_test, lambda_double_string) == string_test_result &&
           sayten::types::call_fmap(int_test, &int_to_string) == int_string_test_result &&
           sayten::types::call_fmap(int_test, lambda_int_to_string) == int_string_test_result, 
           "Test functor should produce correct value for 'call_fmap'!");

    return 0;
}