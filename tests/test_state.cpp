#include <string>

#include "../sayten.hpp"
#include "test.hpp"

int main()
{
    auto xxx = [] (int x) -> sayten::types::identity<sayten::types::product<std::string, int>> { return sayten::types::identity<sayten::types::product<std::string, int>>(sayten::types::product<std::string, int>(std::to_string(x), x + 1)); };

    auto aaa = sayten::types::make_state(xxx);
    auto fff = [] (const std::string &x) { return x + x; };

    std::cout << xxx(1).extract().get_first() << "\r\n";
    std::cout << aaa.fmap(fff).map_state([] (const sayten::types::identity<sayten::types::product<std::string, int>> &st) { 
                                            return st.fmap([] (const sayten::types::product<std::string, int> &y) {
                                                               return sayten::types::product<unsigned long, int>(y.get_first().size(), y.get_second() + 1);
                                                           });
                                            })(1).extract().get_first() << "\r\n";

    return 0;
}