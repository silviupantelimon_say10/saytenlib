#include <string>
#include "../sayten.hpp"

inline std::string plus(const int &a, const long &b)
{
    return std::to_string(a + b);
} 

inline std::string prod(const int &a, const long &b)
{
    return std::to_string(a * b);
}

int main()
{
    using test_function_view = sayten::function_view<std::string(const int &, const long &)>;
    
    test_function_view fun_plus = plus;
    test_function_view fun_prod = prod;

    auto lambda_plus = [] (const int &a, const long &b) { return std::to_string(a + b); };
    auto lambda_prod = [] (const int &a, const long &b) { return std::to_string(a * b); };

    test_function_view fun_lambda_plus = lambda_plus;
    test_function_view fun_lambda_prod = lambda_prod;

    std::function<std::string(const int &, const long &)> std_plus = lambda_plus;
    std::function<std::string(const int &, const long &)> std_prod = prod;

    test_function_view fun_std_plus = std_plus;
    test_function_view fun_std_prod = std_prod;

    if (fun_plus(2, 3) != "5")
        return 1;
    
    if (fun_prod(2, 3) != "6")
        return 1;

    if (fun_lambda_plus(2, 3) != "5")
        return 1;
    
    if (fun_lambda_prod(2, 3) != "6")
        return 1;

    if (fun_std_plus(2, 3) != "5")
        return 1;
    
    if (fun_std_prod(2, 3) != "6")
        return 1;

    return 0;
}