#include "test.hpp"
#include "../sayten.hpp"

struct int_wrapper
{
    static const int_wrapper identity_element;

    int value = 0;

    constexpr int_wrapper(const int &value) : value(value) {}

    int_wrapper cross(const int_wrapper &other) const noexcept
    {
        return int_wrapper(value + other.value);
    }

    int_wrapper inverse() const noexcept
    {
        return int_wrapper(-value);
    }

    constexpr bool operator==(const int_wrapper &other) const noexcept
    {
        return value == other.value;
    }

    constexpr std::strong_ordering operator<=>(const int_wrapper &other) const noexcept
    {
        return value <=> other.value;
    }
};

constexpr const int_wrapper int_wrapper::identity_element = int_wrapper(0);

int plus(const int &a, const int &b) noexcept
{
    return a + b;
}

int inverse(const int &x) noexcept
{
    return -x;
}

int main()
{
    assert_const<!sayten::types::is_group<int>>("Type 'int' should not be a group!");
    assert_const<sayten::types::is_semigroup<int_wrapper>>("Type 'int_wrapper' should be a semigroup!");
    assert_const<sayten::types::is_monoid<int_wrapper>>("Type 'int_wrapper' should be a monoid!");
    assert_const<sayten::types::is_group<int_wrapper>>("Type 'int_wrapper' should be a group!");
    assert_const<sayten::types::is_semigroup<sayten::types::group<int, plus, 0, inverse>>>("Type 'group<int, plus, 0, inverse>' should be a semigroup!");
    assert_const<sayten::types::is_monoid<sayten::types::group<int, plus, 0, inverse>>>("Type 'group<int, plus, 0, inverse>' should be a monoid!");
    assert_const<sayten::types::is_group<sayten::types::group<int, plus, 0, inverse>>>("Type 'group<int, plus, 0, inverse>' should be a group!");
    assert_const<sayten::types::has_identity_element<int_wrapper>>("Group 'int_wrapper' should have an identity element!");
    assert_const<sayten::types::get_identity_element<int_wrapper>() == int_wrapper(0)>("Group 'int_wrapper' should have the identity element 'int_wrapper(0)'!");
    assert(sayten::types::group<int, plus, 0, inverse>::identity_element == 0 && sayten::types::get_identity_element<sayten::types::group<int, plus, 0, inverse>>() == 0, 
        "Group 'group<int, plus, 0, inverse>' should have the identity element '0'!");
    assert(int_wrapper(2).cross(int_wrapper(3)) == int_wrapper(5), "Cross product failed for type 'int_wrapper'!");
    assert(sayten::types::call_cross(int_wrapper(2), int_wrapper(3)) == int_wrapper(5), "Generic cross product call failed for type 'int_wrapper'!");
    assert(sayten::types::group<int, plus, 0, inverse>(2).cross(sayten::types::group<int, plus, 0, inverse>(3)) == 5, "Cross product failed for type 'group<int, plus, 0, inverse>'!");
    assert(sayten::types::group<int, plus, 0, inverse>(2).cross(3) == 5, "Cross product with implicit cast failed for type 'group<int, plus, 0, inverse>'!");
    assert(sayten::types::call_cross(sayten::types::group<int, plus, 0, inverse>(2), sayten::types::group<int, plus, 0, inverse>(3)) == 5, "Generic cross product call failed for type 'group<int, plus, 0, inverse>'!");
    assert(sayten::types::call_cross<sayten::types::group<int, plus, 0, inverse>>(2, 3) == 5, "Generic cross product call with implicit cast failed for type 'group<int, plus, 0, inverse>'!");
    assert(int_wrapper(2).inverse() == int_wrapper(-2), "Inverse failed for type 'int_wrapper'!");
    assert(sayten::types::get_inverse(int_wrapper(2)) == int_wrapper(-2), "Generic inverse call failed for type 'int_wrapper'!");
    assert(sayten::types::group<int, plus, 0, inverse>(2).inverse() == -2, "Inverse failed for type 'group<int, plus, 0, inverse>'!");
    assert(sayten::types::get_inverse(sayten::types::group<int, plus, 0, inverse>(2)) == -2, "Generic inverse call failed for type 'group<int, plus, 0, inverse>'!");

    return 0;
}