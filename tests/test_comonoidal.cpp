#include "test.hpp"

int main()
{
    assert_const<sayten::types::has_fmap<test_comonoidal>>("Test comonoidal should have a proper 'fmap'!");
    assert_const<sayten::types::functor<test_comonoidal>>("Test comonoidal should be a functor!");
    assert_const<sayten::types::has_unit_untransform<test_comonoidal>>("Test comonoidal should have a proper 'unit_untransform'!");
    assert_const<sayten::types::has_split<test_comonoidal>>("Test comonoidal should have a proper 'split'!");
    assert_const<sayten::types::comonoidal<test_comonoidal>>("Test comonoidal should be a comonoidal functor!");

    test_comonoidal<int> int_test(1);
    test_comonoidal<std::string> string_test(test_string);
    test_comonoidal<int> int_test_result(2);
    test_comonoidal<std::string> string_test_result(test_string_doubled);
    test_comonoidal<sayten::types::unit> unit_test(sayten::types::unit{});
    test_comonoidal<sayten::types::product<int, std::string>> product_test1(sayten::types::make_product(1, std::string("test")));
    test_comonoidal<sayten::types::product<std::string, int>> product_test2(sayten::types::make_product(std::string("test"), 1));

    assert(sayten::types::call_fmap(int_test, &plus_one_int) == int_test_result &&
           sayten::types::call_fmap(int_test, lambda_plus_one_int) == int_test_result &&
           sayten::types::call_fmap(string_test, &double_string) == string_test_result &&
           sayten::types::call_fmap(string_test, lambda_double_string) == string_test_result, 
           "Test comonoidal should produce correct value for 'call_fmap'!");
    assert(sayten::types::call_unit_untransform<test_comonoidal, int>(unit_test) == sayten::types::unit{} &&
           sayten::types::call_unit_untransform<test_comonoidal, std::string>(unit_test) == sayten::types::unit{},
           "Test comonoidal should produce correct value for 'call_unit_untransform'!");
    assert(sayten::types::call_split(int_test) == sayten::types::make_product(unit_test, int_test) &&
           sayten::types::call_split(string_test) == sayten::types::make_product(unit_test, string_test) &&
           sayten::types::call_split(product_test1) == sayten::types::make_product(int_test, string_test) &&
           sayten::types::call_split(product_test2) == sayten::types::make_product(string_test, int_test),
           "Test comonoidal should produce correct value for 'call_split'!");

    return 0;
}