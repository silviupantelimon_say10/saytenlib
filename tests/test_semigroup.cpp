#include "test.hpp"
#include "../sayten.hpp"

struct int_wrapper
{
    int value = 0;

    int_wrapper(const int &value) : value(value) {}

    int_wrapper cross(const int_wrapper &other) const noexcept
    {
        return int_wrapper(value * other.value);
    }

    constexpr bool operator==(const int_wrapper &other) const noexcept
    {
        return value == other.value;
    }

    constexpr std::strong_ordering operator<=>(const int_wrapper &other) const noexcept
    {
        return value <=> other.value;
    }
};

int plus(const int &a, const int &b) noexcept
{
    return a + b;
}

int main()
{
    assert_const<!sayten::types::is_semigroup<int>>("Type 'int' should not be a semigroup!");
    assert_const<sayten::types::is_semigroup<int_wrapper>>("Type 'int_wrapper' should be a semigroup!");
    assert_const<sayten::types::is_semigroup<sayten::types::semigroup<int, plus>>>("Type 'semigroup<int, plus>' should be a semigroup!");
    assert(int_wrapper(2).cross(int_wrapper(3)) == int_wrapper(6), "Cross product failed for type 'int_wrapper'!");
    assert(sayten::types::call_cross(int_wrapper(2), int_wrapper(3)) == int_wrapper(6), "Generic cross product call failed for type 'int_wrapper'!");
    assert(sayten::types::semigroup<int, plus>(2).cross(sayten::types::semigroup<int, plus>(3)) == 5, "Cross product failed for type 'semigroup<int, plus>'!");
    assert(sayten::types::semigroup<int, plus>(2).cross(3) == 5, "Cross product with implicit cast failed for type 'semigroup<int, plus>'!");
    assert(sayten::types::call_cross(sayten::types::semigroup<int, plus>(2), sayten::types::semigroup<int, plus>(3)) == 5, "Generic cross product call failed for type 'semigroup<int, plus>'!");
    assert(sayten::types::call_cross<sayten::types::semigroup<int, plus>>(2, 3) == 5, "Generic cross product call with implicit cast failed for type 'semigroup<int, plus>'!");

    return 0;
}