#include "test.hpp"
#include "../sayten.hpp"

int main()
{
    assert_const<sayten::types::has_fmap<test_monoidal>>("Test monoidal should have a proper 'fmap'!");
    assert_const<sayten::types::functor<test_monoidal>>("Test monoidal should be a functor!");
    assert_const<sayten::types::has_unit_transform<test_monoidal>>("Test monoidal should have a proper 'unit_transform'!");
    assert_const<sayten::types::has_merge<test_monoidal>>("Test monoidal should have a proper 'merge'!");
    assert_const<sayten::types::monoidal<test_monoidal>>("Test monoidal should be a monoidal functor!");

    test_monoidal<int> int_test(1);
    test_monoidal<std::string> string_test(test_string);
    test_monoidal<int> int_test_result(2);
    test_monoidal<std::string> string_test_result(test_string_doubled);
    test_monoidal<sayten::types::unit> unit_test(sayten::types::unit{});
    test_monoidal<sayten::types::product<int, std::string>> product_test1(sayten::types::make_product(1, std::string("test")));
    test_monoidal<sayten::types::product<std::string, int>> product_test2(sayten::types::make_product(std::string("test"), 1));

    assert(sayten::types::call_fmap(int_test, &plus_one_int) == int_test_result &&
           sayten::types::call_fmap(int_test, lambda_plus_one_int) == int_test_result &&
           sayten::types::call_fmap(string_test, &double_string) == string_test_result &&
           sayten::types::call_fmap(string_test, lambda_double_string) == string_test_result, 
           "Test monoidal should produce correct value for 'call_fmap'!");
    assert(sayten::types::call_unit_transform<test_monoidal, int>(sayten::types::unit()) == unit_test &&
           sayten::types::call_unit_transform<test_monoidal, std::string>(sayten::types::unit()) == unit_test,
           "Test monoidal should produce correct value for 'call_unit_transform'!");
    assert(sayten::types::call_merge(int_test, string_test) == product_test1 &&
           sayten::types::call_merge(string_test, int_test) == product_test2,
           "Test monoidal should produce correct value for 'call_merge'!");

    return 0;
}