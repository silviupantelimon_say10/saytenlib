#include <string>
#include "../sayten.hpp"

int increment(const int &a)
{
    return a + 1;
}

std::string double_str(const std::string &str)
{
    return str + str;
}

int main()
{
    sayten::types::either<int, std::string> either_string("test");
    sayten::types::either<int, std::string> either_int(5);

    if constexpr (!sayten::types::has_bimap<sayten::types::either>) {
        return 1;
    }

    if constexpr (!sayten::types::has_first_map<sayten::types::either>) {
        return 1;
    }

    if constexpr (!sayten::types::has_second_map<sayten::types::either>) {
        return 1;
    }

    if constexpr (!sayten::types::bifunctor<sayten::types::either>) {
        return 1;
    }

    if constexpr (!sayten::types::bifunctor_instance<sayten::types::either<int, std::string>>) {
        return 1;
    }

    if (!either_int.has_first()) {
        return 1;
    }

    if (!either_string.has_second()) {
        return 1;
    }

    if (!either_int.bimap(increment, double_str).has_first()) {
        return 1;
    }

    if (!either_string.bimap(increment, double_str).has_second()) {
        return 1;
    }

    if (either_int.bimap(increment, double_str).get_first() != 6) {
        return 1;
    }

    if (either_string.bimap(increment, double_str).get_second() != "testtest") {
        return 1;
    }

    if (!either_int.first_map(increment).has_first()) {
        return 1;
    }

    if (!either_string.second_map(double_str).has_second()) {
        return 1;
    }

    if (either_int.first_map(increment).get_first() != 6) {
        return 1;
    }

    if (either_string.second_map(double_str).get_second() != "testtest") {
        return 1;
    }

    sayten::types::either<sayten::types::either<int, std::string>, std::string> either_int_doubled_first(either_int);
    sayten::types::either<sayten::types::either<int, std::string>, std::string> either_string_doubled_first(either_string);

    if (!either_int_doubled_first.first_join().has_first()) {
        return 1;
    }

    if (either_int_doubled_first.first_join().get_first() != 5) {
        return 1;
    }

    if (!either_string_doubled_first.first_join().has_second()) {
        return 1;
    }

    if (either_string_doubled_first.first_join().get_second() != "test") {
        return 1;
    }

    sayten::types::either<int, sayten::types::either<int, std::string>> either_int_doubled_second(either_int);
    sayten::types::either<int, sayten::types::either<int, std::string>> either_string_doubled_second(either_string);

    if (!either_int_doubled_second.second_join().has_first()) {
        return 1;
    }

    if (either_int_doubled_second.second_join().get_first() != 5) {
        return 1;
    }

    if (!either_string_doubled_second.second_join().has_second()) {
        return 1;
    }

    if (either_string_doubled_second.second_join().get_second() != "test") {
        return 1;
    }

    auto binder_int = [] (int x) { return sayten::types::either<int, std::string>(std::to_string(x)); };
    auto binder_string = [] (std::string x) { return sayten::types::either<int, std::string>(static_cast<int>(x.length())); }; 

    if (!either_int.first_bind(binder_int).has_second()) {
        return 1;
    }

    if (either_int.first_bind(binder_int).get_second() != "5") {
        return 1;
    }

    if (!either_string.second_bind(binder_string).has_first()) {
        return 1;
    }

    if (either_string.second_bind(binder_string).get_first() != 4) {
        return 1;
    }

    if (!either_int.bibind(binder_int, binder_string).has_second()) {
        return 1;
    }

    if (either_int.bibind(binder_int, binder_string).get_second() != "5") {
        return 1;
    }

    if (!either_string.bibind(binder_int, binder_string).has_first()) {
        return 1;
    }

    if (either_string.bibind(binder_int, binder_string).get_first() != 4) {
        return 1;
    }

    auto int_combiner = [] (const int &x, const int &y) { return x + y; };
    auto string_combiner = [] (const std::string &x, const std::string &y) { return x + y; };
    sayten::types::either<int, std::string> combined_int = either_int.combine_first(int_combiner, either_int);
    sayten::types::either<int, std::string> other_either_string("other");
    sayten::types::either<int, std::string> combined_string = either_string.combine_first(int_combiner, other_either_string);

    if (!combined_int.has_first()) {
        return 1;
    }

    if (combined_int.get_first() != 10) {
        return 1;
    }

    if (!combined_string.has_second()) {
        return 1;
    }

    if (combined_string.get_second() != "test") {
        return 1;
    }

    combined_string = other_either_string.combine_first(int_combiner, either_string);

    if (!combined_string.has_second()) {
        return 1;
    }

    if (combined_string.get_second() != "other") {
        return 1;
    }

    combined_string = combined_int.combine_first(int_combiner, either_string);

    if (!combined_string.has_second()) {
        return 1;
    }

    if (combined_string.get_second() != "test") {
        return 1;
    }

    sayten::types::either<int, std::string> other_either_int(7);
    combined_int = either_int.combine_second(string_combiner, other_either_int);
    combined_string = either_string.combine_second(string_combiner, either_string);

    if (!combined_string.has_second()) {
        return 1;
    }

    if (combined_string.get_second() != "testtest") {
        return 1;
    }

    if (!combined_int.has_first()) {
        return 1;
    }

    if (combined_int.get_first() != 5) {
        return 1;
    }

    combined_int = other_either_int.combine_second(string_combiner, either_int);

    if (!combined_int.has_first()) {
        return 1;
    }

    if (combined_int.get_first() != 7) {
        return 1;
    }

    combined_int = combined_string.combine_second(string_combiner, either_int);

    if (!combined_int.has_first()) {
        return 1;
    }

    if (combined_int.get_first() != 5) {
        return 1;
    }

    return 0;
}