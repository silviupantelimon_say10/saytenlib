#ifndef TEST_HPP
#define TEST_HPP

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "../sayten.hpp"

const std::string test_string("test");
const std::string test_string_doubled("testtest");

template<bool value>
constexpr void assert_const(const std::string &message)
{
    if constexpr (!value) {
        std::cerr << message << "\r\n";
        std::exit(1);
    }
}

constexpr void assert(const bool &value, const std::string &message)
{
    if (!value) {
        std::cerr << message << "\r\n";
        std::exit(1);
    }
}

constexpr int plus_one_int(const int &value) noexcept
{
    return value + 1;
}

const auto lambda_plus_one_int = [] (const int &value) { return value + 1; };

std::string double_string(const std::string &value) noexcept
{
    return value + value;
}

const auto lambda_double_string = [] (const std::string &value) { return value + value; };

std::string int_to_string(const int &value) noexcept
{
    return std::to_string(value);
}

const auto lambda_int_to_string = [] (const int &value) { return std::to_string(value); };

std::string bifunction(const int &int_value, const double &double_value) noexcept
{
    std::ostringstream ss;
    ss << std::setprecision(1) << test_string << ' ' << int_value << ' ' << double_value;
    
    return ss.str();
}

const auto lambda_bifunction = [] (const int &int_value, const double &double_value) { return bifunction(int_value, double_value); };

template<template<typename> typename derived_t, typename value_t>
struct test_functor_base
{
    const value_t value;

    test_functor_base() = delete;
    constexpr test_functor_base(const value_t &value) : value(value) {}
    
    [[nodiscard]]
    constexpr bool operator==(const derived_t<value_t> &value_b) const
    requires std::equality_comparable<value_t>
    {
        return value == value_b.value;
    }

    [[nodiscard]]
    constexpr auto operator<=>(const derived_t<value_t> &value_b) const
    requires std::three_way_comparable<value_t>
    {
        return *this <=> value_b.value;
    }

    template<typename map_function_t>
    requires sayten::types::fmap_requirement<value_t, map_function_t>
    [[nodiscard]]
    constexpr derived_t<typename sayten::unwrap_function<map_function_t>::result_type> fmap(const map_function_t &map_function) const
    {
        return derived_t<typename sayten::unwrap_function<map_function_t>::result_type>(map_function(value));
    }
};

template<typename value_t>
struct test_functor : public test_functor_base<test_functor, value_t>
{
    constexpr test_functor(const value_t &value) : test_functor_base<test_functor, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_contravariant_base
{
    const std::function<std::string(const value_t &)> get_str_func;

    test_contravariant_base() = delete;
    constexpr test_contravariant_base(const std::function<std::string(const value_t &)> &get_str_func) : get_str_func(get_str_func) {}

    constexpr std::string get_str(const value_t &value) const
    {
        return get_str_func(value);
    }

    template<typename contramap_function_t>
    requires sayten::types::contramap_requirement<value_t, contramap_function_t>
    [[nodiscard]]
    constexpr auto contramap(const contramap_function_t &contramap_function) const
    {
        return derived_t<typename sayten::unwrap_function<contramap_function_t>::arg_no_cvref_type<0>>([&get_str_func = get_str_func, &contramap_function] (const typename sayten::unwrap_function<contramap_function_t>::arg_no_cvref_type<0> &value) { return get_str_func(contramap_function(value)); });
    }
};

template<typename value_t>
struct test_contravariant : public test_contravariant_base<test_contravariant, value_t>
{
    constexpr test_contravariant(const std::function<std::string(const value_t &)> &get_str_func) : test_contravariant_base<test_contravariant, value_t>(get_str_func) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base : public test_functor_base<derived_t, value_t>
{
    constexpr test_monoidal_base(const value_t &value) : test_functor_base<derived_t, value_t>(value) {}

    [[nodiscard]]
    constexpr static derived_t<sayten::types::unit> unit_transform(const sayten::types::unit &value) 
    { 
        return derived_t<sayten::types::unit>(value); 
    }

    template<typename other_value_t>
    [[nodiscard]]
    constexpr derived_t<sayten::types::product<value_t, other_value_t>> merge(const derived_t<other_value_t> &other) const
    {
        return derived_t<sayten::types::product<value_t, other_value_t>>(sayten::types::product<value_t, other_value_t>(this->value, other.value)); 
    }
};

template<typename value_t>
struct test_monoidal : public test_monoidal_base<test_monoidal, value_t>
{
    constexpr test_monoidal(const value_t &value) : test_monoidal_base<test_monoidal, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_comonoidal_base : public test_functor_base<derived_t, value_t>
{
    constexpr test_comonoidal_base(const value_t &value) : test_functor_base<derived_t, value_t>(value) {}

    [[nodiscard]]
    constexpr static sayten::types::unit unit_untransform(const derived_t<sayten::types::unit> &value) 
    { 
        return value.value;
    }

    [[nodiscard]]
    constexpr auto split() const
    {
        if constexpr (!sayten::types::product_instance<value_t>) {
            return sayten::types::product<derived_t<sayten::types::unit>, derived_t<value_t>>(derived_t<sayten::types::unit>(sayten::types::unit()), derived_t<value_t>(this->value));
        } else {
            return sayten::types::product<derived_t<sayten::types::product_first_value_type<value_t>>, derived_t<sayten::types::product_second_value_type<value_t>>>(
                derived_t<sayten::types::product_first_value_type<value_t>>(this->value.get_first()), 
                derived_t<sayten::types::product_second_value_type<value_t>>(this->value.get_second()));
        }
    }
};

template<typename value_t>
struct test_comonoidal : public test_comonoidal_base<test_comonoidal, value_t>
{
    constexpr test_comonoidal(const value_t &value) : test_comonoidal_base<test_comonoidal, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_pure : public test_monoidal_base<derived_t, value_t>
{
    constexpr test_monoidal_base_with_pure(const value_t &value) : test_monoidal_base<derived_t, value_t>(value) {}

    [[nodiscard]]
    constexpr static derived_t<value_t> pure(const value_t &value) 
    { 
        return derived_t<value_t>(value); 
    }
};

template<typename value_t>
struct test_monoidal_with_pure : public test_monoidal_base_with_pure<test_monoidal_with_pure, value_t>
{
    constexpr test_monoidal_with_pure(const value_t &value) : test_monoidal_base_with_pure<test_monoidal_with_pure, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_apply : public test_monoidal_base<derived_t, value_t>
{
    constexpr test_monoidal_base_with_apply(const value_t &value) : test_monoidal_base<derived_t, value_t>(value) {}

    template<typename other_value_t>
    requires sayten::types::apply_requirement<other_value_t, value_t>
    [[nodiscard]]
    constexpr auto apply(const derived_t<other_value_t> &other) const
    {
        return derived_t<typename sayten::unwrap_function<value_t>::result_type>(this->value(other.value));
    }
};

template<typename value_t>
struct test_monoidal_with_apply : public test_monoidal_base_with_apply<test_monoidal_with_apply, value_t>
{
    constexpr test_monoidal_with_apply(const value_t &value) : test_monoidal_base_with_apply<test_monoidal_with_apply, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_reverse_apply : public test_monoidal_base<derived_t, value_t>
{
    constexpr test_monoidal_base_with_reverse_apply(const value_t &value) : test_monoidal_base<derived_t, value_t>(value) {}

    template<typename other_value_t>
    requires sayten::types::apply_requirement<value_t, other_value_t>
    [[nodiscard]]
    constexpr auto reverse_apply(const derived_t<other_value_t> &other) const
    {
        return derived_t<typename sayten::unwrap_function<other_value_t>::result_type>(other.value(this->value));
    }
};

template<typename value_t>
struct test_monoidal_with_reverse_apply : public test_monoidal_base_with_reverse_apply<test_monoidal_with_reverse_apply, value_t>
{
    constexpr test_monoidal_with_reverse_apply(const value_t &value) : test_monoidal_base_with_reverse_apply<test_monoidal_with_reverse_apply, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_lift2 : public test_monoidal_base<derived_t, value_t>
{
    constexpr test_monoidal_base_with_lift2(const value_t &value) : test_monoidal_base<derived_t, value_t>(value) {}

    template<typename function_t>
    requires sayten::types::lift2_requirement<value_t, function_t>
    [[nodiscard]]
    constexpr auto lift2(const function_t &function, const derived_t<typename sayten::unwrap_function<function_t>::arg_no_cvref_type<1>> &other) const
    {
        return derived_t<typename sayten::unwrap_function<function_t>::result_type>(function(this->value, other.value));
    }
};

template<typename value_t>
struct test_monoidal_with_lift2 : public test_monoidal_base_with_lift2<test_monoidal_with_lift2, value_t>
{
    constexpr test_monoidal_with_lift2(const value_t &value) : test_monoidal_base_with_lift2<test_monoidal_with_lift2, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_apply_and_reverse_apply : public test_monoidal_base_with_apply<derived_t, value_t>
{
    constexpr test_monoidal_base_with_apply_and_reverse_apply(const value_t &value) : test_monoidal_base_with_apply<derived_t, value_t>(value) {}

    template<typename other_value_t>
    requires sayten::types::apply_requirement<value_t, other_value_t>
    [[nodiscard]]
    constexpr auto reverse_apply(const derived_t<other_value_t> &other) const
    {
        return derived_t<typename sayten::unwrap_function<other_value_t>::result_type>(other.value(this->value));
    }
};

template<typename value_t>
struct test_monoidal_with_apply_and_reverse_apply : public test_monoidal_base_with_apply_and_reverse_apply<test_monoidal_with_apply_and_reverse_apply, value_t>
{
    constexpr test_monoidal_with_apply_and_reverse_apply(const value_t &value) : test_monoidal_base_with_apply_and_reverse_apply<test_monoidal_with_apply_and_reverse_apply, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_apply_and_lift2 : public test_monoidal_base_with_apply<derived_t, value_t>
{
    constexpr test_monoidal_base_with_apply_and_lift2(const value_t &value) : test_monoidal_base_with_apply<derived_t, value_t>(value) {}

    template<typename function_t>
    requires sayten::types::lift2_requirement<value_t, function_t>
    [[nodiscard]]
    constexpr auto lift2(const function_t &function, const derived_t<typename sayten::unwrap_function<function_t>::arg_no_cvref_type<1>> &other) const
    {
        return derived_t<typename sayten::unwrap_function<function_t>::result_type>(function(this->value, other.value));
    }
};

template<typename value_t>
struct test_monoidal_with_apply_and_lift2 : public test_monoidal_base_with_apply_and_lift2<test_monoidal_with_apply_and_lift2, value_t>
{
    constexpr test_monoidal_with_apply_and_lift2(const value_t &value) : test_monoidal_base_with_apply_and_lift2<test_monoidal_with_apply_and_lift2, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_reverse_apply_and_lift2 : public test_monoidal_base_with_reverse_apply<derived_t, value_t>
{
    constexpr test_monoidal_base_with_reverse_apply_and_lift2(const value_t &value) : test_monoidal_base_with_reverse_apply<derived_t, value_t>(value) {}

    template<typename function_t>
    requires sayten::types::lift2_requirement<value_t, function_t>
    [[nodiscard]]
    constexpr auto lift2(const function_t &function, const derived_t<typename sayten::unwrap_function<function_t>::arg_no_cvref_type<1>> &other) const
    {
        return derived_t<typename sayten::unwrap_function<function_t>::result_type>(function(this->value, other.value));
    }
};

template<typename value_t>
struct test_monoidal_with_reverse_apply_and_lift2 : public test_monoidal_base_with_reverse_apply_and_lift2<test_monoidal_with_reverse_apply_and_lift2, value_t>
{
    constexpr test_monoidal_with_reverse_apply_and_lift2(const value_t &value) : test_monoidal_base_with_reverse_apply_and_lift2<test_monoidal_with_reverse_apply_and_lift2, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_monoidal_base_with_apply_and_reverse_apply_and_lift2 : public test_monoidal_base_with_apply_and_reverse_apply<derived_t, value_t>
{
    constexpr test_monoidal_base_with_apply_and_reverse_apply_and_lift2(const value_t &value) : test_monoidal_base_with_apply_and_reverse_apply<derived_t, value_t>(value) {}

    template<typename function_t>
    requires sayten::types::lift2_requirement<value_t, function_t>
    [[nodiscard]]
    constexpr auto lift2(const function_t &function, const derived_t<typename sayten::unwrap_function<function_t>::arg_no_cvref_type<1>> &other) const
    {
        return derived_t<typename sayten::unwrap_function<function_t>::result_type>(function(this->value, other.value));
    }
};

template<typename value_t>
struct test_monoidal_with_apply_and_reverse_apply_and_lift2 : public test_monoidal_base_with_apply_and_reverse_apply_and_lift2<test_monoidal_with_apply_and_reverse_apply_and_lift2, value_t>
{
    constexpr test_monoidal_with_apply_and_reverse_apply_and_lift2(const value_t &value) : test_monoidal_base_with_apply_and_reverse_apply_and_lift2<test_monoidal_with_apply_and_reverse_apply_and_lift2, value_t>(value) {}
};

template<template<typename> typename derived_t, typename value_t>
struct test_applicative_base : public test_monoidal_base_with_apply_and_reverse_apply_and_lift2<derived_t, value_t>
{
    constexpr test_applicative_base(const value_t &value) : test_monoidal_base_with_apply_and_reverse_apply_and_lift2<derived_t, value_t>(value) {}

    [[nodiscard]]
    constexpr static derived_t<value_t> pure(const value_t &value) 
    { 
        return derived_t<value_t>(value); 
    }
};

template<typename value_t>
struct test_applicative : public test_applicative_base<test_applicative, value_t>
{
    constexpr test_applicative(const value_t &value) : test_applicative_base<test_applicative, value_t>(value) {}
};

#endif