#include <string>
#include "../sayten.hpp"

template<typename container_t>
requires sayten::qualified_container<container_t> &&
std::same_as<typename container_t::value_type, int>
sayten::contained_enumerable<container_t> simple_generator() noexcept
{
    for (int i = -10; i <= 10; ++i) {
        co_yield i;
    }
}

template<typename container_t>
requires sayten::qualified_container<container_t> &&
std::same_as<typename container_t::value_type, int>
sayten::contained_enumerable<container_t> simple_generator_n(int n) noexcept
{
    for (int i = 1; i <= n; ++i) {
        co_yield i;
    }
}

template<typename container_t>
requires sayten::qualified_container<container_t> &&
std::same_as<typename container_t::value_type, std::pair<const int, int>>
sayten::contained_enumerable<container_t> map_generator() noexcept
{
    for (int i = -10; i <= 10; ++i) {
        co_yield std::pair<const int, int>(i, i);
    }
}

std::string double_string(std::string str) noexcept
{
    std::string rev = str;
    std::reverse(rev.begin(), rev.end());

    return str + rev;
}

std::pair<const std::string, std::string> double_string_pair(std::pair<const std::string, std::string> pair) noexcept
{
    std::string rev_fist = pair.first;
    std::string rev_second = pair.second;
    std::reverse(rev_fist.begin(), rev_fist.end());
    std::reverse(rev_second.begin(), rev_second.end());

    return std::pair<const std::string, std::string>(pair.first + rev_fist, pair.second + rev_second);
}

template<typename stream_t>
bool test_simple()
{
    std::function<long(int)> square = [] (int x) -> long { return x * x; };
    auto to_string = [] (long x) -> std::string { return std::to_string(x); };

    using replaced_stream_t = typename stream_t::replace_value_type_t<std::string>;
    using container_t = typename replaced_stream_t::container_type;
    stream_t stream = stream_t(std::function(simple_generator<typename stream_t::container_type>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = -10; i <= 10; ++i)
        sayten::add(result, double_string(to_string(square(i))));
    
    size_t i = 0;

    for (std::string value : stream
                        .fmap(square)
                        .fmap(to_string)
                        .fmap(double_string)()) {
        
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 11;
    } else {
        return i == 21;
    }
}

template<typename stream_t>
bool test_map()
{
    std::function<std::pair<const long, long>(std::pair<const int, int>)> square = [] (std::pair<const int, int> x) -> std::pair<const long, long> { return std::pair<const long, long>(x.first * x.first, x.second * x.second); };
    auto to_string = [] (std::pair<const long, long> x) -> std::pair<const std::string, std::string> { return std::pair<const std::string, std::string>(std::to_string(x.first), std::to_string(x.second)); };

    using replaced_stream_t = typename stream_t::replace_value_type_t<std::pair<const std::string, std::string>>;
    using container_t = typename replaced_stream_t::container_type;
    stream_t stream = stream_t(std::function(map_generator<typename stream_t::container_type>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = -10; i <= 10; ++i)
        sayten::add(result, double_string_pair(to_string(square(std::pair<const int, int>(i, i)))));
    
    size_t i = 0;

    for (std::pair<const std::string, std::string> value : stream
                        .fmap(square)
                        .fmap(to_string)
                        .fmap(double_string_pair)()) {
        
        iterator found = result.find(value.first);

		if (found == result.end() || found->second != value.second)
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 11;
    } else {
        return i == 21;
    }
}

template<template<typename> typename stream_t, typename value_t>
bool test_unit_transform()
{
    typename stream_t<sayten::types::unit>::container_type result = stream_t<value_t>::unit_transform(sayten::types::unit())().collect_all();

    if (result.begin() == result.end()) {
        return false;
    }

    if (sayten::types::unit() != *result.begin()) {
        return false;
    }

    result = sayten::types::call_unit_transform<stream_t, value_t>(sayten::types::unit())().collect_all();

    if (result.begin() == result.end()) {
        return false;
    }

    if (sayten::types::unit() != *result.begin()) {
        return false;
    }

    return true;
}

template<template<typename> typename stream_t, typename value_t>
bool test_pure(value_t value)
{
    typename stream_t<value_t>::container_type result = stream_t<value_t>::pure(value)().collect_all();

    if (result.begin() == result.end()) {
        return false;
    }

    if (value != *result.begin()) {
        return false;
    }

    result = sayten::types::call_pure<stream_t, value_t>(value)().collect_all();

    if (result.begin() == result.end()) {
        return false;
    }

    if (value != *result.begin()) {
        return false;
    }

    return true;
}

template<typename stream_t>
bool test_merge()
{
    auto to_string = [] (int x) -> std::string { return std::to_string(x); };
    
    using replaced_stream_t = typename stream_t::replace_value_type_t<sayten::types::product<int, std::string>>;
    using container_t = typename replaced_stream_t::container_type;
    stream_t stream = stream_t(std::function(simple_generator<typename stream_t::container_type>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = -10; i <= 10; ++i)
        for (int j = -10; j <= 10; ++j)
            sayten::add(result, sayten::types::product<int, std::string>(i, to_string(j)));
    
    size_t i = 0;

    for (auto value : stream.merge(stream.fmap(to_string))()) {
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 11 * 11;
    } else {
        return i == 21 * 21;
    }
}

template<typename stream_t>
bool test_reverse_apply()
{
    std::function<long(int)> square = [] (int x) -> long { return x * x; };
    auto to_string = [] (long x) -> std::string { return std::to_string(x); };
    
    using replaced_stream_t = typename stream_t::replace_value_type_t<std::string>;
    using container_t = typename replaced_stream_t::container_type;
    stream_t stream = stream_t(std::function(simple_generator<typename stream_t::container_type>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = -10; i <= 10; ++i)
        sayten::add(result, double_string(to_string(square(i))));
    
    size_t i = 0;

    using first_applicative_stream_t = typename stream_t::replace_value_type_t<decltype(square)>;
    using second_applicative_stream_t = typename stream_t::replace_value_type_t<decltype(to_string)>;
    using third_applicative_stream_t = typename stream_t::replace_value_type_t<std::function<std::string(std::string)>>;

    for (std::string value : stream
                        .reverse_apply(first_applicative_stream_t::pure(square))
                        .reverse_apply(second_applicative_stream_t::pure(to_string))
                        .reverse_apply(third_applicative_stream_t::pure(std::function<std::string(std::string)>(double_string)))()) {
        
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 11;
    } else {
        return i == 21;
    }
}

template<typename stream_t>
bool test_apply()
{
    std::function<long(int)> square = [] (int x) -> long { return x * x; };
    auto to_string = [] (long x) -> std::string { return std::to_string(x); };
    
    using replaced_stream_t = typename stream_t::replace_value_type_t<std::string>;
    using container_t = typename replaced_stream_t::container_type;
    stream_t stream = stream_t(std::function(simple_generator<typename stream_t::container_type>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = -10; i <= 10; ++i)
        sayten::add(result, double_string(to_string(square(i))));
    
    size_t i = 0;

    using first_applicative_stream_t = typename stream_t::replace_value_type_t<decltype(square)>;
    using second_applicative_stream_t = typename stream_t::replace_value_type_t<decltype(to_string)>;
    using third_applicative_stream_t = typename stream_t::replace_value_type_t<std::function<std::string(std::string)>>;

    for (std::string value : third_applicative_stream_t::pure(std::function<std::string(std::string)>(double_string))
                             .apply(second_applicative_stream_t::pure(to_string)
                                     .apply(first_applicative_stream_t::pure(square)
                                             .apply(stream)))())
    {
        
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 11;
    } else {
        return i == 21;
    }
}

template<typename container_t>
requires sayten::qualified_container<container_t> &&
std::same_as<typename container_t::value_type, std::string>
sayten::contained_enumerable<container_t> string_generator() noexcept
{
    for (int i = -10; i <= 10; ++i) {
        co_yield std::to_string(i);
    }
}

template<typename stream_t>
bool test_lift()
{
    std::function<long(int, int)> product = [] (int x, int y) -> long { return x * y; };
    auto add_string = [] (long x, std::string str) -> std::string { return std::to_string(x) + str; };
    
    using replaced_stream_t = typename stream_t::replace_value_type_t<std::string>;
    using container_t = typename replaced_stream_t::container_type;
    stream_t stream = stream_t(std::function(simple_generator<typename stream_t::container_type>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = -10; i <= 10; ++i)
        for (int j = -10; j <= 10; ++j)
            for (int k = -10; k <= 10; ++k)
                sayten::add(result, add_string(product(i, j), std::to_string(k)));
    
    size_t i = 0;

    for (std::string value : stream
                        .lift2(product, stream)
                        .lift2(add_string, replaced_stream_t(std::function(string_generator<container_t>)))()) {
        
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == result.size();
    } else {
        return i == 21 * 21 * 21;
    }
}

template<template<typename> typename stream_t>
sayten::contained_enumerable<typename stream_t<stream_t<int>>::container_type> double_generator()
{
    for (int i = 0; i < 10; ++i) {
        co_yield stream_t<int>(std::function(simple_generator<typename stream_t<int>::container_type>));
    }
}

template<template<typename> typename stream_t>
bool test_join()
{
    using container_t = typename stream_t<int>::container_type;
    stream_t<stream_t<int>> stream = stream_t<stream_t<int>>(std::function(double_generator<stream_t>));
    container_t result;
    using iterator = container_t::iterator;

    for (int i = 0; i < 10; ++i) {
        for (int j = -10; j <= 10; ++j) {
            sayten::add(result, j);
        }
    }

    size_t i = 0;

    for (int value : stream.join()()) {
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 21;
    } else {
        return i == 21 * 10;
    }
}

template<template<typename> typename stream_t>
bool test_bind()
{
    using container_t = typename stream_t<std::string>::container_type;
    auto func = [] (int x) -> stream_t<std::string> { 
            auto to_string = [] (int x) -> std::string { return std::to_string(x); };
            auto generator = [x = x] () { return simple_generator_n<typename stream_t<int>::container_type>(x); };

            return stream_t<int>(generator).fmap(to_string); 
        };
    auto generator = [] () { return simple_generator_n<typename stream_t<int>::container_type>(10); };
    stream_t<int> stream = stream_t<int>(generator);
    container_t result;
    using iterator = container_t::iterator;

    for (int i = 1; i <= 10; ++i) {
        for (int j = 1; j <= i; ++j) {
            sayten::add(result, std::to_string(j));
        }
    }

    size_t i = 0;

    for (std::string value : stream.bind(func)()) {
        iterator found = std::find(result.begin(), result.end(), value);

		if (found == result.end())
			return false;

        ++i;
    }

    if constexpr (sayten::unique_keys<container_t>) {
        return i == 10;
    } else {
        return i == 10 * 11 / 2;
    }
}

int main()
{
    if constexpr (!sayten::types::functor<sayten::types::vector_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::functor<sayten::types::list_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::functor<sayten::types::forward_list_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::functor<sayten::types::deque_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::applicative<sayten::types::vector_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::applicative<sayten::types::list_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::applicative<sayten::types::forward_list_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::applicative<sayten::types::deque_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::monad<sayten::types::vector_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::monad<sayten::types::list_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::monad<sayten::types::forward_list_stream_functor>) {
        return 1;
    }

    if constexpr (!sayten::types::monad<sayten::types::deque_stream_functor>) {
        return 1;
    }

    if (!test_simple<sayten::types::vector_stream_functor<int>>()) {
        return 1;
    }

    if (!test_simple<sayten::types::list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_simple<sayten::types::forward_list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_simple<sayten::types::deque_stream_functor<int>>()) {
        return 1;
    }

    if (!test_unit_transform<sayten::types::vector_stream_functor, int>()) {
        return 1;
    }

    if (!test_unit_transform<sayten::types::list_stream_functor, int>()) {
        return 1;
    }

    if (!test_unit_transform<sayten::types::forward_list_stream_functor, int>()) {
        return 1;
    }

    if (!test_unit_transform<sayten::types::deque_stream_functor, int>()) {
        return 1;
    }

    if (!test_merge<sayten::types::vector_stream_functor<int>>()) {
        return 1;
    }

    if (!test_merge<sayten::types::list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_merge<sayten::types::forward_list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_merge<sayten::types::deque_stream_functor<int>>()) {
        return 1;
    }

    if (!test_pure<sayten::types::vector_stream_functor, int>(1)) {
        return 1;
    }

    if (!test_pure<sayten::types::list_stream_functor, int>(1)) {
        return 1;
    }

    if (!test_pure<sayten::types::forward_list_stream_functor, int>(1)) {
        return 1;
    }

    if (!test_pure<sayten::types::deque_stream_functor, int>(1)) {
        return 1;
    }

    if (!test_reverse_apply<sayten::types::vector_stream_functor<int>>()) {
        return 1;
    }

    if (!test_reverse_apply<sayten::types::list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_reverse_apply<sayten::types::forward_list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_reverse_apply<sayten::types::deque_stream_functor<int>>()) {
        return 1;
    }

    if (!test_apply<sayten::types::vector_stream_functor<int>>()) {
        return 1;
    }

    if (!test_apply<sayten::types::list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_apply<sayten::types::forward_list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_apply<sayten::types::deque_stream_functor<int>>()) {
        return 1;
    }

    if (!test_lift<sayten::types::vector_stream_functor<int>>()) {
        return 1;
    }

    if (!test_lift<sayten::types::list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_lift<sayten::types::forward_list_stream_functor<int>>()) {
        return 1;
    }

    if (!test_lift<sayten::types::deque_stream_functor<int>>()) {
        return 1;
    }

    if (!test_join<sayten::types::vector_stream_functor>()) {
        return 1;
    }

    if (!test_join<sayten::types::list_stream_functor>()) {
        return 1;
    }

    if (!test_join<sayten::types::forward_list_stream_functor>()) {
        return 1;
    }

    if (!test_join<sayten::types::deque_stream_functor>()) {
        return 1;
    }
    
    if (!test_bind<sayten::types::vector_stream_functor>()) {
        return 1;
    }

    if (!test_bind<sayten::types::list_stream_functor>()) {
        return 1;
    }

    if (!test_bind<sayten::types::forward_list_stream_functor>()) {
        return 1;
    }

    if (!test_bind<sayten::types::deque_stream_functor>()) {
        return 1;
    }

    return 0;
}