
#include "test.hpp"
#include "../sayten.hpp"

std::string double_int_and_string(std::string str, int x) noexcept
{
    return str + str + std::to_string(2 * x);
}

int main()
{
    auto lambda_func = [] (int x) { return x + 1; };
    auto lambda_bifunc = [] (int x, std::string str) { return std::to_string(x) + str; };
    std::function<std::string(std::string)> simple_func(double_string);
    std::function<std::string(std::string, int)> simple_bifunc(double_int_and_string);
    auto maybe_to_string = [] (int x) { return sayten::types::maybe(std::to_string(x)); };

    const std::string test_str = "abc";

    assert_const<sayten::types::functor<sayten::types::maybe>>("Template 'maybe' should be a functor!");
    assert_const<sayten::types::applicative<sayten::types::maybe>>("Template 'maybe' should be an applicative!");
    assert_const<sayten::types::monad<sayten::types::maybe>>("Template 'maybe' should be a monad!");
    assert_const<sayten::types::alternative<sayten::types::maybe>>("Template 'maybe' should be a alternative!");
    
    sayten::types::maybe<std::string> str_result = sayten::types::maybe<int>().fmap(lambda_func).fmap(int_to_string).fmap(simple_func);

    assert(!str_result.has_value(), "The 'fmap' result on an empty 'maybe' functor returned something!");

    str_result = sayten::types::maybe(1).fmap(lambda_func).fmap(int_to_string).fmap(simple_func);
    assert(str_result.has_value(), "The 'fmap' result on a non-empty 'maybe' functor returned nothing!");
    assert(str_result.value() == simple_func(int_to_string(lambda_func(1))), "The correct 'fmap' result was not produced for the 'maybe' functor!");
    
    str_result = call_fmap<sayten::types::maybe, decltype(int_to_string)>(sayten::types::maybe<int>(), int_to_string);

    assert(!str_result.has_value(), "The generic 'fmap' call result on an empty 'maybe' functor returned something!");
    
    str_result = sayten::types::call_fmap<sayten::types::maybe, decltype(int_to_string)>(sayten::types::maybe(1), int_to_string);
    
    assert(str_result.has_value(), "The generic 'fmap' call result on a non-empty 'maybe' functor returned nothing!");
    assert(str_result.value() == int_to_string(1), "The correct generic 'fmap' call result was not produced for the 'maybe' functor!");
    assert(sayten::types::maybe<int>::unit_transform(sayten::types::unit()) == sayten::types::maybe(sayten::types::unit()), 
           "The correct 'unit_transform' result was not produced for the 'maybe' monoidal!");
    assert(sayten::types::maybe<int>::pure(1).merge(sayten::types::maybe<std::string>::pure(test_str)) == sayten::types::maybe(sayten::types::product<int, std::string>(1, test_str)), 
           "The correct 'merge' result was not produced for the 'maybe' monoidal!");
    assert(
        sayten::types::maybe<int>::pure(1) == sayten::types::maybe(1) &&
        sayten::types::maybe<std::string>::pure(test_str) == sayten::types::maybe(test_str) &&
        sayten::types::maybe<decltype(lambda_func)>::pure(lambda_func) == sayten::types::maybe(lambda_func),
        "The correct 'pure' result was not produced for the 'maybe' applicative!");
    assert(
        sayten::types::call_pure<sayten::types::maybe>(1) == sayten::types::maybe(1) &&
        sayten::types::call_pure<sayten::types::maybe>(test_str) == sayten::types::maybe(test_str) &&
        sayten::types::call_pure<sayten::types::maybe>(lambda_func) == sayten::types::maybe(lambda_func),
        "The correct generic 'pure' call result was not produced for the 'maybe' applicative!");
    assert(!sayten::types::maybe<int>().reverse_apply(sayten::types::maybe(lambda_func)).has_value() &&
           !sayten::types::maybe(1).reverse_apply(sayten::types::maybe<decltype(lambda_func)>()).has_value() &&
           !sayten::types::maybe<int>().reverse_apply(sayten::types::maybe(lambda_func)).has_value(),
           "The 'reverse_apply' result on an empty 'maybe' applicative returned something!");
    assert(!sayten::types::maybe(lambda_func).apply(sayten::types::maybe<int>()).has_value() &&
           !sayten::types::maybe<decltype(lambda_func)>().apply(sayten::types::maybe(1)).has_value() &&
           !sayten::types::maybe(lambda_func).apply(sayten::types::maybe<int>()).has_value(),
           "The 'reverse_apply' result on an empty 'maybe' applicative returned something!");

    sayten::types::maybe<int> int_result = sayten::types::maybe(1).reverse_apply(sayten::types::maybe<decltype(lambda_func)>::pure(lambda_func));
    str_result = sayten::types::maybe(1).reverse_apply(sayten::types::maybe<std::function<std::string(int)>>::pure(int_to_string));

    assert(int_result.has_value() && str_result.has_value(), 
        "The 'reverse_apply' result on an non-empty 'maybe' applicative returned nothing!");
    assert(int_result.value() == lambda_func(1) && str_result.value() == int_to_string(1),
        "The correct 'reverse_apply' result was not produced for the 'maybe' applicative!");

    int_result = sayten::types::maybe<decltype(lambda_func)>::pure(lambda_func).apply(sayten::types::maybe(1));
    str_result = sayten::types::maybe<std::function<std::string(int)>>::pure(int_to_string).apply(sayten::types::maybe(1));

    assert(int_result.has_value() && str_result.has_value(), 
        "The 'reverse_apply' result on an non-empty 'maybe' applicative returned nothing!");
    assert(int_result.value() == lambda_func(1) && str_result.value() == int_to_string(1),
        "The correct 'reverse_apply' result was not produced for the 'maybe' applicative!");

    int_result = sayten::types::call_reverse_apply<sayten::types::maybe>(sayten::types::maybe(1), sayten::types::maybe<decltype(lambda_func)>::pure(lambda_func));
    str_result = sayten::types::call_reverse_apply<sayten::types::maybe>(sayten::types::maybe(1), sayten::types::maybe<std::function<std::string(int)>>::pure(int_to_string));

    assert(int_result.has_value() && str_result.has_value(), 
        "The generic 'reverse_apply' call result on an non-empty 'maybe' applicative returned nothing!");
    assert(int_result.value() == lambda_func(1) && str_result.value() == int_to_string(1),
        "The correct generic 'reverse_apply' call result was not produced for the 'maybe' applicative!");

    int_result = sayten::types::call_apply<sayten::types::maybe>(sayten::types::maybe<decltype(lambda_func)>::pure(lambda_func), sayten::types::maybe(1));
    str_result = sayten::types::call_apply<sayten::types::maybe>(sayten::types::maybe<std::function<std::string(int)>>::pure(int_to_string), sayten::types::maybe(1));

    assert(int_result.has_value() && str_result.has_value(), 
        "The generic 'reverse_apply' call result on an non-empty 'maybe' applicative returned nothing!");
    assert(int_result.value() == lambda_func(1) && str_result.value() == int_to_string(1),
        "The correct generic 'reverse_apply' call result was not produced for the 'maybe' applicative!");

    assert(
        !sayten::types::maybe<int>().lift2(lambda_bifunc, sayten::types::maybe<std::string>()).has_value() &&
        !sayten::types::maybe(1).lift2(lambda_bifunc, sayten::types::maybe<std::string>()).has_value() &&
        !sayten::types::maybe<int>().lift2(lambda_bifunc, sayten::types::maybe(test_str)).has_value(),
        "The 'lift2' result on an empty 'maybe' applicative returned something!");

    str_result = sayten::types::maybe(1).lift2(lambda_bifunc, sayten::types::maybe(test_str));
    assert(str_result.has_value(), 
        "The 'lift2' result on an non-empty 'maybe' applicative returned nothing!");
    assert(str_result.value() == lambda_bifunc(1, test_str),
        "The correct 'lift2' result was not produced for the 'maybe' applicative!");

    str_result = call_lift2<sayten::types::maybe>(sayten::types::maybe<std::string>(test_str), simple_bifunc, sayten::types::maybe(1));

    assert(str_result.has_value(), 
        "The generic 'lift2' call result on an non-empty 'maybe' applicative returned nothing!");
    assert(str_result.value() == simple_bifunc(test_str, 1),
        "The correct generic 'lift2' call result was not produced for the 'maybe' applicative!");
    assert(!sayten::types::maybe<int>::empty.has_value(), "The empty value for the 'maybe' alternative has a value!");
    assert(
        sayten::types::maybe<int>::empty.alternative(sayten::types::maybe<int>::empty) == sayten::types::maybe<int>::empty &&
        sayten::types::maybe(1).alternative(sayten::types::maybe(2)) == 1 &&
        sayten::types::maybe(1).alternative(sayten::types::maybe<int>::empty) == 1 &&
        sayten::types::maybe<int>::empty.alternative(sayten::types::maybe(2)) == 2,
        "The correct 'alternative' result was not produced for the 'maybe' alternative!");
    assert(
        sayten::types::call_alternative(sayten::types::maybe<int>::empty, sayten::types::maybe<int>::empty) == sayten::types::maybe<int>::empty &&
        sayten::types::call_alternative(sayten::types::maybe(1), sayten::types::maybe(2)) == 1 &&
        sayten::types::call_alternative(sayten::types::maybe(1), sayten::types::maybe<int>::empty) == 1 &&
        sayten::types::call_alternative(sayten::types::maybe<int>::empty, sayten::types::maybe(2)) == 2,
        "The correct generic 'alternative' call result was not produced for the 'maybe' alternative!");
    assert(
        !sayten::types::maybe<sayten::types::maybe<int>>(sayten::types::maybe<int>()).join().has_value(),
        "The 'join' result on an empty 'maybe' monad returned something!");

    int_result = sayten::types::maybe<sayten::types::maybe<int>>(sayten::types::maybe(1)).join();
    assert(
        int_result.has_value(),
        "The 'join' result on an non-empty 'maybe' monad returned nothing!");
    assert(
        int_result.value() == 1,
        "The correct 'join' result was not produced for the 'maybe' monad!");
    assert(
        !sayten::types::maybe<sayten::types::maybe<std::string>>(sayten::types::maybe<std::string>()).join().has_value(),
        "The generic 'join' call result on an empty 'maybe' monad returned something!");

    str_result = sayten::types::call_join(sayten::types::maybe<sayten::types::maybe<std::string>>(sayten::types::maybe(test_str)));
    assert(
        str_result.has_value(),
        "The generic 'join' call result on an non-empty 'maybe' monad returned nothing!");
    assert(
        str_result.value() == test_str,
        "The correct generic 'join' call result was not produced for the 'maybe' monad!");
    assert(
        !sayten::types::maybe<int>().bind(maybe_to_string).has_value(),
        "The 'bind' result on an empty 'maybe' monad returned something!");

    str_result = sayten::types::maybe(1).bind(maybe_to_string);
    assert(
        str_result.has_value(),
        "The 'bind' result on an non-empty 'maybe' monad returned nothing!");
    assert(
        str_result.value() == maybe_to_string(1).value(),
        "The correct 'bind' call result was not produced for the 'maybe' monad!");
    assert(
        !sayten::types::call_bind(sayten::types::maybe<int>(), maybe_to_string).has_value(),
        "The generic 'bind' call result on an empty 'maybe' monad returned something!");

    str_result = sayten::types::call_bind(sayten::types::maybe(1), maybe_to_string);
    assert(
        str_result.has_value(),
        "The generic 'bind' call result on an non-empty 'maybe' monad returned nothing!");
    assert(
        str_result.value() == maybe_to_string(1).value(),
        "The correct generic 'bind' call result was not produced for the 'maybe' monad!");
    assert(
        !sayten::types::maybe<int>().then(sayten::types::maybe(test_str)) && 
        sayten::types::maybe(1).then(sayten::types::maybe(test_str)) == sayten::types::maybe(test_str),
        "The correct 'then' result was not produced for the 'maybe' monad!");
    assert(
        !sayten::types::call_then(sayten::types::maybe<int>(), sayten::types::maybe(test_str)) && 
        sayten::types::call_then(sayten::types::maybe(1), sayten::types::maybe(test_str)) == sayten::types::maybe(test_str),
        "The correct generic 'then' call result was not produced for the 'maybe' monad!");

    return 0;
}