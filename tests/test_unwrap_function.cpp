#include <string>

#include "../sayten.hpp"

bool func(int, long)
{
    return false;
}

int main()
{
    if constexpr (sayten::unwrap_function<std::function<int()>>::args_size != 0)
        return 1;

    if constexpr (sayten::unwrap_function<std::function<std::string(int)>>::args_size != 1)
        return 1;
    
    if constexpr (sayten::unwrap_function<std::function<std::string(int, long)>>::args_size != 2)
        return 1;

    if constexpr (sayten::unwrap_function<decltype(func)>::args_size != 2)
        return 1;

    auto lambda = [] (int, long) -> bool { return false; };

    if constexpr (sayten::unwrap_function<decltype(lambda)>::args_size != 2)
        return 1;

    if constexpr (!std::same_as<bool, sayten::unwrap_function<decltype(func)>::result_type>)
        return 1;

    if constexpr (!std::same_as<bool, sayten::unwrap_function<decltype(lambda)>::result_type>)
        return 1;

    if constexpr (!std::same_as<std::string, sayten::unwrap_function<std::function<std::string(int, long)>>::result_type>)
        return 1;

    if constexpr (!std::same_as<int, sayten::unwrap_function<decltype(func)>::arg_type<0>>)
        return 1;

    if constexpr (!std::same_as<int, sayten::unwrap_function<decltype(lambda)>::arg_type<0>>)
        return 1;

    if constexpr (!std::same_as<int, sayten::unwrap_function<std::function<std::string(int, long)>>::arg_type<0>>)
        return 1;

    if constexpr (!std::same_as<long, sayten::unwrap_function<decltype(func)>::arg_type<1>>)
        return 1;

    if constexpr (!std::same_as<long, sayten::unwrap_function<decltype(lambda)>::arg_type<1>>)
        return 1;

    if constexpr (!std::same_as<long, sayten::unwrap_function<std::function<std::string(int, long)>>::arg_type<1>>)
        return 1;

    auto to_compose1 = [] (int a, int b) -> int { return a + b; };
    auto to_compose2 = [] (int x) -> bool { return x % 2 == 0; };

    if (sayten::compose<decltype(to_compose2), decltype(to_compose1), int, int>(to_compose2, to_compose1)(1, 2))
        return 1;

    if (!sayten::compose<decltype(to_compose2), decltype(to_compose1), int, int>(to_compose2, to_compose1)(3, 5))
        return 1;

    if (sayten::compose_and_apply(to_compose2, to_compose1, 1, 2))
        return 1;

    if (!sayten::compose_and_apply(to_compose2, to_compose1, 3, 5))
        return 1;

    return 0;
}