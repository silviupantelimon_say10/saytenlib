#include "test.hpp"

int main()
{
    assert_const<sayten::types::has_contramap<test_contravariant>>("Test contravariant should have a proper 'contramap'!");
    assert_const<sayten::types::contravariant<test_contravariant>>("Test contravariant should be a contravariant functor!");

    test_contravariant<int> int_test([] (const int &x) { return std::to_string(x); });
    test_contravariant<std::string> string_test([] (const std::string &x) { return x; });
    std::string contramap_result = "2";

    assert(sayten::types::call_contramap(int_test, &plus_one_int).get_str(1) == contramap_result &&
           sayten::types::call_contramap(int_test, lambda_plus_one_int).get_str(1) == contramap_result &&
           sayten::types::call_contramap(string_test, &double_string).get_str(test_string) == test_string_doubled &&
           sayten::types::call_contramap(string_test, lambda_double_string).get_str(test_string) == test_string_doubled &&
           sayten::types::call_contramap(string_test, &int_to_string).get_str(2) == contramap_result &&
           sayten::types::call_contramap(string_test, lambda_int_to_string).get_str(2) == contramap_result,
           "Test functor should produce correct value for 'call_contramap'!");

    return 0;
}